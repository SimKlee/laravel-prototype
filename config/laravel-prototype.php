<?php

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;

return [
    'model-definition' => [
        'extends'    => [
            'Illuminate\\Database\\Eloquent\\Model',
            'App\\Models\\AbstractModel',
            'App\\Models\\AbstractBusinessModel',
            'App\\Models\\AbstractRelationModel',
        ],
        'interfaces' => [
            'App\\Models\\Interfaces\\BusinessModelInterface',
            'App\\Models\\Interfaces\\PayloadModelInterface',
        ],
        'traits' => [
            'App\\Models\\Traits\\AutoUuid',
            'App\\Models\\Traits\\AutoUuidSecondaryKey',
            'App\\Models\\Traits\\UuidSecondaryKey',
            'App\\Models\\Traits\\HasProcessTrait',
        ],
    ],
    'model'            => [
        'annotations' => [
            'use'     => [
                Collection::class,
                Arrayable::class,
                Closure::class,
            ],
            'methods' => [
                [
                    'name'       => 'create',
                    'returnType' => 'SELF',
                    'signature'  => 'array $attributes',
                    'isStatic'   => true,
                ],
                [
                    'name'       => 'find',
                    'returnType' => 'SELF',
                    'signature'  => 'mixed $id, array $columns = []',
                    'isStatic'   => true,
                ],
                [
                    'name'       => 'findMany',
                    'returnType' => 'Collection|Book[]',
                    'signature'  => 'Arrayable $ids, array $columns = []',
                    'isStatic'   => true,
                ],
                [
                    'name'       => 'findOrFail',
                    'returnType' => 'Collection|SELF[]',
                    'signature'  => 'Arrayable $ids, array $columns = []',
                    'isStatic'   => true,
                ],
                [
                    'name'       => 'findOrNew',
                    'returnType' => 'SELF',
                    'signature'  => 'mixed $id, array $columns = []',
                    'isStatic'   => true,
                ],
                [
                    'name'       => 'firstOrCreate',
                    'returnType' => 'SELF',
                    'signature'  => 'array $attributes, array $values = []',
                    'isStatic'   => true,
                ],
                [
                    'name'       => 'firstOrFail',
                    'returnType' => 'SELF',
                    'signature'  => 'array $columns = []',
                    'isStatic'   => true,
                ],
                [
                    'name'       => 'firstOrNew',
                    'returnType' => 'SELF',
                    'signature'  => 'array $attributes = [], array $values = []',
                    'isStatic'   => true,
                ],
                [
                    'name'       => 'updateOrCreate',
                    'returnType' => 'SELF',
                    'signature'  => 'array $attributes, array $values = []',
                    'isStatic'   => true,
                ],
                [
                    'name'       => 'firstOr',
                    'returnType' => 'SELF|mixed',
                    'signature'  => 'Closure|array $columns = [], Closure|array $callback = null',
                    'isStatic'   => true,
                ],
                [
                    'name'       => 'make',
                    'returnType' => 'SELF',
                    'signature'  => 'array $attributes',
                    'isStatic'   => true,
                ],
                [
                    'name'       => 'sole',
                    'returnType' => 'SELF',
                    'signature'  => 'array $columns = []',
                    'isStatic'   => true,
                ],
            ],
        ],
    ],
];
