<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Importer;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use SimKlee\LaravelPrototype\Definitions\Collections\ModelDefinitionCollection;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelWorkbench\Database\InformationSchema\DataTransferObjects\TableDTO;
use SimKlee\LaravelWorkbench\Database\InformationSchema\Tables;

class ModelDefinitionsImporter
{
    private ModelDefinitionCollection $modelDefinitions;

    private array $ignoreTables = [
        'migrations',
        'jobs',
        'password_reset_tokens',
        'failed_jobs',
        'users',
        'job_batches',
        'cache',
        'sessions',
        'cache_locks',
        'telescope_monitoring',
        'telescope_entries_tags',
        'telescope_entries',
    ];

    public function __construct()
    {
        $this->modelDefinitions = new ModelDefinitionCollection();
    }

    public function import(): void
    {
        (new Tables())
            ->get()
            ->reject(fn(TableDTO $table) => in_array($table->tableName, $this->ignoreTables))
            ->each(function (TableDTO $table, int $i) {

                $modelDefinition = new ModelDefinition();
                $modelDefinition->version(1);
                $modelDefinition->position(($i + 1));
                $modelDefinition->model(Str::ucfirst(Str::camel(Str::singular($table->tableName))));
                $modelDefinition->table($table->tableName);
                $this->modelDefinitions->add($modelDefinition);

                $columnImporter = new ColumnDefinitionsImporter($modelDefinition);
                $columnImporter->import();

                $this->setTimestamps($modelDefinition);
                $this->setSoftDelete($modelDefinition);

                $foreignKeyImporter = new ForeignKeyDefinitionsImporter($modelDefinition);
                $foreignKeyImporter->import();

                $indexImporter = new IndexDefinitionsImporter($modelDefinition);
                $indexImporter->import();
            });

        $this->modelDefinitions->fillRelations();
    }

    private function setTimestamps(ModelDefinition $modelDefinition): void
    {
        if ($modelDefinition->columns->has('created_at') && $modelDefinition->columns->has('updated_at')) {
            $modelDefinition->timestamps(true);
        }
    }

    private function setSoftDelete(ModelDefinition $modelDefinition): void
    {
        if ($modelDefinition->columns->has('deleted_at')) {
            $modelDefinition->softDelete(true);
            $modelDefinition->traits[] = SoftDeletes::class;
        }
    }

    public function getModelDefinitions(): ModelDefinitionCollection
    {
        return $this->modelDefinitions;
    }
}
