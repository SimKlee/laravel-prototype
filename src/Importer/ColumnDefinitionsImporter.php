<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Importer;

use Illuminate\Support\Str;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelWorkbench\Database\InformationSchema\Columns;
use SimKlee\LaravelWorkbench\Database\InformationSchema\DataTransferObjects\ColumnDTO;

readonly class ColumnDefinitionsImporter
{
    public function __construct(private ModelDefinition $modelDefinition)
    {
    }

    public function import(): void
    {
        (new Columns())
            ->get($this->modelDefinition->table())
            ->each(function (ColumnDTO $column, int $i) {

                $columnDefinition = new ColumnDefinition($this->modelDefinition->model());
                $columnDefinition->version(1);
                $columnDefinition->name($column->columnName);
                $columnDefinition->type($this->getType($column->dataType));
                if (Str::startsWith($column->columnType, 'tinyint(1)')) {
                    $columnDefinition->type('boolean');
                }
                $columnDefinition->nullable($column->isNullable);

                if ($column->columnKey === 'PRI') {
                    $columnDefinition->primary(true);
                }

                if (Str::endsWith($column->columnType, 'unsigned')) {
                    $columnDefinition->unsigned(true);
                }

                if (Str::contains($column->extra, 'auto_increment')) {
                    $columnDefinition->autoIncrement(true);
                }

                if ($columnDefinition->dataType()->hasDecimals()) {
                    $columnDefinition->decimals($column->numericPrecision);
                }
                if ($columnDefinition->dataType()->hasLength()) {
                    $columnDefinition->length($column->characterMaximumLength);
                }

                $this->modelDefinition->columns->add($columnDefinition);
            });
    }

    private function getType(string $type): string
    {
        return match ($type) {
            'int'        => 'integer',
            'bigint'     => 'bigInteger',
            'mediumint'  => 'mediumInteger',
            'smallint'   => 'smallInteger',
            'tinyint'    => 'tinyInteger',
            'varchar'    => 'string',
            'char'       => 'char',
            'mediumtext' => 'mediumText',
            'longtext'   => 'json',
            'text'       => 'text',
            'enum'       => 'enum',
            'float'      => 'float',
            'decimal'    => 'decimal',
            'timestamp'  => 'timestamp',
            'datetime'   => 'dateTime',
            'date'       => 'date',
            default      => throw new \Exception('Unknown: ' . $type),
        };
    }
}
