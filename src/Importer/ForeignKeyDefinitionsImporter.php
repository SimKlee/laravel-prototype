<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Importer;

use Illuminate\Support\Str;
use SimKlee\LaravelPrototype\Definitions\ForeignKeyDefinition;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelWorkbench\Database\InformationSchema\DataTransferObjects\ForeignKeyDTO;
use SimKlee\LaravelWorkbench\Database\InformationSchema\ForeignKeys;

readonly class ForeignKeyDefinitionsImporter
{
    public function __construct(private ModelDefinition $modelDefinition)
    {
    }

    public function import(): void
    {
        (new ForeignKeys())
            ->get($this->modelDefinition->table())
            ->each(function (ForeignKeyDto $foreignKey, int $i) {
                $foreignKeyDefinition = new ForeignKeyDefinition($this->modelDefinition->model(), $this->modelDefinition->table(), $foreignKey->columnName);
                $foreignKeyDefinition->type('one-to-many');
                $foreignKeyDefinition->name($foreignKey->constraintName);
                $foreignKeyDefinition->foreignModel(Str::ucfirst(Str::camel($foreignKey->referencedTableName)));
                $foreignKeyDefinition->foreignTable($foreignKey->referencedTableName);
                $foreignKeyDefinition->foreignColumn($foreignKey->referencedColumnName);
                $foreignKeyDefinition->onUpdate($foreignKey->updateRule);
                $foreignKeyDefinition->onDelete($foreignKey->deleteRule);
                $this->modelDefinition->foreignKeys->add($foreignKeyDefinition);
            });
    }

    private function getType(string $type): string
    {
        return match ($type) {
            'int'        => 'integer',
            'bigint'     => 'bigInteger',
            'mediumint'  => 'mediumInteger',
            'smallint'   => 'smallInteger',
            'tinyint'    => 'tinyInteger',
            'varchar'    => 'string',
            'char'       => 'char',
            'mediumtext' => 'mediumText',
            'longtext'   => 'json',
            'text'       => 'text',
            'enum'       => 'enum',
            'float'      => 'float',
            'decimal'    => 'decimal',
            'timestamp'  => 'timestamp',
            'datetime'   => 'dateTime',
            'date'       => 'date',
            default      => throw new \Exception('Unknown: ' . $type),
        };
    }
}
