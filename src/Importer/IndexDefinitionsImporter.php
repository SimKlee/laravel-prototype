<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Importer;

use Illuminate\Support\Str;
use SimKlee\LaravelPrototype\Definitions\ForeignKeyDefinition;
use SimKlee\LaravelPrototype\Definitions\IndexDefinition;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelWorkbench\Database\InformationSchema\DataTransferObjects\ForeignKeyDTO;
use SimKlee\LaravelWorkbench\Database\InformationSchema\DataTransferObjects\IndexDTO;
use SimKlee\LaravelWorkbench\Database\InformationSchema\ForeignKeys;
use SimKlee\LaravelWorkbench\Database\InformationSchema\Indexes;

readonly class IndexDefinitionsImporter
{
    public function __construct(private ModelDefinition $modelDefinition)
    {
    }

    public function import(): void
    {
        (new Indexes())
            ->get($this->modelDefinition->table())
            ->reject(fn(IndexDTO $index) => $index->indexName === 'PRIMARY')
            ->each(function (IndexDTO $index) {
                $indexDefinition = new IndexDefinition($this->modelDefinition->model());
                $indexDefinition->name($index->indexName);
                $indexDefinition->type($index->nonUnique ? 'index' : 'unique');
                $indexDefinition->columns(explode(',', $index->columns));
                $this->modelDefinition->indexes->add($indexDefinition);
            });
    }
}
