<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Actions;

use SimKlee\LaravelPrototype\Definitions\Class\ClassDefinition;
use SimKlee\LaravelPrototype\Model\AbstractModelRepository;

class CreateModelRepositoryClass extends AbstractCreateClassFromModelDefinition
{
    protected function description(): string
    {
        return 'Model repository class';
    }

    protected function stub(): string
    {
        return 'prototype::stubs.model-repository-class';
    }

    public function file(): string
    {
        return app_path(sprintf('Models/Repositories/%sRepository.php', $this->modelDefinition->model()));
    }

    protected function classDefinition(): ClassDefinition
    {
        $classDefinition = new ClassDefinition($this->modelDefinition->model() . 'Repository');
        $classDefinition->namespace('App\Models\Repositories');
        $classDefinition->extends(AbstractModelRepository::class);

        return $classDefinition;
    }

}