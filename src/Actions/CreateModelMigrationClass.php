<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Actions;

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use SimKlee\LaravelPrototype\Definitions\Class\ClassDefinition;
use SimKlee\LaravelPrototype\Model\MigrationHelper;

class CreateModelMigrationClass extends AbstractCreateClassFromModelDefinition
{
    protected function description(): string
    {
        return 'Model meta class';
    }

    protected function stub(): string
    {
        return 'prototype::stubs.model-migration-class';
    }

    public function file(): string
    {
        $datetime      = Carbon::now()->format('Y_m_d_His');
        $oldMigrations = MigrationHelper::getMigrations($this->modelDefinition->table());
        if ($oldMigrations->count() > 0) {
            $latest   = $oldMigrations->last();
            $datetime = $latest['datetime'];
        }

        return database_path(sprintf(
            'migrations/%s_create_table_%s.php',
            $datetime,
            $this->modelDefinition->table()
        ));
    }

    protected function classDefinition(): ClassDefinition
    {
        $classDefinition = new ClassDefinition('Migration');
        $classDefinition->extends(Migration::class);
        $classDefinition->addUse(Blueprint::class);
        $classDefinition->addUse(sprintf('App\Models\%s', $this->modelDefinition->model()));
        $this->modelDefinition->foreignKeys
            ->getUsedModels()
            ->each(function (string $class) use ($classDefinition) {
                $classDefinition->addUse($class);
            });

        return $classDefinition;
    }

}