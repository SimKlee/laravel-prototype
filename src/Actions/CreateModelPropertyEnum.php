<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Actions;

use BackedEnum;
use Illuminate\Support\Str;
use SimKlee\LaravelPrototype\Definitions\Class\ClassDefinition;
use SimKlee\LaravelPrototype\Definitions\Class\BackedEnumDefinition;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelWorkbench\Generators\ClassGenerator;
use SimKlee\LaravelPrototype\Model\Enumerations\Contracts\AsKeyValueInterface;
use SimKlee\LaravelPrototype\Model\Enumerations\Contracts\DefaultValueInterface;
use SimKlee\LaravelPrototype\Model\Enumerations\Contracts\TranslatableInterface;
use SimKlee\LaravelPrototype\Model\Enumerations\Traits\UseKeyValue;
use SimKlee\LaravelPrototype\Model\Enumerations\Traits\UseTranslations;

class CreateModelPropertyEnum extends AbstractCreateClassFromModelDefinition
{
    protected function description(): string
    {
        return 'Model property enum';
    }

    public function __construct(protected ModelDefinition  $modelDefinition,
                                protected ColumnDefinition $columnDefinition)
    {
        parent::__construct($this->modelDefinition);
    }

    protected function stub(): string
    {
        return 'prototype::stubs.model-property-enum';
    }

    public function file(): string
    {
        return app_path(sprintf(
            'Models/Enumerations/%s%sEnum.php',
            $this->modelDefinition->model(),
            $this->columnDefinition->formatter()->asVarName()
        ));
    }

    protected function configureGenerator(ClassGenerator $generator): void
    {
        $generator->setData('columnDefinition', $this->columnDefinition);
    }

    protected function classDefinition(): ClassDefinition
    {
        $classDefinition = new BackedEnumDefinition(sprintf(
            '%s%sEnum',
            $this->modelDefinition->model(),
            $this->columnDefinition->formatter()->asVarName()
        ));
        $classDefinition->namespace('App\Models\Enumerations');
        collect($this->columnDefinition->values())->each(function ($value) use ($classDefinition) {
            $classDefinition->addCase(Str::upper($value), $value);
        });

        $classDefinition->addUse(BackedEnum::class);
        $classDefinition->implements(DefaultValueInterface::class);

        $classDefinition->implements(TranslatableInterface::class);
        $classDefinition->addTrait(UseTranslations::class);
        $classDefinition->classAnnotationsDefinition->addMethod('translate', 'string', 'string $lang = null');

        $classDefinition->implements(AsKeyValueInterface::class);
        $classDefinition->addTrait(UseKeyValue::class);
        $classDefinition->classAnnotationsDefinition->addMethod('toKeyValue', 'array');

        return $classDefinition;
    }
}
