<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Actions;

use SimKlee\LaravelPrototype\Definitions\Class\ClassDefinition;
use SimKlee\LaravelPrototype\Model\AbstractModelQuery;

class CreateModelQueryClass extends AbstractCreateClassFromModelDefinition
{
    protected function description(): string
    {
        return 'Model query class';
    }

    protected function stub(): string
    {
        return 'prototype::stubs.model-query-class';
    }

    public function file(): string
    {
        return app_path(sprintf('Models/Queries/%sQuery.php', $this->modelDefinition->model()));
    }

    protected function classDefinition(): ClassDefinition
    {
        $classDefinition = new ClassDefinition($this->modelDefinition->model() . 'Query');
        $classDefinition->namespace('App\Models\Queries');
        $classDefinition->extends(AbstractModelQuery::class);

        return $classDefinition;
    }

}