<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Actions;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use SimKlee\LaravelPrototype\Definitions\Class\ClassConstantDefinition;
use SimKlee\LaravelPrototype\Definitions\Class\ClassDefinition;
use SimKlee\LaravelPrototype\Definitions\Class\ClassPropertyDefinition;
use SimKlee\LaravelPrototype\Definitions\Class\ValueDefinition;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\LaravelPrototype\Definitions\ForeignKeyDefinition;
use SimKlee\LaravelPrototype\Definitions\RelationDefinition;
use SimKlee\LaravelPrototype\Exceptions\UnexpectedMatchValueException;
use SimKlee\LaravelPrototype\Model\AbstractModel;
use SimKlee\StringBuffer\StringBuffer;

class CreateModelClass extends AbstractCreateClassFromModelDefinition
{
    protected function description(): string
    {
        return 'Model class';
    }

    protected function stub(): string
    {
        return 'prototype::stubs.model-class';
    }

    public function file(): string
    {
        return app_path(sprintf('Models/%s.php', $this->modelDefinition->model()));
    }

    protected function classDefinition(): ClassDefinition
    {
        $classDefinition = new ClassDefinition($this->modelDefinition->model());
        $classDefinition->namespace('App\Models');
        $classDefinition->extends(AbstractModel::class);

        $this->setConstants($classDefinition);
        $this->setProperties($classDefinition);
        $this->setUseAndTraits($classDefinition);
        $this->setAnnotationsForColumns($classDefinition);
        $this->setMethodAnnotations($classDefinition);

        return $classDefinition;
    }

    private function setUseAndTraits(ClassDefinition $classDefinition): void
    {
        if ($this->modelDefinition->softDelete()) {
            $classDefinition->addTrait(SoftDeletes::class);
        }

        if ($this->modelDefinition->columns->dates()->count() > 0) {
            $classDefinition->addUse(Carbon::class);
        }

        $this->modelDefinition->columns
            ->enumerations()
            ->each(function (string $enum) use ($classDefinition) {
                $classDefinition->addUse($enum);
            });

        $this->modelDefinition->foreignKeys
            ->getUsedRelations()
            ->each(function (string $class) use ($classDefinition) {
                $classDefinition->addUse($class);
            });

        $this->modelDefinition->relations
            ->getUsedClasses()
            ->each(function (string $class) use ($classDefinition) {
                $classDefinition->addUse($class);
            });
    }

    private function setAnnotationsForColumns(ClassDefinition $classDefinition): void
    {
        $this->modelDefinition->columns
            ->all()
            ->each(function (ColumnDefinition $columnDefinition) use ($classDefinition) {
                try {
                    $classDefinition->classAnnotationsDefinition->addProperty(
                        name: $columnDefinition->name(),
                        type: $columnDefinition->cast()
                    );
                } catch (UnexpectedMatchValueException $e) {
                    Log::error($e->getMessage());
                }
            });

        $this->modelDefinition->foreignKeys
            ->all()
            ->each(function (ForeignKeyDefinition $foreignKeyDefinition) use ($classDefinition) {
                $classDefinition->classAnnotationsDefinition->addProperty(
                    name: $foreignKeyDefinition->formatter()->varName(),
                    type: $foreignKeyDefinition->foreignModel()
                );
            });

        $this->modelDefinition->relations
            ->all()
            ->each(function (RelationDefinition $relationDefinition) use ($classDefinition) {
                $classDefinition->classAnnotationsDefinition->addProperty(
                    name: $relationDefinition->formatter()->varName(),
                    type: 'Collection|' . $relationDefinition->relatedModel() . '[]'
                );
            });
    }

    private function setMethodAnnotations(ClassDefinition $classDefinition): void
    {
        foreach (config('laravel-prototype.model.annotations.use', []) as $use) {
            $classDefinition->addUse($use);
        }

        $repositoryClass = sprintf('App\Models\Repositories\%sRepository', $this->modelDefinition->model());
        $classDefinition->addUse($repositoryClass);
        $classDefinition->classAnnotationsDefinition->addMethod(
            name      : 'repository',
            returnType: class_basename($repositoryClass),
            isStatic  : true
        );

        $queryClass = sprintf('App\Models\Queries\%sQuery', $this->modelDefinition->model());
        $classDefinition->addUse($queryClass);
        $classDefinition->classAnnotationsDefinition->addMethod(
            name      : 'query',
            returnType: class_basename($queryClass),
            isStatic  : true
        );

        $queryClass = sprintf('App\Models\Meta\%sMeta', $this->modelDefinition->model());
        $classDefinition->addUse($queryClass);
        $classDefinition->classAnnotationsDefinition->addMethod(
            name      : 'meta',
            returnType: class_basename($queryClass),
            isStatic  : true
        );

        foreach (config('laravel-prototype.model.annotations.methods', []) as $method) {
            $returnType = Str::replace('SELF', $this->modelDefinition->model(), $method['returnType']);
            $classDefinition->classAnnotationsDefinition->addMethod(
                name      : $method['name'],
                returnType: $returnType,
                signature : $method['signature'] ?? null,
                isStatic  : $method['isStatic'] ?? false
            );
        }
    }

    private function setConstants(ClassDefinition $classDefinition): void
    {
        $classDefinition->addConstant(new ClassConstantDefinition('TABLE', $this->modelDefinition->table()));
        $this->modelDefinition->columns
            ->all()
            ->each(function (ColumnDefinition $columnDefinition) use ($classDefinition) {
                $classDefinition->addConstant(new ClassConstantDefinition(
                    name : $columnDefinition->formatter()->asPropertyString(),
                    value: $columnDefinition->name()
                ));
            });

        $this->modelDefinition->foreignKeys
            ->all()
            ->each(function (ForeignKeyDefinition $foreignKeyDefinition) use ($classDefinition) {
                $classDefinition->addConstant(new ClassConstantDefinition(
                    name : 'WITH_' . Str::upper(Str::snake($foreignKeyDefinition->foreignModel())),
                    value: Str::snake($foreignKeyDefinition->foreignModel())
                ));
            });

        $this->modelDefinition->relations
            ->all()
            ->each(function (RelationDefinition $relationDefinition) use ($classDefinition) {
                $classDefinition->addConstant(new ClassConstantDefinition(
                    name : 'WITH_' . Str::upper($relationDefinition->formatter()->varName()),
                    value: Str::camel($relationDefinition->formatter()->varName())
                ));
            });
    }

    private function setProperties(ClassDefinition $classDefinition): void
    {
        $classDefinition->addProperty(new ClassPropertyDefinition(
            name       : 'table',
            value      : new ValueDefinition('self::TABLE', ValueDefinition::TYPE_AS_CONST),
            visibility : ClassPropertyDefinition::VISIBILITY_PROTECTED,
            annotations: ['var' => 'string']
        ));

        $classDefinition->addProperty(new ClassPropertyDefinition(
            name       : 'timestamps',
            value      : $this->modelDefinition->timestamps(),
            visibility : ClassPropertyDefinition::VISIBILITY_PUBLIC,
            annotations: ['var' => 'bool']
        ));

        $classDefinition->addProperty(new ClassPropertyDefinition(
            name       : 'fillable',
            value      : [],
            visibility : ClassPropertyDefinition::VISIBILITY_PROTECTED,
            annotations: ['var' => 'array|string[]']
        ));

        $classDefinition->addProperty($this->createPropertyWithColumnList('guarded'));
        $classDefinition->addProperty($this->createPropertyWithColumnList('hidden'));
        $classDefinition->addProperty($this->createPropertyWithColumnList('dates'));

        $classDefinition->addProperty(new ClassPropertyDefinition(
            name       : 'casts',
            value      : $this->getCastedColumns()->toArray(),
            visibility : ClassPropertyDefinition::VISIBILITY_PROTECTED,
            annotations: ['var' => 'array']
        ));
    }

    private function createPropertyWithColumnList(string $name): ClassPropertyDefinition
    {
        $columns = $this->modelDefinition->columns;
        $columns = call_user_func([$columns, $name]);
        $columns = $columns->map(function (ColumnDefinition $columnDefinition) {
            return 'self::' . $columnDefinition->formatter()->asPropertyString();
        });

        return new ClassPropertyDefinition(
            name       : $name,
            value      : $columns->toArray(),
            visibility : ClassPropertyDefinition::VISIBILITY_PROTECTED,
            annotations: ['var' => 'array|string[]']
        );
    }

    private function getCastedColumns(): Collection
    {
        return $this->modelDefinition->columns
            ->all()
            ->filter(function (ColumnDefinition $columnDefinition) {
                return !in_array($columnDefinition->cast(), ['string', 'Carbon']);
            })
            ->map(function (ColumnDefinition $columnDefinition) {
                $cast = $columnDefinition->cast();
                return StringBuffer::create('self::')
                                   ->append($columnDefinition->formatter()->asPropertyString())
                                   ->append(' => ')
                                   ->appendIf(Str::endsWith($cast, 'Enum'), $cast . '::class', sprintf("'%s'", $cast))
                                   ->toString();
            });
    }
}
