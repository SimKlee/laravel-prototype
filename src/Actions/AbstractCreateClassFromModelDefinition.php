<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Actions;

use SimKlee\LaravelPrototype\Definitions\Class\ClassDefinition;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelPrototype\Messages\Message;
use SimKlee\LaravelWorkbench\Generators\ClassGenerator;

abstract class AbstractCreateClassFromModelDefinition extends AbstractAction
{
    protected bool $overwrite = false;

    public function __construct(protected ModelDefinition $modelDefinition)
    {
        parent::__construct();
    }

    public function overwrite(bool $overwrite = null): bool
    {
        if (!is_null($overwrite)) {
            $this->overwrite = $overwrite;
        }

        return $this->overwrite;
    }

    public function handle(): void
    {
        $generator = new ClassGenerator($this->stub(), $this->classDefinition());
        $generator->setData('modelDefinition', $this->modelDefinition);
        $this->configureGenerator($generator);
        $result = $generator->save($this->file(), $this->overwrite);

        $this->messages->addIf(
            $result,
            new Message($this->description() . ' successfully written.', Message::TYPE_INFO, $this->file()),
            new Message($this->description() . ' writing failed!', Message::TYPE_ERROR, $this->file())
        );
    }

    protected function configureGenerator(ClassGenerator $generator): void
    {

    }

    abstract protected function classDefinition(): ClassDefinition;

    abstract protected function description(): string;

    abstract protected function stub(): string;

    abstract public function file(): string;
}
