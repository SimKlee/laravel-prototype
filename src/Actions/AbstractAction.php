<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Actions;

use SimKlee\LaravelPrototype\Messages\MessageCollection;

abstract class AbstractAction
{
    public MessageCollection $messages;

    public function __construct()
    {
        $this->messages = new MessageCollection();
    }

    abstract public function handle(): void;
}