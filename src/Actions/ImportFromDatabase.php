<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Actions;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ImportFromDatabase extends AbstractAction
{
    private array $json = [
        'ModelConfiguration' => [
            'version' => 1,
            'models'  => [],
        ],
    ];

    public function __construct(private readonly string $connection)
    {
        parent::__construct();
    }

    public function handle(): void
    {
        #dump($this->getTables());

        /** @var string $table */
        $table = $this->getTables()->get(11);
        dump($this->createModelJson($table));
    }

    public function getTables(): Collection
    {
        return DB::connection($this->connection)
            ->table('INFORMATION_SCHEMA.TABLES')
            ->where('TABLES.TABLE_SCHEMA', config(sprintf('database.connections.%s.database', $this->connection)))
            ->where('TABLES.TEMPORARY', 'N')
            ->where('TABLES.TABLE_TYPE', 'BASE TABLE')
            ->pluck('TABLES.TABLE_NAME');
    }

    public function getTableColumns(string $table): Collection
    {
        return DB::connection($this->connection)
            ->table('INFORMATION_SCHEMA.COLUMNS')
            ->where('COLUMNS.TABLE_SCHEMA', config(sprintf('database.connections.%s.database', $this->connection)))
            ->where('COLUMNS.TABLE_NAME', $table)
            ->orderBy('COLUMNS.ORDINAL_POSITION')
            ->get();
    }

    private function createModelJson(string $table, int $i = 0): array
    {
        $columns = $this->getTableColumns($table);

        return [
            'position'    => $i,
            'name'        => Str::ucfirst(Str::camel(Str::singular($table))),
            'table'       => $table,
            'package'     => null,
            'timestamps'  => $columns->filter(fn(\stdClass $info) => in_array($info->COLUMN_NAME, ['created_at', 'updated_at']))->count() === 2,
            'softDelete'  => $columns->filter(fn(\stdClass $info) => $info->COLUMN_NAME === 'deleted_at')->count() === 1,
            'uuid'        => null,
            'traits'      => [],
            'interfaces'  => [],
            'columns'     => $columns->map(fn(\stdClass $info) => $this->columnInfoToJson($info))->toArray(),
            'indexes'     => $this->getIndexNames($table)
                ->map(fn(string $type, string $indexName) => [
                    'type'    => $type,
                    'name'    => $indexName,
                    'columns' => $this->getIndexColumns($table, $indexName)->toArray(),
                ])->values()->toArray(),
            'foreignKeys' => [],
            'relations'   => [],
        ];
    }

    private function columnInfoToJson(\stdClass $info): array
    {
        return [
            'name'          => $info->COLUMN_NAME,
            'type'          => $info->DATA_TYPE,
            'unsigned'      => Str::contains($info->COLUMN_TYPE, 'unsigned'),
            'primary'       => $info->COLUMN_KEY === 'PRI',
            'autoIncrement' => Str::contains($info->EXTRA, 'auto_increment'),
            'length'        => $info->CHARACTER_MAXIMUM_LENGTH,
            'nullable'      => $info->IS_NULLABLE === 'YES',
        ];
    }

    private function getIndexNames(string $table): Collection
    {
        return DB::connection($this->connection)
            ->table('INFORMATION_SCHEMA.STATISTICS')
            ->where('STATISTICS.TABLE_SCHEMA', config(sprintf('database.connections.%s.database', $this->connection)))
            ->where('STATISTICS.TABLE_NAME', $table)
            ->where('STATISTICS.INDEX_NAME', '<>', 'PRIMARY')
            ->groupBy('STATISTICS.INDEX_NAME')
            ->get()
            ->mapWithKeys(function (\stdClass $index) {
                dump($index);
                $type = 'index';
                if ($index->INDEX_NAME === 'PRIMARY') {
                    $type = 'primary';
                } elseif ($index->NON_UNIQUE === 0) {
                    $type = 'unique';
                }
                return [$index->INDEX_NAME => $type];
            });
    }

    private function getIndexColumns(string $table, string $indexName): Collection
    {
        return DB::connection($this->connection)
            ->table('INFORMATION_SCHEMA.STATISTICS')
            ->where('STATISTICS.TABLE_SCHEMA', config(sprintf('database.connections.%s.database', $this->connection)))
            ->where('STATISTICS.TABLE_NAME', $table)
            ->where('STATISTICS.INDEX_NAME', $indexName)
            ->orderBy('STATISTICS.SEQ_IN_INDEX')
            ->pluck('STATISTICS.COLUMN_NAME');
    }
}
