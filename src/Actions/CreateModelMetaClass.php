<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Actions;

use Illuminate\Support\Collection;
use SimKlee\LaravelPrototype\Definitions\Class\ClassDefinition;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\LaravelPrototype\Model\AbstractModelMeta;

class CreateModelMetaClass extends AbstractCreateClassFromModelDefinition
{
    protected function description(): string
    {
        return 'Model meta class';
    }

    protected function stub(): string
    {
        return 'prototype::stubs.model-meta-class';
    }

    public function file(): string
    {
        return app_path(sprintf('Models/Meta/%sMeta.php', $this->modelDefinition->model()));
    }

    protected function classDefinition(): ClassDefinition
    {
        $classDefinition = new ClassDefinition($this->modelDefinition->model() . 'Meta');
        $classDefinition->namespace('App\Models\Meta');
        $classDefinition->extends(AbstractModelMeta::class);
        $classDefinition->addUse(Collection::class);
        $classDefinition->addUse(sprintf('App\Models\%s', $this->modelDefinition->model()));
        $this->modelDefinition->columns
            ->enumerations()
            ->each(function (string $class) use ($classDefinition) {
                $classDefinition->addUse($class);
            });

        return $classDefinition;
    }

}