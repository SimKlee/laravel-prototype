<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Model;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\StringBuffer\StringBuffer;
use SplFileInfo;

final class MigrationHelper
{
    public static function getMigrations(string $table): Collection
    {
        return collect(File::files(database_path('migrations')))
            ->filter(function (SplFileInfo $fileInfo) use ($table) {
                return Str::endsWith($fileInfo->getBasename('.php'), sprintf('create_table_%s', $table));
            })->map(function (SplFileInfo $fileInfo) {
                [$year, $month, $day, $time, , , $table] = explode('_', $fileInfo->getBasename('.php'));
                return [
                    'file'     => $fileInfo->getPathInfo(),
                    'datetime' => sprintf('%s_%s_%s_%s', $year, $month, $day, $time),
                    'table'    => $table,
                    'model'    => Str::camel(Str::singular($table)),
                ];
            });
    }

    public static function createMigrationBlueprint(ColumnDefinition $columnDefinition): string
    {
        $default = false;
        if (!is_null($columnDefinition->default())) {
            $default = sprintf('->default(%s)', self::castMigrationDefaultValue($columnDefinition));
        }

        return StringBuffer::create('$table->')
                           ->append($columnDefinition->dataType()->value)
                           ->append('(')
                           ->append($columnDefinition->formatter()->asPropertyString(true))
                           ->appendIf(is_int($columnDefinition->length()), ', ' . $columnDefinition->length())
                           ->appendIf(is_int($columnDefinition->precision()), ', ' . $columnDefinition->precision())
                           ->appendIf(is_int($columnDefinition->decimals()), ', ' . $columnDefinition->decimals())
                           ->append(')')
                           ->appendIf($columnDefinition->unsigned() === true, self::migrationNewLine() . '->unsigned()')
                           ->appendIf($columnDefinition->nullable() === true, self::migrationNewLine() . '->nullable()')
                           ->appendIf($columnDefinition->autoIncrement() === true, self::migrationNewLine() . '->autoIncrement()')
                           ->appendIf($default !== false, self::migrationNewLine() . $default)
                           ->append(';')
                           ->toString();
    }

    private static function castMigrationDefaultValue(ColumnDefinition $columnDefinition): string|null
    {
        return match (gettype($columnDefinition->default())) {
            'boolean' => $columnDefinition->default() === true ? 'true' : 'false',
            'string'  => sprintf("'%s'", $columnDefinition->default()),
            default   => (string) $columnDefinition->default(),
        };
    }

    private static function migrationNewLine(): string
    {
        return PHP_EOL . "\t\t\t\t";
    }

}