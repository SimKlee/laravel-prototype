<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use SimKlee\LaravelPrototype\Exceptions\UnknownJoinTypeException;
use SimKlee\StringBuffer\StringBuffer;

abstract class AbstractModelQuery extends Builder
{
    protected array $joined = [];

    protected const JOIN_TYPE_INNER = 'inner';
    protected const JOIN_TYPE_LEFT  = 'left';

    public function selectAll(string $table): AbstractModelQuery
    {
        $this->select([sprintf('%s.*', $table)]);

        return $this;
    }

    /**
     * @throws UnknownJoinTypeException
     */
    public function createJoin(
        string $type,
        string $table,
        string $condition1,
        string $condition2,
        string $with = null,
        string $groupBy = null
    ): AbstractModelQuery
    {
        if ($this->isJoined($table)) {
            return $this;
        }

        $method = match ($type) {
            self::JOIN_TYPE_INNER => 'join',
            self::JOIN_TYPE_LEFT  => 'leftJoin',
            default               => throw new UnknownJoinTypeException($type)
        };

        $this->{$method}($table, $condition1, $condition2)
             ->when(is_string($with), function (AbstractModelQuery $query) use ($with) {
                 return $query->with($with);
             })
             ->when(!is_null($groupBy), function (AbstractModelQuery $query) use ($groupBy) {
                 return $query->groupBy($groupBy);
             });

        $this->joined[] = $table;

        return $this;
    }

    public function concat(string $separator, array $columns, string $alias = null): AbstractModelQuery
    {
        $this->addSelect(DB::raw(
            StringBuffer::create(string: 'CONCAT_WS(')
                        ->appendFormatted(format: "'%s'", values: $separator)
                        ->appendImplode(values: $columns, separator: ', ')
                        ->append(string: ')')
                        ->appendIf(condition: !is_null($alias), string: ' AS ' . $alias)
        ));

        return $this;
    }

    protected function isJoined(string $table): bool
    {
        return in_array($table, $this->joined);
    }
}
