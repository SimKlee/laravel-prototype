<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Model;

use Closure;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\Expression;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

/**
 * @method static AbstractModel create(array $attributes)
 * @method static AbstractModel find(mixed $id, array $columns = [])
 * @method static Collection|AbstractModel[] findMany(Arrayable $ids, array $columns = [])
 * @method static AbstractModel findOrFail(mixed $id, array $columns = [])
 * @method static AbstractModel findOrNew(mixed $id, array $columns = [])
 * @method static AbstractModel firstOrCreate(array $attributes, array $values = [])
 * @method static AbstractModel firstOrFail(array $columns = [])
 * @method static AbstractModel firstOrNew(array $attributes = [], array $values = [])
 * @method static AbstractModel updateOrCreate(array $attributes, array $values = [])
 * @method static AbstractModel|mixed firstOr(Closure|array $columns = [], Closure|array $callback = null)
 * @method static AbstractModel sole(array $columns = [])
 * @method static mixed value(string|Expression $column)
 * @method static mixed valueOrFail(string|Expression $column)
 */
abstract class AbstractModel extends Model
{
    /**
     * @param Builder $query
     *
     * @return AbstractModelQuery
     * @throws ClassNotFoundException
     */
    public function newEloquentBuilder($query): AbstractModelQuery
    {
        $class = sprintf('\App\Models\Queries\%sQuery', class_basename(static::class));

        if (class_exists($class) === false) {
            throw new ClassNotFoundException($class);
        }

        return new $class($query);
    }

    public static function repository(): ModelRepositoryInterface|null
    {
        $class = sprintf('\App\Models\Repositories\%sRepository', class_basename(static::class));
        if (class_exists($class)) {
            return new $class();
        }

        return null;
    }

    public static function meta(): AbstractModelMeta|null
    {
        $class = sprintf('\App\Models\Meta\%sMeta', class_basename(static::class));
        if (class_exists($class)) {
            return new $class(static::class);
        }

        return null;
    }

    public static function slugName(): string
    {
        return Str::singular(static::TABLE);
    }

    public static function column(string $column, string|null $alias = null): string
    {
        if (is_null($alias)) {
            return sprintf('%s.%s', static::TABLE, $column);
        }

        return sprintf('%s.%s AS %s', static::TABLE, $column, $alias);
    }

    public function baseName(): string
    {
        return class_basename($this);
    }

    public static function getModel(AbstractModel|string $model, int $id): AbstractModel
    {
        if (is_string($model)) {
            if (Str::contains($model, '\\') === false) {
                $model = sprintf('App\\Models\\%s', $model);
            }

            $model = new $model();
        }

        return $model::find($id);
    }
}