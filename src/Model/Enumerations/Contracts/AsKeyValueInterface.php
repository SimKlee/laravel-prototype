<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Model\Enumerations\Contracts;

interface AsKeyValueInterface
{
    public static function toKeyValue(): array;
}