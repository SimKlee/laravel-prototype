<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Model\Enumerations\Contracts;

use BackedEnum;

interface DefaultValueInterface
{
    public static function default(): BackedEnum|null;
}