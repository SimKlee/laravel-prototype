<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Model\Enumerations\Contracts;

interface TranslatableInterface
{
    public function translationKey(): string;

    public function translate(string $lang = null): string;
}