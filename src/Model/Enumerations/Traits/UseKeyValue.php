<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Model\Enumerations\Traits;

use App\Models\Enumerations\BookFormatEnum;
use BackedEnum;
use SimKlee\LaravelPrototype\Model\Enumerations\Contracts\TranslatableInterface;

trait UseKeyValue
{
    public static function toKeyValue(): array
    {
        if (method_exists(static::class, 'translate')) {
            return collect(BookFormatEnum::cases())
                ->mapWithKeys(function (BackedEnum|TranslatableInterface $case) {
                    return [$case->value => $case->translate()];
                })->toArray();
        }

        return collect(BookFormatEnum::cases())
            ->mapWithKeys(function (BackedEnum $case) {
                return [$case->value => $case->value];
            })->toArray();
    }
}