<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Model\Enumerations\Traits;

/**
 * @method string translationKey()
 */
trait UseTranslations
{
    public function translate(string $lang = null): string
    {
        return trans($this->translationKey() . $this, [], $lang);
    }
}