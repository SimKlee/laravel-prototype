<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Model\Traits;

use Illuminate\Database\Query\Builder;
use SimKlee\LaravelPrototype\Exceptions\ClassNotFoundException;
use SimKlee\LaravelPrototype\Model\AbstractModelQuery;

trait UseModelQuery
{
    /**
     * @param Builder $query
     *
     * @return AbstractModelQuery
     * @throws ClassNotFoundException
     */
    public function newEloquentBuilder($query): AbstractModelQuery
    {
        $class = sprintf('\App\Models\Queries\%sQuery', class_basename(static::class));

        if (class_exists($class) === false) {
            throw new ClassNotFoundException($class);
        }

        return new $class($query);
    }
}