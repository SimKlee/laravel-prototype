<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Model\Traits;

use Illuminate\Support\Str;
use SimKlee\LaravelPrototype\Model\AbstractModel;

/**
 * @property array $hidden
 */
trait HasUuidAsSecondaryKey
{
    // only allowed since PHP 8.2
    //public const PROPERTY_ID   = 'id';
    //public const PROPERTY_UUID = 'uuid';

    public static function bootHasUuidAsSecondaryKey(): void
    {
        self::creating(function (AbstractModel $model) {
            $model->setAttribute('uuid', Str::uuid());
        });
    }

    public function getRouteKeyName(): string
    {
        return self::PROPERTY_UUID;
    }

    protected function getArrayableItems(array $values): array
    {
        /** @var AbstractModel $this */
        if (!in_array('hidden', $this->hidden)) {
            $this->hidden[] = self::PROPERTY_ID;
        }

        return parent::getArrayableItems($values);
    }

    public static function findByUuid(string $uuid): AbstractModel
    {
        return static::where(static::PROPERTY_UUID, $uuid)->first();
    }
}