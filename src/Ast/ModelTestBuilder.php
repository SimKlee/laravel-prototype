<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Ast;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use PhpParser\Modifiers;
use PhpParser\Node\Arg;
use PhpParser\Node\Expr;
use PhpParser\Node\ArrayItem;
use PhpParser\Node\Expr\Array_;
use PhpParser\Node\Expr\ArrayDimFetch;
use PhpParser\Node\Expr\ArrowFunction;
use PhpParser\Node\Expr\Assign;
use PhpParser\Node\Expr\ConstFetch;
use PhpParser\Node\Expr\FuncCall;
use PhpParser\Node\Expr\MethodCall;
use PhpParser\Node\Expr\PropertyFetch;
use PhpParser\Node\Expr\StaticCall;
use PhpParser\Node\Expr\Variable;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use PhpParser\Node\Param;
use PhpParser\Node\Scalar\Int_;
use PhpParser\Node\Scalar\String_;
use PhpParser\Node\Stmt\ClassMethod;
use PhpParser\Node\Stmt\Expression;
use PhpParser\Node\Stmt\Return_;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\LaravelPrototype\Definitions\ForeignKeyDefinition;
use SimKlee\LaravelPrototype\Definitions\RelationDefinition;
use SimKlee\LaravelWorkbench\Ast\ArrayBuilder;
use SimKlee\LaravelWorkbench\Ast\ArrayItemBuilder;
use SimKlee\LaravelWorkbench\Ast\ClassConstFetchBuilder;
use SimKlee\LaravelWorkbench\Ast\ClassMethodBuilder;
use SimKlee\LaravelWorkbench\Ast\ConstFetchBuilder;
use SimKlee\LaravelWorkbench\Ast\MethodCallBuilder;
use SimKlee\LaravelWorkbench\Ast\StaticCallBuilder;

readonly class ModelTestBuilder extends ModelBuilder
{
    private function getColumnDefinitions(): Collection
    {
        return $this->modelDefinition->columns->all()
            ->reject(fn(ColumnDefinition $columnDefinition) => in_array($columnDefinition->name(), [
                'id',
                'created_at',
                'updated_at',
                'deleted_at',
            ]));
    }

    public function getTestData(bool $forUpdate = false): ClassMethod
    {
        return ClassMethodBuilder::create()
            ->name($forUpdate ? 'getUpdateTestData' : 'getTestData')
            ->flags(Modifiers::PRIVATE)
            ->returnType('array')
            ->stmt(
                new Return_(ArrayBuilder::create(
                    $this->getColumnDefinitions()
                        ->map(fn(ColumnDefinition $columnDefinition) => ArrayItemBuilder::create(
                            ConstFetchBuilder::create('null'),
                            $this->modelConstFetch($columnDefinition->name())
                        ))
                )->object())
            )->object();
    }

    public function testCreate(): ClassMethod
    {
        $method = ClassMethodBuilder::create('testCreate')
            ->flags(Modifiers::PUBLIC)
            ->returnType('void')
            ->stmts($this->expressions([
                $this->assign('data', new MethodCall(new Variable('this'), new Identifier('getTestData'))),
                $this->assign(
                    Str::lcfirst($this->modelDefinition->model()),
                    StaticCallBuilder::create($this->modelDefinition->model(), 'create')
                        ->arg(new Variable('data'))
                        ->object()
                ),
                $this->modelMethodCall('refresh'),
            ]));


        $this->getColumnDefinitions()
            ->each(fn(ColumnDefinition $columnDefinition) => $method->stmt(
                new Expression($this->assertSameProperty($columnDefinition, 'data'))
            ));

        return $method->object();
    }

    public function testUpdate(): ClassMethod
    {
        $stmts = array_merge(
            [
                $this->assign('data', new MethodCall(new Variable('this'), new Identifier('getTestData'))),
                $this->assign('updateData', new MethodCall(new Variable('this'), new Identifier('getUpdateTestData'))),
                $this->assign(Str::lcfirst($this->modelDefinition->model()), new StaticCall(
                    class: new Name($this->modelDefinition->model()),
                    name : new Identifier('create'),
                    args : [new Arg(new Variable('data'))]
                )),
                $this->modelMethodCall('refresh'),
            ],
            $this->getColumnDefinitions()
                ->map(fn(ColumnDefinition $columnDefinition) => $this->assertSameProperty($columnDefinition, 'data'))
                ->toArray(),
            [
                $this->modelMethodCall('update', [new Variable('updateData')]),
                $this->modelMethodCall('refresh'),
            ],
            $this->getColumnDefinitions()
                ->map(fn(ColumnDefinition $columnDefinition) => $this->assertSameProperty($columnDefinition, 'updateData'))
                ->toArray(),
        );

        return new ClassMethod(
            name    : new Identifier('testUpdate'),
            subNodes: [
                'flags'      => Modifiers::PUBLIC,
                'returnType' => new Identifier('void'),
                'stmts'      => $this->expressions($stmts),
            ],
        );
    }

    public function testTimestamps(): ClassMethod
    {
        return ClassMethodBuilder::create('testTimestamps')
            ->flags(Modifiers::PUBLIC)
            ->returnType('void')
            ->stmts($this->expressions([
                $this->assign(Str::lcfirst($this->modelDefinition->model()), $this->factoryMethodCall()),
                $this->assertInstanceOf('Carbon', $this->modelPropertyFetch('created_at')),
                $this->assertInstanceOf('Carbon', $this->modelPropertyFetch('updated_at')),
            ]))->object();
    }

    public function testSoftDelete(): ClassMethod
    {
        return ClassMethodBuilder::create('testSoftDelete')
            ->flags(Modifiers::PUBLIC)
            ->returnType('void')
            ->stmts($this->expressions([
                $this->assign(Str::lcfirst($this->modelDefinition->model()), $this->factoryMethodCall()),
                $this->assertInstanceOf($this->modelDefinition->model(), new Variable(Str::lcfirst($this->modelDefinition->model()))),
                $this->assertNull($this->modelPropertyFetch('deleted_at')),
                $this->modelMethodCall('delete'),
                $this->modelMethodCall('refresh'),
                $this->assertInstanceOf('Carbon', $this->modelPropertyFetch('deleted_at')),
                new MethodCall(
                    var : new Variable('this'),
                    name: new Identifier('assertSoftDeleted'),
                    args: [new Arg(new Variable(Str::lcfirst($this->modelDefinition->model())))]
                ),
            ]))->object();
    }

    public function testDelete(): ClassMethod
    {
        return ClassMethodBuilder::create('testDelete')
            ->flags(Modifiers::PUBLIC)
            ->returnType('void')
            ->stmts($this->expressions([
                $this->assign(Str::lcfirst($this->modelDefinition->model()), $this->factoryMethodCall()),
                $this->assign('id', $this->modelPropertyFetch('id')),
                $this->assertInstanceOf($this->modelDefinition->model(), new Variable(Str::lcfirst($this->modelDefinition->model()))),
                $this->modelMethodCall('delete'),
                $this->modelMethodCall('refresh'),
                $this->assertNull(new StaticCall(
                    class: new Name($this->modelDefinition->model()),
                    name : new Identifier('find'),
                    args : [new Arg(new Variable('id'))]
                )),
            ]))->object();
    }

    public function testForceDelete(): ClassMethod
    {
        return ClassMethodBuilder::create('testForceDelete')
            ->flags(Modifiers::PUBLIC)
            ->returnType('void')
            ->stmts($this->expressions([
                $this->assign(Str::lcfirst($this->modelDefinition->model()), $this->factoryMethodCall()),
                $this->assign('id', $this->modelPropertyFetch('id')),
                $this->assertInstanceOf($this->modelDefinition->model(), new Variable(Str::lcfirst($this->modelDefinition->model()))),
                $this->modelMethodCall('forceDelete'),
                $this->assertNull(new MethodCall(
                    var : $this->modelStaticCall('withTrashed'),
                    name: new Identifier('find'),
                    args: [new Arg(new Variable('id'))]
                )),
            ]))->object();
    }

    public function testForeignKeys(): ClassMethod
    {
        $method = ClassMethodBuilder::create('testForeignKeys')
            ->flags(Modifiers::PUBLIC)
            ->returnType('void')
            ->stmt(new Expression($this->assign(Str::lcfirst($this->modelDefinition->model()), $this->factoryMethodCall())));

        $this->modelDefinition->foreignKeys->all()
            ->each(fn(ForeignKeyDefinition $foreignKeyDefinition) => $method->stmt(new Expression(
                $this->assertInstanceOf(
                    class   : $foreignKeyDefinition->foreignModel(),
                    argValue: $this->modelPropertyFetch(Str::replaceEnd('_id', '', $foreignKeyDefinition->column()))
                )
            )));

        return $method->object();
    }

    public function testRelations(): ClassMethod
    {
        $method = ClassMethodBuilder::create('testRelations')
            ->flags(Modifiers::PUBLIC)
            ->returnType('void')
            ->stmt(new Expression(
                $this->assign(Str::lcfirst($this->modelDefinition->model()), $this->factoryMethodCall())
            ));


        $this->modelDefinition->relations->all()
            ->each(function (RelationDefinition $relationDefinition) use (&$method) {

                $method->stmt(
                    MethodCallBuilder::create()
                        ->var(StaticCallBuilder::create($relationDefinition->relatedModel(), 'factory'))
                        ->name('create')
                        ->args([
                            ArrayBuilder::create([
                                ArrayItemBuilder::create(
                                    new PropertyFetch(
                                        new Variable(Str::lcfirst($this->modelDefinition->model())),
                                        new Identifier('id')
                                    ),
                                    ClassConstFetchBuilder::create(
                                        $relationDefinition->relatedModel(),
                                        sprintf('PROPERTY_%s', Str::upper($relationDefinition->relatedColumn()))
                                    )
                                ),
                            ]),
                        ])->asExpression()
                );

                $method->stmt(
                    MethodCallBuilder::create()
                        ->var(new Variable('this'))
                        ->name('assertCount')
                        ->args([
                            new Int_(1),
                            new PropertyFetch(
                                var : new Variable(Str::lcfirst($this->modelDefinition->model())),
                                name: new Identifier(Str::camel($relationDefinition->relatedTable()))
                            ),
                        ])->asExpression()
                );

                $method->stmt(new Expression(
                    MethodCallBuilder::create()
                        ->var(
                            new FuncCall(name: new Name('collect'), args: [
                                new Arg(new PropertyFetch(
                                    var : new Variable(Str::lcfirst($this->modelDefinition->model())),
                                    name: new Identifier(Str::camel($relationDefinition->relatedTable()))
                                )),
                            ])
                        )->name('each')
                        ->args([
                            new ArrowFunction([
                                'params' => [new Param(
                                                 var : new Variable(Str::lcfirst($relationDefinition->relatedModel())),
                                                 type: new Name($relationDefinition->relatedModel())
                                             )],
                                'expr'   => MethodCallBuilder::create()
                                    ->var(new Variable('this'))
                                    ->name('assertInstanceOf')
                                    ->args([
                                        ClassConstFetchBuilder::create(class: $relationDefinition->relatedModel(), name: 'class')->object(),
                                        new Variable(Str::lcfirst($relationDefinition->relatedModel())),
                                    ])->object(),
                            ]),
                        ])->object()
                ));
            });

        return $method->object();
    }

    public function assertInstanceOf(string $class, $argValue): MethodCall
    {
        return MethodCallBuilder::create('this', 'assertInstanceOf')
            ->arg(ClassConstFetchBuilder::create($class, 'class'))
            ->arg($argValue)
            ->object();
    }

    public function assertNull($argValue): MethodCall
    {
        return MethodCallBuilder::create('this', 'assertNull')
            ->arg(new Arg($argValue))
            ->object();
    }

    public function assertSame($expectedArgValue, $actualArgValue): MethodCall
    {
        return MethodCallBuilder::create('this', 'assertSame')
            ->args([
                new Arg($expectedArgValue),
                new Arg($actualArgValue),
            ])->object();
    }

    public function assertSameProperty(ColumnDefinition $columnDefinition, string $dataVar): MethodCall
    {
        $expectedArgValue = new ArrayDimFetch(var: new Variable($dataVar), dim: $this->modelConstFetch($columnDefinition->name()));
        $actualArgValue   = $this->modelPropertyFetch($columnDefinition->name());

        if ($columnDefinition->dataType()->getCastType() === 'array') {
            $expectedArgValue = new FuncCall(name: new Name('serialize'), args: [new Arg($expectedArgValue)]);
            $actualArgValue   = new FuncCall(name: new Name('serialize'), args: [new Arg($actualArgValue)]);
        } elseif (is_array($columnDefinition->values())) {
            $actualArgValue = new PropertyFetch(
                var : new PropertyFetch(
                    var : new Variable(Str::lcfirst($columnDefinition->model())),
                    name: new Identifier($columnDefinition->name())
                ),
                name: new Identifier('value')
            );
        } elseif ($columnDefinition->dataType()->isDate()) {

            $expectedArgValue = MethodCallBuilder::create()
                ->var(new ArrayDimFetch(
                    var: new Variable($dataVar),
                    dim: $this->modelConstFetch($columnDefinition->name())
                ))
                ->name('format')
                ->arg(new String_('U'))
                ->object();

            $actualArgValue = MethodCallBuilder::create()
                ->var(new PropertyFetch(
                    var : new Variable(Str::lcfirst($columnDefinition->model())),
                    name: new Identifier($columnDefinition->name())
                ))
                ->name('format')
                ->arg(new String_('U'))
                ->object();
        }

        return $this->assertSame($expectedArgValue, $actualArgValue);
    }

    public function expressions(array $expr): array
    {
        return collect($expr)->map(fn(Expr $expr) => new Expression($expr))->toArray();
    }

    public function assign(string $variable, Expr $expr): Assign
    {
        return new Assign(var: new Variable($variable), expr: $expr);
    }

    public function factoryMethodCall(): MethodCall
    {
        return MethodCallBuilder::create()
            ->var(StaticCallBuilder::create($this->modelDefinition->model(), 'factory'))
            ->name('create')
            ->object();
    }

}
