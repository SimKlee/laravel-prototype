<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Ast;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Str;
use PhpParser\Modifiers;
use PhpParser\Node\Arg;
use PhpParser\Node\Expr\Array_;
use PhpParser\Node\Expr\ClassConstFetch;
use PhpParser\Node\Expr\MethodCall;
use PhpParser\Node\Expr\PropertyFetch;
use PhpParser\Node\Expr\StaticCall;
use PhpParser\Node\Expr\Variable;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use PhpParser\Node\Stmt\ClassMethod;
use PhpParser\Node\Stmt\Return_;
use SimKlee\LaravelPrototype\Definitions\ForeignKeyDefinition;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelPrototype\Definitions\RelationDefinition;
use SimKlee\LaravelWorkbench\Ast\ClassConstFetchBuilder;
use SimKlee\LaravelWorkbench\Ast\ClassMethodBuilder;
use SimKlee\LaravelWorkbench\Ast\MethodCallBuilder;
use SimKlee\LaravelWorkbench\Ast\ReturnBuilder;
use SimKlee\LaravelWorkbench\Generators\Components\Annotations\PropertyAnnotation;

readonly class ModelBuilder
{
    public function __construct(protected ModelDefinition $modelDefinition)
    {
    }

    public function oneToMany(ForeignKeyDefinition $foreignKeyDefinition): ClassMethod
    {
        return new ClassMethod(
            name    : Str::camel($foreignKeyDefinition->foreignModel()),
            subNodes: [
                'flags'      => Modifiers::PUBLIC,
                'returnType' => new Name(name: 'BelongsTo'),
                'stmts'      => [
                    new Return_(expr: new MethodCall(
                        var : new Variable(name: 'this'),
                        name: new Identifier(name: 'belongsTo'),
                        args: [
                            new Arg(value: new ClassConstFetch(
                                class: new Name(name: $foreignKeyDefinition->foreignModel()),
                                name : new Identifier(name: 'class')
                            )),
                        ],
                    )),
                ],
            ]
        );
    }

    public function oneToManyAnnotation(ForeignKeyDefinition $foreignKeyDefinition): PropertyAnnotation
    {
        return (new PropertyAnnotation(Str::lcfirst($foreignKeyDefinition->foreignModel())))
            ->type($foreignKeyDefinition->foreignModel());
    }

    public function getUsage(ForeignKeyDefinition|RelationDefinition $definition): string
    {
        return match ($definition->type()) {
            'HasMany'                  => HasMany::class,
            'HasOne'                   => HasOne::class,
            'one-to-many', 'BelongsTo' => BelongsTo::class,
        };
    }

    public function hasMany(RelationDefinition $relationDefinition): ClassMethod
    {
        return new ClassMethod(
            name    : Str::lcfirst(Str::camel($relationDefinition->relatedTable())),
            subNodes: [
                'flags'      => Modifiers::PUBLIC,
                'returnType' => new Name(name: 'HasMany'),
                'stmts'      => [
                    new Return_(expr: new MethodCall(
                        var : new Variable(name: 'this'),
                        name: new Identifier(name: 'hasMany'),
                        args: [
                            new Arg(value: new ClassConstFetch(
                                class: new Name(name: $relationDefinition->relatedModel()),
                                name : new Identifier(name: 'class')
                            )),
                        ],
                    )),
                ],
            ]
        );
    }

    public function hasOne(ForeignKeyDefinition $foreignKeyDefinition): ClassMethod
    {
        return ClassMethodBuilder::createPublic(name: Str::lcfirst(Str::camel($foreignKeyDefinition->foreignModel())))
            ->returnType('HasOne')
            ->stmt(
                ReturnBuilder::create(
                    expr: MethodCallBuilder::create(
                        var : 'this',
                        name: 'hasOne',
                        args: [
                            ClassConstFetchBuilder::create(class: $foreignKeyDefinition->foreignModel(), name: 'class'),
                        ]
                    )
                )
            )->object();
    }

    public function belongsTo(ForeignKeyDefinition|RelationDefinition $definition): ClassMethod
    {
        $model = $definition instanceof ForeignKeyDefinition
            ? $definition->foreignModel()
            : $definition->relatedModel();

        return ClassMethodBuilder::createPublic(Str::lcfirst(Str::camel($model)))
            ->returnType('BelongsTo')
            ->stmt(
                ReturnBuilder::create(
                    expr: MethodCallBuilder::create(
                        var : 'this',
                        name: 'belongsTo',
                        args: [ClassConstFetchBuilder::create(class: $model, name: 'class')]
                    )
                )
            )->object();
    }

    public function hasManyAnnotation(ForeignKeyDefinition|RelationDefinition $definition): PropertyAnnotation
    {
        if ($definition instanceof ForeignKeyDefinition) {
            $name = Str::lcfirst(Str::camel($definition->foreignModel()));
            $type = $definition->foreignModel();
        } else {
            $name = $definition->relatedTable();
            $type = 'Collection|' . $definition->relatedModel() . '[]';
        }

        return (new PropertyAnnotation(name: Str::camel($name)))->type($type);
    }

    public function hasOneAnnotation(ForeignKeyDefinition|RelationDefinition $definition): PropertyAnnotation
    {
        if ($definition instanceof ForeignKeyDefinition) {
            $name = Str::lcfirst(Str::camel($definition->foreignModel()));
            $type = $definition->foreignModel();
        } else {
            $name = $definition->relatedTable();
            $type = $definition->relatedModel();
        }

        return (new PropertyAnnotation(name: $name))->type($type);
    }

    public function belongsToAnnotation(ForeignKeyDefinition|RelationDefinition $definition): PropertyAnnotation
    {
        if ($definition instanceof ForeignKeyDefinition) {
            $name = Str::lcfirst(Str::camel($definition->foreignModel()));
            $type = $definition->foreignModel();
        } else {
            $name = $definition->relatedTable();
            $type = $definition->relatedModel();
        }

        return (new PropertyAnnotation(name: $name))->type($type);
    }

    public function modelPropertyFetch(string $property): PropertyFetch
    {
        return new PropertyFetch(
            var : new Variable(Str::lcfirst($this->modelDefinition->model())),
            name: new Identifier($property)
        );
    }

    public function modelStaticCall(string $method): StaticCall
    {
        return new StaticCall(
            class: new Name($this->modelDefinition->model()),
            name : new Identifier($method)
        );
    }

    public function modelConstFetch(string $column): ClassConstFetch
    {
        return new ClassConstFetch(
            class: new Name($this->modelDefinition->model()),
            name : new Identifier(sprintf('PROPERTY_%s', Str::upper($column)))
        );
    }

    public function modelMethodCall(string $method, array $args = []): MethodCall
    {
        return new MethodCall(
            var : new Variable(Str::lcfirst($this->modelDefinition->model())),
            name: new Identifier($method),
            args: $args
        );
    }

    public function propertiesArray(array $columns): Array_
    {
        return new Array_(
            collect($columns)
                ->map(fn(string $column) => $this->modelConstFetch($column))
                ->toArray()
        );
    }
}
