<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Ast;

use Illuminate\Support\Str;
use PhpParser\Modifiers;
use PhpParser\Node\Arg;
use PhpParser\Node\Expr\Array_;
use PhpParser\Node\Expr\ClassConstFetch;
use PhpParser\Node\Expr\Closure;
use PhpParser\Node\Expr\ConstFetch;
use PhpParser\Node\Expr\MethodCall;
use PhpParser\Node\Expr\StaticCall;
use PhpParser\Node\Expr\Variable;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use PhpParser\Node\Param;
use PhpParser\Node\Scalar\Int_;
use PhpParser\Node\Scalar\String_;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\ClassMethod;
use PhpParser\Node\Stmt\Expression;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\LaravelPrototype\Definitions\ForeignKeyDefinition;
use SimKlee\LaravelPrototype\Definitions\IndexDefinition;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelWorkbench\Ast\FluentMethodBuilder;

readonly class MigrationBuilder
{
    public function __construct(private ModelDefinition $modelDefinition)
    {
    }

    public function migrationClass(): Class_
    {
        return new Class_(name: null, subNodes: [
            'extends' => new Name(name: 'Migration'),
        ]);
    }

    public function upMethod(array $stmts = []): ClassMethod
    {
        return new ClassMethod(
            name    : new Identifier(name: 'up'),
            subNodes: [
                'flags'      => Modifiers::PUBLIC,
                'returnType' => new Identifier(name: 'void'),
                'stmts'      => $stmts,
            ]
        );
    }

    public function downMethod(array $stmts = []): ClassMethod
    {
        return new ClassMethod(
            name    : new Identifier(name: 'down'),
            subNodes: [
                'flags'      => Modifiers::PUBLIC,
                'returnType' => new Identifier(name: 'void'),
                'stmts'      => $stmts,
            ]
        );
    }

    public function schemaExpression(string $method, array $stmts): Expression
    {
        return new Expression(
            expr: new StaticCall(
                class: new Name(name: 'Schema'),
                name : new Identifier(name: $method),
                args : [
                    new Arg(value: new ClassConstFetch(
                        class: new Name(name: $this->modelDefinition->model()),
                        name : new Identifier(name: 'TABLE')
                    )),
                    new Arg(value: new Closure(subNodes: [
                        'params' => [
                            new Param(
                                var : new Variable(name: 'table'),
                                type: new Name(name: 'Blueprint')
                            ),
                        ],
                        'stmts'  => $stmts,
                    ])),
                ]
            )
        );
    }

    public function fetchColumnProperty(ColumnDefinition $columnDefinition): Arg
    {
        return new Arg(value: new ClassConstFetch(
            class: new Name(name: $columnDefinition->model()),
            name : new Identifier(name: sprintf('PROPERTY_%s', Str::upper($columnDefinition->name())))
        ));
    }

    public function createOrUpdateColumn(ColumnDefinition $columnDefinition): Expression
    {
        $params = [$this->fetchColumnProperty($columnDefinition)];
        if ($columnDefinition->length() > 0) {
            $params[] = new Arg(value: new Int_(value: $columnDefinition->length()));
        }

        $builder = FluentMethodBuilder::create(
            new MethodCall(
                var : new Variable(name: 'table'),
                name: new Identifier(name: $columnDefinition->dataType()->getMethod()),
                args: $params
            )
        );
        collect([
            'autoIncrement',
            'primary',
            'unsigned',
            'nullable',
        ])->filter(fn(string $methodName) => call_user_func([
            $columnDefinition,
            $methodName,
        ]))->each(fn(string $methodName) => $builder->addMethod($methodName));

        if ($columnDefinition->default()) {
            $builder->addMethod('default', [
                match (gettype($columnDefinition->default())) {
                    'integer' => new Int_($columnDefinition->default()),
                    'boolean' => new ConstFetch(name: new Name(name: $columnDefinition->default() ? 'true' : 'false')),
                    default   => new String_($columnDefinition->default()),
                },
            ]);
        }

        return new Expression(expr: $builder->methodCall);
    }

    public function dropColumn(ColumnDefinition $columnDefinition): Expression
    {
        return new Expression(
            expr: new MethodCall(
                var : new Variable(name: 'table'),
                name: new Identifier(name: 'dropColumn'),
                args: [$this->fetchColumnProperty($columnDefinition)]
            )
        );
    }

    public function indexCreate(IndexDefinition $indexDefinition): Expression
    {
        return new Expression(
            expr: new MethodCall(
                var : new Variable(name: 'table'),
                name: new Identifier(name: $indexDefinition->type()),
                args: [
                    new Arg(value: new Array_(
                        items: collect($indexDefinition->columns())
                            ->map(fn(string $column) => $this->fetchColumnProperty($this->modelDefinition->columns->get($column)))
                            ->toArray()
                    )),
                    new Arg(value: new String_(value: $indexDefinition->name())),
                ]
            )
        );
    }

    public function dropIndex(IndexDefinition $indexDefinition): Expression
    {
        return new Expression(
            expr: new MethodCall(
                var : new Variable(name: 'table'),
                name: new Identifier(name: 'dropIndex'),
                args: [new Arg(value: new String_(value: $indexDefinition->name()))]
            )
        );
    }

    public function foreignKeyCreate(ForeignKeyDefinition $foreignKeyDefinition): Expression
    {
        $builder = FluentMethodBuilder::create(
            new MethodCall(
                var : new Variable(name: 'table'),
                name: new Identifier(name: 'foreign'),
                args: [
                    $this->fetchColumnProperty($this->modelDefinition->columns->get($foreignKeyDefinition->column())),
                    new Arg(value: new String_(value: $foreignKeyDefinition->name())),
                ]
            )
        )->addMethod(name: 'on', args: [
            new Arg(value: new ClassConstFetch(
                class: new Name(name: $foreignKeyDefinition->foreignModel()),
                name : new Identifier(name: 'TABLE')
            )),
        ])->addMethod(name: 'references', args: [
            new Arg(value: new ClassConstFetch(
                class: new Name(name: $foreignKeyDefinition->foreignModel()),
                name : new Identifier(name: sprintf('PROPERTY_%s', Str::upper($foreignKeyDefinition->foreignColumn())))
            )),
        ])->addMethod(name: 'onUpdate', args: [
            new Arg(value: new String_('cascade')),
        ])->addMethod(name: 'onDelete', args: [
            new Arg(value: new String_('cascade')),
        ]);

        return new Expression(expr: $builder->methodCall);
    }

    public function dropForeignKey(ForeignKeyDefinition $foreignKeyDefinition): Expression
    {
        return new Expression(
            expr: new MethodCall(
                var : new Variable(name: 'table'),
                name: new Identifier(name: 'dropForeign'),
                args: [new Arg(value: new String_(value: $foreignKeyDefinition->name()))]
            )
        );
    }

}
