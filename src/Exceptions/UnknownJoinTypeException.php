<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Exceptions;

use Exception;

class UnknownJoinTypeException extends Exception
{

}