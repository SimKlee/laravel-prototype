<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Exceptions;

use Exception;

class UnexpectedMatchValueException extends Exception
{

}