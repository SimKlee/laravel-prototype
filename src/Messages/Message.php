<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Messages;

class Message
{
    public const TYPE_INFO    = 'info';
    public const TYPE_NOTICE  = 'notice';
    public const TYPE_WARNING = 'warning';
    public const TYPE_ERROR   = 'error';

    public function __construct(public string $message, public ?string $type = null, public mixed $data = null)
    {

    }
}