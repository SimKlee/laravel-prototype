<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Messages;

use Illuminate\Support\Collection;

class MessageCollection
{
    private Collection $collection;

    public function __construct()
    {
        $this->collection = new Collection();
    }

    public function add(Message $message): void
    {
        $this->collection->add($message);
    }

    public function addIf(bool $condition, Message $message, Message $elseMessage = null): void
    {
        if ($condition) {
            $this->collection->add($message);
        } elseif (!is_null($elseMessage)) {
            $this->collection->add($elseMessage);
        }
    }

    public function count(): int
    {
        return $this->collection->count();
    }

    public function all(): Collection
    {
        return $this->collection;
    }

    private function filterType(string $type): Collection
    {
        return $this->collection
                   ->filter(function (Message $message) use ($type) {
                       return $message->type === $type;
                   });
    }

    public function errors(): Collection
    {
        return $this->filterType(Message::TYPE_ERROR);
    }

    public function warnings(): Collection
    {
        return $this->filterType(Message::TYPE_WARNING);
    }

    public function notices(): Collection
    {
        return $this->filterType(Message::TYPE_NOTICE);
    }

    public function infos(): Collection
    {
        return $this->filterType(Message::TYPE_INFO);
    }
}