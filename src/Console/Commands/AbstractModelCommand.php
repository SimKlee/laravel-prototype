<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use SimKlee\LaravelPrototype\Definitions\Collections\ModelDefinitionCollection;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelPrototype\Messages\Message;
use SimKlee\LaravelPrototype\Messages\MessageCollection;

use function Laravel\Prompts\confirm;
use function Laravel\Prompts\error;
use function Laravel\Prompts\select;

abstract class AbstractModelCommand extends AbstractCommand
{
    protected $signature   = 'prototype';
    protected $description = 'Abstract mode command. Not for direct usage!';

    protected ?ModelDefinitionCollection $models = null;

    public function __construct()
    {
        parent::__construct();

        $content = File::get(resource_path('json/models.json'));
        $json    = json_decode(json: $content, associative: true);
        try {
            $this->models = ModelDefinitionCollection::fromArray($json);
            #$this->models->fillRelations();
        } catch (\Exception $e) {
            error($e->getMessage());
        }

        #dump($this->models);
    }

    protected function getPayloadModels(): Collection
    {
        return $this->models->all()
            ->filter(fn(ModelDefinition $modelDefinition) => class_exists(sprintf('App\Models\%s', $modelDefinition->model())))
            ->filter(fn(ModelDefinition $modelDefinition) => $this->implementsInterface(sprintf('App\Models\%s', $modelDefinition->model()), 'App\Models\Interfaces\PayloadModelInterface'))
            ->map(fn(ModelDefinition $modelDefinition) => $modelDefinition->model())
            ->values();
    }

    protected function getRelatedModels(): Collection
    {
        return $this->models->all()
            ->filter(fn(ModelDefinition $modelDefinition) => class_exists(sprintf('App\Models\%s', $modelDefinition->model())))
            ->filter(fn(ModelDefinition $modelDefinition) => $this->implementsInterface(sprintf('App\Models\%s', $modelDefinition->model()), 'App\Models\Interfaces\HasRelationsInterface'))
            ->map(fn(ModelDefinition $modelDefinition) => $modelDefinition->model())
            ->values();
    }

    protected function implementsInterface(string $class, string $interface): bool
    {
        return in_array($interface, class_implements($class));
    }

    protected function chooseModel(array $excludes = []): string
    {
        $models = $this->models->all()
            ->reject(fn(ModelDefinition $modelDefinition) => $modelDefinition->model() === 'User')
            ->reject(fn(ModelDefinition $modelDefinition) => in_array($modelDefinition->model(), $excludes))
            ->map(fn(ModelDefinition $modelDefinition) => $modelDefinition->model())
            ->values();

        return select(label: 'Choose Model', options: $models->toArray(), scroll: $models->count());
    }

    protected function chooseModelProperty(string $model): string
    {
        $properties = $this->models->get($model)
            ->columns->withValues()
            ->map(fn(ColumnDefinition $columnDefinition) => $columnDefinition->name());

        return $this->components->choice('Choose Property', $properties->toArray());
    }

    protected function isForced(): bool
    {
        return $this->option('force');
    }

    protected function shouldWrite(string $file, string $confirmMessage): bool
    {
        if (File::exists($file) === false || $this->isForced()) {
            return true;
        }

        return confirm($confirmMessage, File::exists($file) === false);
    }

    protected function showMessages(MessageCollection $messages): void
    {
        $messages->all()
            ->each(function (Message $message) {

                $string = $message->message;
                if (is_string($message->data)) {
                    $string .= sprintf(' [%s]', $message->data);
                }

                match ($message->type) {
                    Message::TYPE_ERROR   => $this->components->error($string),
                    Message::TYPE_WARNING => $this->components->warn($string),
                    default               => $this->components->info($string),
                };
            });
    }

    abstract public function handle(): void;

}
