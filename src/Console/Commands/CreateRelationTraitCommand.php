<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelPrototype\Generators\ModelGenerator;
use SimKlee\LaravelPrototype\Generators\ModelMetaGenerator;
use SimKlee\LaravelPrototype\Generators\RelationTraitGenerator;
use SimKlee\LaravelWorkbench\Generators\ClassGenerator;
use function Laravel\Prompts\select;

class CreateRelationTraitCommand extends AbstractModelCommand
{
    protected $signature   = 'prototype:create:trait:relation {model? : Model name}
                                                              {--f|force : Overwrite existing model meta class.}';
    protected $description = 'Creates a relation trait.';

    public function handle(): void
    {
        $options = $this->models->all()
            ->filter(fn(ModelDefinition $modelDefinition) => class_exists(sprintf('App\Models\%s', $modelDefinition->model())))
            ->filter(fn(ModelDefinition $modelDefinition) => in_array('App\Models\Interfaces\PayloadModelInterface', class_implements(sprintf('App\Models\%s', $modelDefinition->model()))))
            ->map(fn(ModelDefinition $modelDefinition) => $modelDefinition->model())
            ->values();

        $payloadModel = $this->argument('model') ?? select(label: 'Model', options: $this->getPayloadModels());
        #$relatedModel = $this->argument('model') ?? select(label: 'Model', options: $this->getRelatedModels());

        $generator = new RelationTraitGenerator($this->models->get($payloadModel));
        if ($generator->write()) {
            $this->components->info(sprintf('Generated %s', $generator->getFile()));
        }

    }
}
