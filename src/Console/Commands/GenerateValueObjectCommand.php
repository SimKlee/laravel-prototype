<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands;

use Illuminate\Support\Str;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelPrototype\Generators\ModelObjectValueGenerator;

use function Laravel\Prompts\intro;
use function Laravel\Prompts\multiselect;

class GenerateValueObjectCommand extends AbstractModelCommand
{
    protected $signature   = 'prototype:generate:value-object {model? : Name of model}
                                                              {column? : Name of column}
                                                              {--all}';
    protected $description = 'Create Object Value classes for models.';

    private ?ModelDefinition $modelDefinition = null;

    public function handle(): void
    {
        intro('Generate Value Objects for Models');
        $this->modelDefinition = $this->models->get($this->argument('model') ?? $this->chooseModel());
        collect($this->getColumns())
            ->each(fn(string $column) => $this->generateModelValueObjectClass(
                $this->modelDefinition->columns->get($column)
            ));
    }

    private function getColumns(): array
    {
        if ($this->option('all')) {
            return array_keys($this->getArrayColumns());
        }

        if ($this->argument('column')) {
            return [$this->argument('column')];
        }

        return multiselect(
            label  : 'Choose columns',
            options: $this->getArrayColumns(),
            default: $this->getArrayColumns(onlyNonExisting: true),
            scroll : 20
        );
    }

    private function getArrayColumns(bool $onlyNonExisting = false): array
    {
        $columns = $this->modelDefinition->columns->all()
            ->filter(fn(ColumnDefinition $columnDefinition) => $columnDefinition->dataType()->getCastType() === 'array' && $columnDefinition->valueObject());

        if ($onlyNonExisting) {
            $columns = $columns->reject(
                fn(ColumnDefinition $columnDefinition) => class_exists($this->getValueObjectClassName($columnDefinition))
            );
        }

        return $columns
            ->mapWithKeys(fn(ColumnDefinition $columnDefinition) => [$columnDefinition->name() => $columnDefinition->name()])
            ->toArray();
    }

    /**
     * @throws \Exception
     */
    private function generateModelValueObjectClass(ColumnDefinition $columnDefinition): void
    {
        $generator = new ModelObjectValueGenerator($this->modelDefinition, $columnDefinition);
        if ($generator->write()) {
            $this->components->info(sprintf('Generated %s', $generator->getFile()));
        }
    }

    private function getValueObjectClassName(ColumnDefinition $columnDefinition, bool $withNamespace = false): string
    {
        $class = sprintf(
            'Generating App\Models\ValueObjects\%s%s',
            Str::ucfirst(Str::camel($columnDefinition->model())),
            Str::ucfirst(Str::camel($columnDefinition->name()))
        );

        if ($withNamespace) {
            return $class;
        }

        return class_basename($class);
    }
}
