<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands;

use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelPrototype\Generators\AbstractModelMetaGenerator;
use SimKlee\LaravelPrototype\Generators\ModelMetaGenerator;

use function Laravel\Prompts\intro;

class GenerateModelMetaCommand extends AbstractModelCommand
{
    protected $signature   = 'prototype:generate:meta {model? : Name of model}';
    protected $description = 'Create Meta class for models.';

    private ?ModelDefinition $modelDefinition = null;

    public function handle(): void
    {
        intro('Generate Meta classes for Models');
        $this->modelDefinition = $this->models->get($this->argument('model') ?? $this->chooseModel());

        $this->copyAbstractModelMetaClass();
        $this->generateModelMetaClass();
    }

    private function copyAbstractModelMetaClass(): void
    {
        if (class_exists('App\Models\Meta\AbstractModelMeta')) {
            return;
        }

        $generator = new AbstractModelMetaGenerator(class: 'App\Models\Meta\AbstractModelMeta');
        if ($generator->write()) {
            $this->components->info(sprintf('Generated %s', $generator->getFile()));
        }
    }

    /**
     * @throws \Exception
     */
    private function generateModelMetaClass(): void
    {
        $generator = new ModelMetaGenerator($this->modelDefinition);
        if ($generator->write()) {
            $this->components->info(sprintf('Generated %s', $generator->getFile()));
        }
    }
}
