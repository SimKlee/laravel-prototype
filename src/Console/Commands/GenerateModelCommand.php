<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands;

use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\LaravelPrototype\Generators\ModelGenerator;
use SimKlee\LaravelPrototype\Generators\ModelMetaGenerator;
use SimKlee\LaravelPrototype\Generators\ModelQueryGenerator;
use SimKlee\LaravelPrototype\Generators\ModelRepositoryGenerator;

use SimKlee\LaravelPrototype\Generators\ModelValidatorGenerator;
use function Laravel\Prompts\confirm;
use function Laravel\Prompts\intro;

class GenerateModelCommand extends AbstractModelCommand
{
    protected $signature   = 'prototype:generate:model {model?} {--all}';
    protected $description = '';

    public function handle(): void
    {
        intro('Generate Model');

        $model = $this->argument('model') ?? $this->chooseModel();

        $this->generateValueObjects($model);
        $this->generateEnumerations($model);
        $this->generateValueObjects($model);
        $this->generateEnumerations($model);
        $this->generateModelMeta($model);
        $this->generateModelRepository($model);
        $this->generateModelQuery($model);
        $this->generateModelValidator($model);
        $this->generateModelMigration($model);
        $this->generateModelTest($model);
        $this->generateModelFactory($model);
        $this->generateModel($model);
    }

    /**
     * @throws \Exception
     */
    private function generateModel(string $model): void
    {
        $generator = new ModelGenerator($this->models->get($model));
        if ($generator->write()) {
            $this->components->info(sprintf('Generated %s', $generator->getFile()));
        }
    }

    /**
     * @throws \Exception
     */
    private function generateModelMeta(string $model): void
    {
        if ($this->generateAll() || confirm(label: 'Generate Model Meta class?', default: false)) {
            $generator = new ModelMetaGenerator($this->models->get($model));
            if ($generator->write()) {
                $this->components->info(sprintf('Generated %s', $generator->getFile()));
            }
        }
    }

    /**
     * @throws \Exception
     */
    private function generateModelRepository(string $model): void
    {
        if ($this->generateAll() || confirm(label: 'Generate Model Repository class?', default: false)) {
            $generator = new ModelRepositoryGenerator($this->models->get($model));
            if ($generator->write()) {
                $this->components->info(sprintf('Generated %s', $generator->getFile()));
            }
        }
    }

    /**
     * @throws \Exception
     */
    private function generateModelQuery(string $model): void
    {
        if ($this->generateAll() || confirm(label: 'Generate Model Query class?', default: false)) {
            $generator = new ModelQueryGenerator($this->models->get($model));
            if ($generator->write()) {
                $this->components->info(sprintf('Generated %s', $generator->getFile()));
            }
        }
    }

    /**
     * @throws \Exception
     */
    private function generateModelValidator(string $model): void
    {
        if ($this->generateAll() || confirm(label: 'Generate Model Validator class?', default: false)) {
            $generator = new ModelValidatorGenerator($this->models->get($model));
            if ($generator->write()) {
                $this->components->info(sprintf('Generated %s', $generator->getFile()));
            }
        }
    }

    private function generateValueObjects(string $model): void
    {
        $columns = $this->models->get($model)->columns->all()
            ->filter(fn(ColumnDefinition $columnDefinition) => $columnDefinition->dataType()->getCastType() === 'array');

        if ($columns->isNotEmpty() && ($this->option('all') || confirm(label: 'Generate Vale Objects before?', default: false))) {
            $this->call('prototype:generate:value-object', [
                'model' => $model,
                '--all' => true,
            ]);
        }
    }

    private function generateEnumerations(string $model): void
    {
        $columns = $this->models->get($model)->columns->all()
            ->filter(fn(ColumnDefinition $columnDefinition) => is_array($columnDefinition->values()) && count($columnDefinition->values()) > 0);

        if ($columns->isNotEmpty() && ($this->option('all') || confirm(label: 'Generate Enumerations before?', default: false))) {
            $this->call('prototype:generate:enum', [
                'model' => $model,
                '--all' => true,
            ]);
        }
    }

    private function generateModelTest(string $model): void
    {
        if ($this->generateAll() || confirm(label: 'Generate Model Test class?', default: false)) {
            $this->call('prototype:generate:test:model', ['model' => $model]);
        }
    }

    private function generateModelMigration(string $model): void
    {
        if ($this->generateAll() || confirm(label: 'Generate Model Migrations?', default: false)) {
            $this->call('prototype:generate:migration', ['model' => $model, '--all' => true]);
        }
    }

    private function generateModelFactory(string $model): void
    {
        if ($this->generateAll() || confirm(label: 'Generate Model Factory?', default: false)) {
            $this->call('prototype:generate:factory', ['model' => $model]);
        }
    }

    private function generateAll(): bool
    {
        if ($this->option('no-interaction')) {
            return false;
        }

        if ($this->option('all')) {
            return true;
        }

        return false;
    }
}
