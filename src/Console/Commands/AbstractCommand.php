<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use SimKlee\LaravelPrototype\Definitions\Collections\ModelDefinitionCollection;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelPrototype\Messages\Message;
use SimKlee\LaravelPrototype\Messages\MessageCollection;

use function Laravel\Prompts\confirm;
use function Laravel\Prompts\error;
use function Laravel\Prompts\intro;
use function Laravel\Prompts\select;
use function SimKlee\LaravelDeveloperAssistant\Functions\Console\line;

abstract class AbstractCommand extends Command
{
    protected function clearScreen(): void
    {
        $this->line("\033c");
    }

    protected function intro(string $message = null, bool $clearScreen = true): void
    {
        if ($clearScreen) {
            $this->clearScreen();
        }

        if (is_null($message)) {
            $message = $this->description;
        }

        intro($message);
    }
}
