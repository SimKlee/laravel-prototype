<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands;

use SimKlee\LaravelPrototype\Generators\ModelMigrationGenerator;

class CreateModelMigrationCommand extends AbstractModelCommand
{
    protected $signature   = 'prototype:create:migration {model? : Model name}
                                                         {--f|force : Overwrite existing model migration class.}';
    protected $description = 'Creates the model migration class.';

    public function handle(): void
    {
        $model = $this->argument('model') ?? $this->chooseModel();
        $this->handleModel($model);
    }

    private function handleModel(string $model): void
    {
        $modelDefinition = $this->models->get($model);

        collect(range(1, $modelDefinition->version() ?? 1))
            ->each(function (int $version) use ($modelDefinition) {
                $generator = new ModelMigrationGenerator($modelDefinition, $version);
                if ($this->shouldWrite($generator->getFile(), sprintf('Overwrite existing model migration version for %s?', $version))) {
                    $generator->write();
                    $this->components->info($generator->getFile());
                } else {
                    $this->components->warn(sprintf('Writing model migration version %s skipped!', $version));
                }
            });
    }
}
