<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands;

use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelPrototype\Generators\ModelFactoryGenerator;
use SimKlee\LaravelPrototype\Generators\ModelQueryGenerator;

use function Laravel\Prompts\intro;

class GenerateModelFactoryCommand extends AbstractModelCommand
{
    protected $signature   = 'prototype:generate:factory {model? : Name of model}';
    protected $description = 'Create Factory class for models.';

    private ?ModelDefinition $modelDefinition = null;

    public function handle(): void
    {
        intro('Generate Factory classes for Models');
        $this->modelDefinition = $this->models->get($this->argument('model') ?? $this->chooseModel());

        $this->generateModelFactoryClass();
    }

    /**
     * @throws \Exception
     */
    private function generateModelFactoryClass(): void
    {
        $generator = new ModelFactoryGenerator($this->modelDefinition);
        if ($generator->write()) {
            $this->components->info(sprintf('Generated %s', $generator->getFile()));
        }
    }
}
