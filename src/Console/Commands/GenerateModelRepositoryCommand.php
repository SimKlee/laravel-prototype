<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands;

use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelPrototype\Generators\ModelRepositoryGenerator;

use function Laravel\Prompts\intro;

class GenerateModelRepositoryCommand extends AbstractModelCommand
{
    protected $signature   = 'prototype:generate:repository {model? : Name of model}';
    protected $description = 'Create Repository class for models.';

    private ?ModelDefinition $modelDefinition = null;

    public function handle(): void
    {
        intro('Generate Repository classes for Models');
        $this->modelDefinition = $this->models->get($this->argument('model') ?? $this->chooseModel());

        $this->generateModelRepositoryClass();
    }

    /**
     * @throws \Exception
     */
    private function generateModelRepositoryClass(): void
    {
        $generator = new ModelRepositoryGenerator($this->modelDefinition);
        if ($generator->write()) {
            $this->components->info(sprintf('Generated %s', $generator->getFile()));
        }
    }
}
