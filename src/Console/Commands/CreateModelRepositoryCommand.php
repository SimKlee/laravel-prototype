<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands;

use SimKlee\LaravelPrototype\Generators\ModelRepositoryGenerator;

class CreateModelRepositoryCommand extends AbstractModelCommand
{
    /**
     * @var string
     */
    protected $signature = 'prototype:create:repository {model? : Model name}
                                                        {--f|force : Overwrite existing model class.}';

    /**
     * @var string
     */
    protected $description = 'Creates the model repository class.';

    public function handle(): void
    {
        $model = $this->argument('model') ?? $this->chooseModel();
        $this->handleModel($model);
    }

    private function handleModel(string $model): void
    {
        $modelDefinition = $this->models->get($model);
        $generator = new ModelRepositoryGenerator($modelDefinition);
        if ($this->shouldWrite($generator->getFile(), 'Overwrite existing model repository class?')) {
            $generator->write();
            $this->components->info($generator->getFile());
        } else {
            $this->components->warn('Writing model repository class skipped!');
        }
    }

}
