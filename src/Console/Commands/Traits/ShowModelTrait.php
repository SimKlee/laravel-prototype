<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands\Traits;

use SimKlee\LaravelPrototype\Definitions\ModelDefinition;

use function Laravel\Prompts\table;

trait ShowModelTrait
{
    use ModelDefinitionTrait;

    private function showModels(): void
    {
        $headers = ['Name', 'Position'];

        table(
            $headers,
            collect($this->models->all())
                ->map(fn(ModelDefinition $modelDefinition) => [
                    $modelDefinition->model(),
                    $modelDefinition->position(),
                ])->toArray()
        );
    }
}
