<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands\Traits;

use Exception;
use Illuminate\Support\Facades\File;
use SimKlee\LaravelPrototype\Definitions\Collections\ModelDefinitionCollection;

use function Laravel\Prompts\error;

trait ModelDefinitionTrait
{
    protected ?ModelDefinitionCollection $models = null;

    public function __construct()
    {
        parent::__construct();

        $this->readModelDefinitions();
    }

    protected function readModelDefinitions(): void
    {
        $json = json_decode(
            json       : File::get(resource_path('json/models.json')),
            associative: true
        );

        try {
            $this->models = ModelDefinitionCollection::fromArray($json);
            $this->models->fillRelations();
        } catch (Exception $e) {
            error($e->getMessage());
        }
    }

    protected function saveModels(bool $readDefinitions = true): void
    {
        $this->models->fillRelations();
        File::put(resource_path('json/models.json'), json_encode(
            $this->models->toArray(),
            JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
        ));

        if ($readDefinitions) {
            $this->readModelDefinitions();
        }
    }
}
