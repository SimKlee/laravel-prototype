<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands\Traits;

use SimKlee\LaravelPrototype\Definitions\ModelDefinition;

use function Laravel\Prompts\select;
use function Laravel\Prompts\suggest;

trait ModelSelectionTrait
{
    use ModelDefinitionTrait;

    protected function chooseModel(bool $withoutLaravelModels = true): string
    {
        $models = $this->models->all()
            ->reject(fn(ModelDefinition $modelDefinition) => $withoutLaravelModels && $modelDefinition->package() === 'Laravel')
            ->mapWithKeys(fn(ModelDefinition $modelDefinition) => [
                $modelDefinition->model() => sprintf('%s (%s)', $modelDefinition->model(), $modelDefinition->position()),
            ]);

        return select(label: 'Choose Model', options: $models->toArray(), scroll: $models->count());
    }

    protected function suggestModel(bool $withoutLaravelModels = true): string
    {
        $models = $this->models->all()
            ->reject(fn(ModelDefinition $modelDefinition) => $withoutLaravelModels && $modelDefinition->package() === 'Laravel')
            ->map(fn(ModelDefinition $modelDefinition) => $modelDefinition->model());

        return suggest(label: 'Choose Model', options: $models->toArray(), scroll: $models->count());
    }
}
