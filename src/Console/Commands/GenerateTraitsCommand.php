<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands;

use Illuminate\Console\Command;
use SimKlee\LaravelPrototype\Generators\ModelQueryTraitGenerator;

class GenerateTraitsCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'prototype:generate:traits';

    /**
     * @var string
     */
    protected $description = 'Generates the traits.';

    public function handle(): void
    {
        $this->generateModelQueryTrait();
    }

    private function generateModelQueryTrait(): void
    {
        $generator = new ModelQueryTraitGenerator('App\Models\Traits\ModelQueryTrait');
        if ($generator->write()) {
            $this->components->info($generator->getFile());
        }
    }
}
