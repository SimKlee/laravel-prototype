<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands;

use Illuminate\Console\Command;

use SimKlee\LaravelPrototype\LaravelPrototypeServiceProvider;
use function Laravel\Prompts\intro;

class InstallCommand extends Command
{
    protected $signature   = 'prototype:install';
    protected $description = 'Installation instructions for package Laravel Prototype.';

    public function handle(): void
    {
        intro($this->description);
        $this->call('vendor:publish', ['--provider' => LaravelPrototypeServiceProvider::class]);
    }
}
