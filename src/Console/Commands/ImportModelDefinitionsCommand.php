<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\File;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelPrototype\Importer\ModelDefinitionsImporter;

use function Laravel\Prompts\intro;

class ImportModelDefinitionsCommand extends Command
{
    protected $signature   = 'prototype:import {file?}';
    protected $description = 'Import model definitions from database.';

    public function handle(): void
    {
        intro('Import model definitions from database');

        $importer = new ModelDefinitionsImporter();
        $importer->import();

        $modelDefinitionCollection = $importer->getModelDefinitions();

        $this->info(' Imported Models:');
        $this->components->bulletList(
            $modelDefinitionCollection->all()
                ->map(fn(ModelDefinition $modelDefinition) => $modelDefinition->model())
                ->toArray()
        );

        $content = json_encode(
            $modelDefinitionCollection->toArray(),
            JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
        );
        $file    = resource_path(sprintf('json/%s', $this->argument('file') ?? 'models_imported.json'));
        if (File::put($file, $content)) {
            $this->info(PHP_EOL . ' Written ' . $file);
        }
    }
}
