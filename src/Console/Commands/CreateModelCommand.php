<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands;

use SimKlee\LaravelPrototype\Actions\CreateModelClass;
use SimKlee\LaravelPrototype\Actions\CreateModelPropertyEnum;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelPrototype\Generators\ModelGenerator;

class CreateModelCommand extends AbstractModelCommand
{
    /**
     * @var string
     */
    protected $signature = 'prototype:create:model {model? : Model name}
                                                   {--f|force : Overwrite existing model class.}';

    /**
     * @var string
     */
    protected $description = 'Creates the model class.';

    public function handle(): void
    {
        $model = $this->argument('model') ?? $this->chooseModel();
        $this->handleModel($model);
    }

    private function handleModel(string $model): void
    {
        $modelDefinition = $this->models->get($model);
        /*
        $modelDefinition->columns
            ->withValues()
            ->each(function (ColumnDefinition $columnDefinition) use ($modelDefinition) {
                $action = new CreateModelPropertyEnum($modelDefinition, $columnDefinition);
                $action->handle();
            });
        */
        #$this->handleModelPropertyEnums($modelDefinition);
        $this->handleModelRepository($modelDefinition);
        $this->handleModelQuery($modelDefinition);
        $this->handleModelMeta($modelDefinition);
        $this->handleModelFactory($modelDefinition);
        $this->handleModelMigrations($modelDefinition);

        $generator = new ModelGenerator($modelDefinition);
        if ($this->shouldWrite($generator->getFile(), 'Overwrite existing model class?')) {
            $generator->write();
            $this->components->info($generator->getFile());
        } else {
            $this->components->warn('Writing model class skipped!');
        }
    }

    private function handleModelRepository(ModelDefinition $modelDefinition): void
    {
        $this->call('prototype:create:repository', [
            'model'   => $modelDefinition->model(),
            '--force' => $this->isForced(),
        ]);
    }

    private function handleModelQuery(ModelDefinition $modelDefinition): void
    {
        $this->call('prototype:create:query', [
            'model'   => $modelDefinition->model(),
            '--force' => $this->isForced(),
        ]);
    }

    private function handleModelMeta(ModelDefinition $modelDefinition): void
    {
        $this->call('prototype:create:meta', [
            'model'   => $modelDefinition->model(),
            '--force' => $this->isForced(),
        ]);
    }

    private function handleModelFactory(ModelDefinition $modelDefinition): void
    {
        $this->call('prototype:generate:factory', [
            'model' => $modelDefinition->model(),
        ]);
    }

    private function handleModelPropertyEnums(ModelDefinition $modelDefinition): void
    {
        $enums = $modelDefinition->columns
            ->withValues();

        if ($enums->count() > 0) {
            $this->components->info('Creating Enumerations for model');
            $enums->each(function (ColumnDefinition $columnDefinition) {
                $this->call('prototype:create:model:enum', [
                    'model'    => $columnDefinition->model(),
                    'property' => $columnDefinition->name(),
                    '--force'  => $this->isForced(),
                ]);
            });
        } else {
            $this->components->info('No Enumerations detected.');
        }
    }

    private function handleModelMigrations(?ModelDefinition $modelDefinition): void
    {
        $this->call('prototype:create:migration', [
            'model' => $modelDefinition->model(),
        ]);
    }
}
