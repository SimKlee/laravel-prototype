<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SplFileInfo;

use function Laravel\Prompts\info;

class RemoveModelCommand extends AbstractModelCommand
{
    protected                $signature       = 'prototype:remove:model {model? : Model name}';
    protected                $description     = 'Removes all model classes';
    private ?ModelDefinition $modelDefinition = null;

    public function handle(): void
    {
        $this->modelDefinition = $this->models->get($this->argument('model') ?? $this->chooseModel());

        $this->handleModel();
        $this->handleModelMeta();
        $this->handleModelQuery();
        $this->handleModelRepository();
        $this->handleModelMigrations();
    }

    private function handleModel(): void
    {
        $this->deleteFile(app_path(sprintf('Models/%s.php', $this->modelDefinition->model())));
    }

    private function handleModelMeta(): void
    {
        $this->deleteFile(app_path(sprintf('Models/Meta/%sMeta.php', $this->modelDefinition->model())));
    }

    private function handleModelRepository(): void
    {
        $this->deleteFile(app_path(sprintf('Models/Repositories/%sRepository.php', $this->modelDefinition->model())));
    }

    private function handleModelQuery(): void
    {
        $this->deleteFile(app_path(sprintf('Models/Queries/%sQuery.php', $this->modelDefinition->model())));
    }

    private function handleModelMigrations(): void
    {
        collect(File::files(database_path('migrations')))
            ->filter(fn(SplFileInfo $fileInfo) => Str::contains($fileInfo->getBasename('.php'), sprintf('_%s_table', $this->modelDefinition->table())))
            ->each(fn(SplFileInfo $fileInfo) => $this->deleteFile($fileInfo->getPathname()));
    }

    private function deleteFile(string $file): void
    {
        if (File::exists($file) && File::delete($file)) {
            info(sprintf('Deleted %s', Str::remove(base_path('/'), $file)));
        }
    }
}
