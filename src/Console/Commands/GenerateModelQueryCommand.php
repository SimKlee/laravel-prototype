<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands;

use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelPrototype\Generators\ModelQueryGenerator;

use function Laravel\Prompts\intro;

class GenerateModelQueryCommand extends AbstractModelCommand
{
    protected $signature   = 'prototype:generate:query {model? : Name of model}';
    protected $description = 'Create Query class for models.';

    private ?ModelDefinition $modelDefinition = null;

    public function handle(): void
    {
        intro('Generate Query classes for Models');
        $this->modelDefinition = $this->models->get($this->argument('model') ?? $this->chooseModel());

        $this->generateModelQueryClass();
    }

    /**
     * @throws \Exception
     */
    private function generateModelQueryClass(): void
    {
        $generator = new ModelQueryGenerator($this->modelDefinition);
        if ($generator->write()) {
            $this->components->info(sprintf('Generated %s', $generator->getFile()));
        }
    }
}
