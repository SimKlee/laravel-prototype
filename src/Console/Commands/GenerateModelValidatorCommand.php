<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands;

use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelPrototype\Generators\ModelQueryGenerator;

use SimKlee\LaravelPrototype\Generators\ModelValidatorGenerator;
use function Laravel\Prompts\intro;

class GenerateModelValidatorCommand extends AbstractModelCommand
{
    protected $signature   = 'prototype:generate:validator {model? : Name of model}';
    protected $description = 'Create Validator class for models.';

    private ?ModelDefinition $modelDefinition = null;

    public function handle(): void
    {
        intro('Generate Validator classes for Models');
        $this->modelDefinition = $this->models->get($this->argument('model') ?? $this->chooseModel());

        $this->generateModelValidatorClass();
    }

    /**
     * @throws \Exception
     */
    private function generateModelValidatorClass(): void
    {
        $generator = new ModelValidatorGenerator($this->modelDefinition);
        if ($generator->write()) {
            $this->components->info(sprintf('Generated %s', $generator->getFile()));
        }
    }
}
