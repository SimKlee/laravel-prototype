<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands;

use SimKlee\LaravelPrototype\Generators\ModelQueryGenerator;

class CreateModelQueryCommand extends AbstractModelCommand
{
    /**
     * @var string
     */
    protected $signature = 'prototype:create:query {model? : Model name}
                                                   {--f|force : Overwrite existing model class.}';

    /**
     * @var string
     */
    protected $description = 'Creates the model query class.';

    public function handle(): void
    {
        $model = $this->argument('model') ?? $this->chooseModel();
        $this->handleModel($model);
    }

    private function handleModel(string $model): void
    {
        $modelDefinition = $this->models->get($model);
        $generator = new ModelQueryGenerator($modelDefinition);
        if ($this->shouldWrite($generator->getFile(), 'Overwrite existing model query class?')) {
            $generator->write();
            $this->components->info($generator->getFile());
        } else {
            $this->components->warn('Writing model query class skipped!');
        }
    }

}
