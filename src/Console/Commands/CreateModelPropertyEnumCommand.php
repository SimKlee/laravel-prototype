<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands;

use SimKlee\LaravelPrototype\Actions\CreateModelPropertyEnum;

class CreateModelPropertyEnumCommand extends AbstractModelCommand
{
    /**
     * @var string
     */
    protected $signature = 'prototype:create:model:enum {model? : Model name}
                                                        {property? : Property name}
                                                        {--f|force : Overwrite existing model class.}';

    /**
     * @var string
     */
    protected $description = 'Creates the model property enum class.';

    public function handle(): void
    {
        $model    = $this->argument('model') ?? $this->chooseModel();
        $property = $this->argument('property') ?? $this->chooseModelProperty($model);

        $this->handleModelProperty($model, $property);
    }

    private function handleModelProperty(string $model, string $property): void
    {
        $modelDefinition = $this->models->get($model);
        $action = new CreateModelPropertyEnum($modelDefinition, $modelDefinition->columns->get($property));
        if ($this->shouldWrite($action->file(), 'Overwrite existing model property enum?')) {
            $action->overwrite(true);
            $action->handle();
        } else {
            $this->components->warn('Writing model property enum skipped!');
        }

        $this->showMessages($action->messages);
    }

}
