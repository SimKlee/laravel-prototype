<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use SimKlee\LaravelPrototype\Console\Commands\Traits\ModelDefinitionTrait;
use SimKlee\LaravelPrototype\Console\Commands\Traits\ModelSelectionTrait;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinitionSuggest;

use SimKlee\LaravelPrototype\Definitions\ForeignKeyDefinition;
use SimKlee\LaravelPrototype\Definitions\IndexDefinition;
use function Laravel\Prompts\confirm;
use function Laravel\Prompts\multiselect;
use function Laravel\Prompts\select;
use function Laravel\Prompts\suggest;
use function Laravel\Prompts\table;
use function Laravel\Prompts\text;

class ChangeModelDefinitionCommand extends AbstractCommand
{
    use ModelDefinitionTrait;
    use ModelSelectionTrait;

    protected $signature   = 'prototype:change:model {model?}';
    protected $description = 'Change a model definition';

    public function handle(): void
    {
        $choice = null;
        while ($choice !== 'exit') {
            $this->intro();
            $model  = $this->argument('model') ?? $this->suggestModel();
            $choice = select(
                label  : 'Choose Action',
                options: [
                    'change-model' => 'Change Model',
                    'add-column'   => 'Add Column',
                    'exit'         => 'EXIT',
                ]
            );
            match ($choice) {
                'add-column'   => $this->addColumn($model),
                'change-model' => $this->changeModel($model),
                default        => null,
            };
        }
    }

    private function changeModel(string $model): void
    {
        $modelDefinition = $this->models->get($model);
        $modelDefinition->package(text(label: 'Package', default: $modelDefinition->package() ?? ''));
        $modelDefinition->position((int) text(label: 'Position', default: (string) $modelDefinition->position()));
        $modelDefinition->extends(
            select(
                label  : 'Extends',
                options: config('laravel-prototype.model-definition.extends'),
                default: $modelDefinition->extends()
            )
        );
        $modelDefinition->interfaces(
            multiselect(
                label  : 'Interfaces',
                options: config('laravel-prototype.model-definition.interfaces'),
                default: $modelDefinition->interfaces()
            )
        );
        $modelDefinition->traits(
            multiselect(
                label  : 'Traits',
                options: config('laravel-prototype.model-definition.traits'),
                default: $modelDefinition->traits()
            )
        );

        $this->saveModels();
    }

    private function addIndividuell(string $model): void
    {
        $columnDefinition = new ColumnDefinition($model);
        $columnDefinition->name(
            text(label: 'Column Name')
        );
        $columnDefinition->type(
            suggest(
                label  : 'Data Type',
                options: ColumnDefinitionSuggest::typeOptions(),
                scroll : ColumnDefinitionSuggest::typeOptions()->count()
            )
        );
        if (ColumnDefinitionSuggest::hasLength($columnDefinition->type())) {
            $columnDefinition->length((int) text(label: 'Length', validate: 'integer|min:1'));
        }
        if (ColumnDefinitionSuggest::isNumber($columnDefinition->type())) {
            $columnDefinition->unsigned(confirm(label: 'Unsigned'));
        }
        $columnDefinition->nullable(confirm(label: 'Nullable', default: false));

        $this->models->get($model)->columns->add($columnDefinition);
        $this->saveModels();
    }

    private function addColumn(string $model): void
    {
        $this->showColumns($model);

        $modelDefinition = $this->models->get($model);

        $options = [
            'individuell' => 'Individuell Column',
            'foreign-key' => 'Foreign Key',
        ];
        if ($modelDefinition->columns->hasNot('id')) {
            $options['primary'] = 'Primary Key';
        }
        if ($modelDefinition->columns->hasNot('uuid')) {
            $options['uuid'] = 'UUID';
        }
        if ($modelDefinition->columns->hasNot('model') && Str::endsWith($modelDefinition->model(), 'Relation')) {
            $options['polymorph'] = 'polymorph';
        }

        $choice = select(
            label  : 'Choose Standard',
            options: $options
        );

        match ($choice) {
            'individuell' => $this->addIndividuell($model),
            'foreign-key' => $this->addForeignKey($model),
            'primary'     => $this->addPrimary($model),
            'uuid'        => $this->addUuid($model),
            'polymorph'   => $this->addPolymorph($model),
        };
    }

    private function addPrimary(string $model): void
    {
        $columnDefinition = new ColumnDefinition($model);
        $columnDefinition->position(1);
        $columnDefinition->name('id');
        $columnDefinition->type('integer');
        $columnDefinition->unsigned(true);
        $columnDefinition->primary(true);
        $columnDefinition->autoIncrement(true);

        $this->models->get($model)->columns->add($columnDefinition);
        $this->saveModels();
    }

    private function addUuid(string $model): void
    {
        $columnDefinition = new ColumnDefinition($model);
        $columnDefinition->position(2);
        $columnDefinition->name('uuid');
        $columnDefinition->type('char');
        $columnDefinition->length(36);

        $indexDefinition = new IndexDefinition($model);
        $indexDefinition->name('uuid');
        $indexDefinition->type(IndexDefinition::TYPE_UNIQUE);
        $indexDefinition->columns(['uuid']);

        $modelDefinition = $this->models->get($model);
        $modelDefinition->columns->add($columnDefinition);
        $modelDefinition->indexes->add($indexDefinition);

        $this->saveModels();
    }

    private function showColumns(string $model): void
    {
        $headers = ['Name', 'Type', 'Length', 'Unsigned'];

        table(
            $headers,
            collect($this->models->get($model)->columns->all())
                ->map(fn(ColumnDefinition $columnDefinition) => [
                    $columnDefinition->name(),
                    $columnDefinition->type(),
                    $columnDefinition->length(),
                    $columnDefinition->unsigned() ? 'yes' : 'no',
                ])
                ->toArray()
        );
    }

    private function addForeignKey(string $model): void
    {
        $modelDefinition = $this->models->get($model);

        $foreignModel = $this->models->get($this->chooseModel(withoutLaravelModels: false));

        $column = new ColumnDefinition($model);
        $column->name(sprintf('%s_id', Str::lcfirst(Str::snake($foreignModel->model()))));
        $foreignPrimary = $foreignModel->columns->get('id');
        $column->type($foreignPrimary->type());
        $column->unsigned($foreignPrimary->unsigned());

        $index = new IndexDefinition($model);
        $index->name($column->name());
        $index->type(IndexDefinition::TYPE_INDEX);
        $index->columns([$column->name()]);


        $foreignKey = new ForeignKeyDefinition($modelDefinition->model(), $modelDefinition->table(), $column->name());
        $foreignKey->name(sprintf('fk__%s__%s', $modelDefinition->table(), $column->name()));
        $foreignKey->type(ForeignKeyDefinition::TYPE_BELONGS_TO);
        $foreignKey->foreignModel($foreignModel->model());
        $foreignKey->foreignTable($foreignModel->table());
        $foreignKey->foreignColumn($column->name());

        $modelDefinition->columns->add($column);
        $modelDefinition->indexes->add($index);
        $modelDefinition->foreignKeys->add($foreignKey);

        $this->saveModels();
    }

    private function addPolymorph(string $model): void
    {
        $modelDefinition = $this->models->get($model);

        $column = new ColumnDefinition($model);
        $column->name('model');
        $column->type('string');
        $column->length(50);
        $modelDefinition->columns->add($column);

        $column = new ColumnDefinition($model);
        $column->name('model_id');
        $column->type('integer');
        $column->unsigned();
        $modelDefinition->columns->add($column);

        $column = new ColumnDefinition($model);
        $column->name('type');
        $column->type('string');
        $column->length(50);
        $column->nullable(true);
        $modelDefinition->columns->add($column);

        $column = new ColumnDefinition($model);
        $column->name('data');
        $column->type('json');
        $column->nullable(true);
        $modelDefinition->columns->add($column);

        $index = new IndexDefinition($model);
        $index->name('foreign_model');
        $index->type(IndexDefinition::TYPE_INDEX);
        $index->columns(['model_id', 'model']);
        $modelDefinition->indexes->add($index);

        $this->saveModels();
    }
}
