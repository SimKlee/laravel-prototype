<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands;

use Illuminate\Support\Facades\File;
use SimKlee\LaravelPrototype\Generators\ModelMetaGenerator;
use SimKlee\LaravelWorkbench\Generators\ClassGenerator;

class CreateModelMetaCommand extends AbstractModelCommand
{
    protected $signature   = 'prototype:create:meta {model? : Model name}
                                                    {--f|force : Overwrite existing model meta class.}';
    protected $description = 'Creates the model meta class.';

    public function handle(): void
    {
        $model = $this->argument('model') ?? $this->chooseModel();
        $this->generateAbstractModelMetaClass();
        $this->handleModel($model);
    }

    private function handleModel(string $model): void
    {
        $modelDefinition = $this->models->get($model);
        $generator       = new ModelMetaGenerator($modelDefinition);
        if ($this->shouldWrite($generator->getFile(), 'Overwrite existing model meta class?')) {
            $generator->write();
            $this->components->info($generator->getFile());
        } else {
            $this->components->warn('Writing model meta class skipped!');
        }
    }

    private function generateAbstractModelMetaClass(): void
    {
        $generator = new ClassGenerator(
            'App\Models\Meta\AbstractModelMeta',
            __DIR__ . '/../../Templates/AbstractModelMeta.php'
        );

        if (File::exists($generator->getFile()) === false) {
            $generator->write();
            $this->components->info($generator->getFile());
        } else {
            $this->components->warn('Writing abstract model meta class skipped (already exists)!');
        }
    }

}
