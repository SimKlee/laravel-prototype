<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands;

use Illuminate\Support\Str;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelPrototype\Generators\ModelEnumerationGenerator;
use SimKlee\LaravelPrototype\Generators\ModelObjectValueGenerator;

use function Laravel\Prompts\intro;
use function Laravel\Prompts\multiselect;

class GenerateEnumerationCommand extends AbstractModelCommand
{
    protected $signature   = 'prototype:generate:enum {model? : Name of model}
                                                      {column? : Name of column}
                                                      {--all}';
    protected $description = 'Create Enumeration classes for models.';

    private ?ModelDefinition $modelDefinition = null;

    public function handle(): void
    {
        intro('Generate Enumerations for Models');
        $this->modelDefinition = $this->models->get($this->argument('model') ?? $this->chooseModel());
        collect($this->getColumns())
            ->each(fn(string $column) => $this->generateModelEnumerationClass(
                $this->modelDefinition->columns->get($column)
            ));
    }

    private function getColumns(): array
    {
        if ($this->option('all')) {
            return array_keys($this->getEnumColumns());
        }

        if ($this->argument('column')) {
            return [$this->argument('column')];
        }

        return multiselect(
            label  : 'Choose columns',
            options: $this->getEnumColumns(),
            default: $this->getEnumColumns(onlyNonExisting: true),
            scroll : 20
        );
    }

    private function getEnumColumns(bool $onlyNonExisting = false): array
    {
        $columns = $this->modelDefinition->columns->all()
            ->filter(fn(ColumnDefinition $columnDefinition) => is_array($columnDefinition->values()) && count($columnDefinition->values()) > 0);

        if ($onlyNonExisting) {
            $columns = $columns->reject(
                fn(ColumnDefinition $columnDefinition) => class_exists($this->getEnumerationClassName($columnDefinition))
            );
        }

        return $columns
            ->mapWithKeys(fn(ColumnDefinition $columnDefinition) => [$columnDefinition->name() => $columnDefinition->name()])
            ->toArray();
    }

    /**
     * @throws \Exception
     */
    private function generateModelEnumerationClass(ColumnDefinition $columnDefinition): void
    {
        $generator = new ModelEnumerationGenerator($this->modelDefinition, $columnDefinition);
        if ($generator->write()) {
            $this->components->info(sprintf('Generated %s', $generator->getFile()));
        }
    }

    private function getEnumerationClassName(ColumnDefinition $columnDefinition, bool $withNamespace = false): string
    {
        $class = sprintf(
            'Generating App\Models\Enumerations\%s%sEnum',
            $columnDefinition->model(),
            Str::ucfirst(Str::camel($columnDefinition->name()))
        );

        if ($withNamespace) {
            return $class;
        }

        return class_basename($class);
    }
}
