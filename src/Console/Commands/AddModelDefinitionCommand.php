<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands;

use App\Models\Interfaces\BusinessModelInterface;
use Illuminate\Support\Str;
use SimKlee\LaravelPrototype\Builder\RelationModelDefinitionBuilder;
use SimKlee\LaravelPrototype\Console\Commands\Traits\ModelDefinitionTrait;
use SimKlee\LaravelPrototype\Console\Commands\Traits\ShowModelTrait;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\LaravelPrototype\Definitions\ForeignKeyDefinition;
use SimKlee\LaravelPrototype\Definitions\IndexDefinition;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;

use function Laravel\Prompts\confirm;
use function Laravel\Prompts\error;
use function Laravel\Prompts\multiselect;
use function Laravel\Prompts\select;
use function Laravel\Prompts\text;

class AddModelDefinitionCommand extends AbstractModelCommand
{
    use ModelDefinitionTrait;
    use ShowModelTrait;

    protected $signature   = 'prototype:add:model {--loop}';
    protected $description = 'Add a model definition';

    public function handle(): void
    {
        do {
            $this->intro();
            $this->showModels();

            $mode = select(
                label  : 'Action',
                options: [
                    'model'          => 'Model',
                    'business-model' => 'Business Model',
                    'payload-model'  => 'Payload Model',
                    'relation-model' => 'Relation Model',
                    'pivot-model'    => 'Pivot Model',
                ]
            );

            match ($mode) {
                'business-model' => $this->handleBusinessModel(),
                'payload-model'  => $this->handlePayloadModel(),
                'pivot-model'    => $this->handlePivotModel(),
                'relation-model' => $this->handleRelationModel(),
                default          => $this->handleModel(),
            };
            $this->readModelDefinitions();

        } while ($this->option('loop'));
    }

    private function handleBusinessModel(): void
    {
        $this->handleModel([
            'extends'    => 'App\Models\AbstractBusinessModel',
            'interfaces' => ['App\Models\Interfaces\BusinessModelInterface'],
            'traits'     => ['App\Models\Traits\HasProcessTrait', 'App\Models\Traits\AutoUuidSecondaryKey'],
            'timestamps' => true,
            'softDelete' => true,
            'uuid'       => true,
        ]);
    }

    private function handlePayloadModel(): void
    {
        $this->handleModel([
            'extends'    => 'App\Models\AbstractModel',
            'interfaces' => ['App\Models\Interfaces\PayloadModelInterface'],
            'traits'     => ['App\Models\Traits\AutoUuidSecondaryKey'],
            'timestamps' => true,
            'softDelete' => true,
            'uuid'       => true,
        ]);
    }

    private function handleRelationModel(): void
    {
        $relatedModels = $this->models->all()
            ->filter(fn(ModelDefinition $modelDefinition) => Str::endsWith($modelDefinition->model(), 'Relation'))
            ->map(fn(ModelDefinition $modelDefinition) => Str::replaceEnd('Relation', '', $modelDefinition->model()));

        $models = $this->models->all()
            ->reject(fn(ModelDefinition $modelDefinition) => Str::endsWith($modelDefinition->model(), 'Relation'))
            ->reject(fn(ModelDefinition $modelDefinition) => $relatedModels->contains($modelDefinition->model()))
            ->map(fn(ModelDefinition $modelDefinition) => $modelDefinition->model());

        if (confirm(label: 'Show only Business Models')) {
            $models = $models->filter(fn(string $model) => in_array(
                BusinessModelInterface::class,
                class_implements(sprintf('App\Models\%s', $model))
            ));
        }

        $model = select(label: 'Choose Model', options: $models->values()) . 'Relation';

        (new RelationModelDefinitionBuilder($model))->save();
    }

    private function handlePivotModel(): void
    {
        $firstModelDefinition  = $this->models->get($this->chooseModel());
        $secondModelDefinition = $this->models->get($this->chooseModel([$firstModelDefinition->model()]));

        $pivotModel = sprintf('%s%s', $firstModelDefinition->model(), $secondModelDefinition->model());
        $table      = sprintf('%s_%s', Str::lower(Str::snake($firstModelDefinition->model())), $secondModelDefinition->table());
        $this->handleModel([
            'model'   => $pivotModel,
            'table'   => $table,
            'extends' => 'App\Models\AbstractModel',
        ]);

        $pivotModelDefinition = $this->models->get($pivotModel);

        $column1 = $this->createPivotColumn($pivotModelDefinition, $firstModelDefinition);
        $this->createForeignKey($pivotModelDefinition, $column1, $firstModelDefinition);

        $column2 = $this->createPivotColumn($pivotModelDefinition, $secondModelDefinition);
        $this->createForeignKey($pivotModelDefinition, $column2, $secondModelDefinition);

        $this->saveModels();
    }

    private function createPivotColumn(ModelDefinition $pivotModelDefinition,
                                       ModelDefinition $modelDefinition): ColumnDefinition
    {
        $column = new ColumnDefinition($pivotModelDefinition->model());
        $column->name(sprintf('%s_id', Str::lower(Str::snake($modelDefinition->model()))));
        $column->type('integer');
        $column->unsigned(true);
        $pivotModelDefinition->columns->add($column);

        return $column;
    }

    private function createForeignKey(ModelDefinition  $modelDefinition,
                                      ColumnDefinition $columnDefinition,
                                      ModelDefinition  $foreignModelDefinition): void
    {
        $foreignKey = new ForeignKeyDefinition(
            $modelDefinition->model(),
            $modelDefinition->table(),
            $columnDefinition->name()
        );
        $foreignKey->foreignModel($foreignModelDefinition->model());
        $foreignKey->foreignTable($foreignModelDefinition->table());
        $foreignKey->foreignColumn('id');
        $foreignKey->type(ForeignKeyDefinition::TYPE_BELONGS_TO);
        $foreignKey->name(sprintf('fk__%s__%s', $modelDefinition->table(), $columnDefinition->name()));
        $modelDefinition->foreignKeys->add($foreignKey);
    }

    private function handleModel(array $settings = []): ModelDefinition
    {
        $model = text(label: 'Model Name', default: $settings['model'] ?? '', required: true);
        if ($this->models->has($model)) {
            error(sprintf('Model %s already exists', $model));
            return $this->models->get($model);
        }

        $modelDefinition = new ModelDefinition();
        $modelDefinition->model($model);

        $tableDefault = $settings['table'] ?? Str::lower(Str::snake(Str::plural($model)));
        $modelDefinition->table(text(label: 'Table Name', default: $tableDefault, required: true));
        $modelDefinition->position((int) text(label: 'Position', validate: 'required|int'));
        $modelDefinition->version(1);
        $modelDefinition->extends(
            select(
                label  : 'Extends',
                options: config('laravel-prototype.model-definition.extends'),
                default: $settings['extends'] ?? null
            )
        );
        $modelDefinition->interfaces(
            multiselect(
                label  : 'Interfaces',
                options: config('laravel-prototype.model-definition.interfaces'),
                default: $settings['interfaces'] ?? []
            )
        );
        $this->addPrimary($modelDefinition);
        if (confirm(label: 'UUID', default: $settings['uuid'] ?? false)) {
            $this->addUuid($modelDefinition);
        }
        $modelDefinition->traits(
            multiselect(
                label  : 'Traits',
                options: config('laravel-prototype.model-definition.traits'),
                default: $settings['traits'] ?? []
            )
        );
        $modelDefinition->timestamps(confirm(label: 'Timestamps', default: $settings['timestamps'] ?? false));
        $modelDefinition->softDelete(confirm(label: 'Soft Delete', default: $settings['softDelete'] ?? false));

        $this->models->add($modelDefinition);
        $this->saveModels();

        return $modelDefinition;
    }

    private function addPrimary(ModelDefinition $modelDefinition): void
    {
        $column = new ColumnDefinition($modelDefinition->model());
        $column->position(1);
        $column->name('id');
        $column->type('integer');
        $column->unsigned(true);
        $column->primary(true);
        $column->autoIncrement(true);
        $modelDefinition->columns->add($column);
    }

    private function addSoftDelete(ModelDefinition $modelDefinition): void
    {
        $column = new ColumnDefinition($modelDefinition->model());
        $column->name('deleted_at');
        $column->type('timestamp');
        $column->nullable(true);
        $modelDefinition->columns->add($column);
        $modelDefinition->softDelete(true);
    }

    private function addUuid(ModelDefinition $modelDefinition): void
    {
        $column = new ColumnDefinition($modelDefinition->model());
        $column->position(2);
        $column->name('uuid');
        $column->type('char');
        $column->length(36);
        $modelDefinition->columns->add($column);

        $index = new IndexDefinition($modelDefinition->model());
        $index->name('uuid');
        $index->type(IndexDefinition::TYPE_UNIQUE);
        $index->columns(['uuid']);
        $modelDefinition->indexes->add($index);
    }
}
