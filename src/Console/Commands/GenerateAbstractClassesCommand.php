<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands;

use Illuminate\Console\Command;

use function Laravel\Prompts\intro;
use function Laravel\Prompts\multiselect;

class GenerateAbstractClassesCommand extends Command
{
    protected     $signature   = 'prototype:generate:abstract';
    protected     $description = 'Generates several abstract classes into app namespace.';
    private array $classes     = [
        'AbstractModel'           => 'App\Models\AbstractModel',
        'AbstractModelMeta'       => 'App\Models\Meta\AbstractModelMeta',
        'AbstractModelQuery'      => 'App\Models\Queries\AbstractModelQuery',
        'AbstractJsonValueObject' => 'App\Models\ValueObjects\AbstractJsonValueObject',
    ];

    public function handle(): void
    {
        intro('Generate abstract classes into app namespace');
        $choices = multiselect(
            label  : 'Select abstract classes',
            options: array_keys($this->classes),
            scroll : 20
        );
    }
}
