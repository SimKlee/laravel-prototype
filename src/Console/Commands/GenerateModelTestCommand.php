<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands;

use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelPrototype\Generators\ModelTestGenerator;

use function Laravel\Prompts\intro;

class GenerateModelTestCommand extends AbstractModelCommand
{
    protected $signature   = 'prototype:generate:test:model {model? : Name of model}';
    protected $description = 'Create Test class for models.';

    private ?ModelDefinition $modelDefinition = null;

    /**
     * @throws \Exception
     */
    public function handle(): void
    {
        intro('Generate Meta classes for Models');
        $this->modelDefinition = $this->models->get($this->argument('model') ?? $this->chooseModel());

        $this->generateModelTestClass();
    }

    /**
     * @throws \Exception
     */
    private function generateModelTestClass(): void
    {
        $generator = new ModelTestGenerator($this->modelDefinition);
        if ($generator->write()) {
            $this->components->info(sprintf('Generated %s', $generator->getFile()));
        }
    }
}
