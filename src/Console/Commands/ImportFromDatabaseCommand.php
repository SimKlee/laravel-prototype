<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands;

use Illuminate\Console\Command;
use SimKlee\LaravelPrototype\Actions\ImportFromDatabase;
use function Laravel\Prompts\select;

class ImportFromDatabaseCommand extends Command
{
    protected $signature = 'prototype:import:database {connection? : Database connection name}';
    protected $description = 'Creates models.json. from database schema.';

    public function handle(): void
    {
        $importer = new ImportFromDatabase($this->getConnection());
        $importer->handle();
    }

    private function getConnection(): string
    {
        return $this->argument('connection')
            ?? select(
                label: 'Database Connection',
                options: array_keys(config('database.connections')),
                default: config('database.default')
            );
    }
}
