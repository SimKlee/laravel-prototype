<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Console\Commands;

use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelPrototype\Generators\ModelMigrationGenerator;

use function Laravel\Prompts\intro;
use function Laravel\Prompts\multiselect;

class GenerateModelMigrationCommand extends AbstractModelCommand
{
    protected $signature   = 'prototype:generate:migration {model? : Name of model} {version?} {--all}';
    protected $description = 'Create Migration class for models.';

    private ?ModelDefinition $modelDefinition = null;

    public function handle(): void
    {
        intro('Generate Migration classes for Models');
        $this->modelDefinition = $this->models->get($this->argument('model') ?? $this->chooseModel());

        $this->generateModelMigrationClass($this->versions());
    }

    private function versions(): array
    {
        if ($this->argument('version')) {
            return [$this->argument('version')];
        }

        if ($this->option('all')) {
            return range(1, $this->modelDefinition->version());
        }

        $options  = collect(range(1, $this->modelDefinition->version()))
            ->mapWithKeys(fn(int $version) => [$version => ModelMigrationGenerator::exists($this->modelDefinition, $version) ? $version . ' (exists)' : $version]);
        $selected = collect(range(1, $this->modelDefinition->version()))
            ->reject(fn(int $version) => ModelMigrationGenerator::exists($this->modelDefinition, $version));

        return multiselect(
            label  : 'Choose versions',
            options: $options,
            default: $selected
        );
    }

    private function generateModelMigrationClass(array $versions): void
    {
        collect($versions)->each(function (int $version) {
            $generator = new ModelMigrationGenerator($this->modelDefinition, $version);
            if ($generator->write()) {
                $this->components->info(sprintf('Generated %s', $generator::file($this->modelDefinition, $version)));
            }
        });
    }
}
