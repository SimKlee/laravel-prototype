<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Definitions;

use SimKlee\LaravelPrototype\Definitions\Formatter\RelationFormatter;
use SimKlee\LaravelPrototype\Messages\MessageCollection;

class RelationDefinition extends AbstractDefinition
{
    protected ?string $type          = null;
    protected ?string $relatedModel  = null;
    protected ?string $relatedTable  = null;
    protected ?string $relatedColumn = null;

    public function __construct(protected string $model)
    {

    }

    public function relatedModel(string $relatedModel = null): string|null
    {
        if (!is_null($relatedModel)) {
            $this->relatedModel = $relatedModel;
        }

        return $this->relatedModel;
    }

    public function relatedTable(string $relatedTable = null): string|null
    {
        if (!is_null($relatedTable)) {
            $this->relatedTable = $relatedTable;
        }

        return $this->relatedTable;
    }

    public function relatedColumn(string $column = null): string|null
    {
        if (!is_null($column)) {
            $this->relatedColumn = $column;
        }

        return $this->relatedColumn;
    }

    public function type(string $type = null): string|null
    {
        if (!is_null($type)) {
            $this->type = $type;
        }

        return $this->type;
    }

    public static function fromArray(string $model, array $definition): RelationDefinition
    {
        $relationDefinition                = new RelationDefinition($model);
        $relationDefinition->type          = $definition['type'] ?? null; // @TODO: remove
        $relationDefinition->relatedModel  = $definition['relatedModel'];
        $relationDefinition->relatedTable  = $definition['relatedTable'] ?? null;
        $relationDefinition->relatedColumn = $definition['relatedColumn'];

        return $relationDefinition;
    }

    public static function fromForeignKey(string $model, ForeignKeyDefinition $foreignKeyDefinition): RelationDefinition
    {
        $relationDefinition                = new RelationDefinition($model);
        $relationDefinition->relatedModel  = $foreignKeyDefinition->model();
        $relationDefinition->relatedTable  = $foreignKeyDefinition->table();
        $relationDefinition->relatedColumn = $foreignKeyDefinition->column();
        $relationDefinition->type          = match ($foreignKeyDefinition->type()) {
            #ForeignKeyDefinition::TYPE_HAS_ONE    => 'HasOne',
            #ForeignKeyDefinition::TYPE_HAS_MANY   => 'HasMany',
            ForeignKeyDefinition::TYPE_BELONGS_TO => 'HasMany',
        };

        return $relationDefinition;
    }

    public function toArray(): array
    {
        return [
            'type'          => $this->type,
            'relatedModel'  => $this->relatedModel,
            'relatedTable'  => $this->relatedTable,
            'relatedColumn' => $this->relatedColumn,
        ];
    }

    public function formatter(): RelationFormatter
    {
        return new RelationFormatter($this);
    }

    public function validate(): MessageCollection|bool
    {
        // TODO: Implement validate() method.
    }
}
