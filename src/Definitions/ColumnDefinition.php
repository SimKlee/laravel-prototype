<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Definitions;

use BackedEnum;
use Illuminate\Support\Str;
use SimKlee\LaravelPrototype\Definitions\Enumerations\ColumnDataType;
use SimKlee\LaravelPrototype\Definitions\Formatter\ColumnFormatter;
use SimKlee\LaravelPrototype\Exceptions\UnexpectedMatchValueException;
use SimKlee\LaravelPrototype\Messages\Message;
use SimKlee\LaravelPrototype\Messages\MessageCollection;

class ColumnDefinition extends AbstractDefinition
{
    protected int             $version       = 1;
    protected ?string         $name          = null;
    protected ?string         $type          = null;
    protected ?ColumnDataType $dataType      = null;
    protected bool            $valueObject   = false;
    protected bool            $nullable      = false;
    protected bool|null       $unsigned      = null;
    protected bool|null       $autoIncrement = null;
    protected bool|null       $primary       = null;
    protected int|null        $length        = null;
    protected int|null        $decimals      = null;
    protected int|null        $precision     = null;
    protected bool            $foreignKey    = false;
    protected mixed           $default       = null;
    protected ?array          $values        = null;
    protected ?array          $scheme        = null;
    protected ?int            $position      = null;

    public function __construct(protected string $model)
    {

    }

    public function model(): string
    {
        return $this->model;
    }

    public function name(string $name = null): string|null
    {
        if (!is_null($name)) {
            $this->name = $name;
        }

        return $this->name;
    }

    public function type(string $type = null): string|null
    {
        if (!is_null($type)) {
            $this->type     = $type;
            $this->dataType = ColumnDataType::tryFrom($type);
        }

        return $this->type;
    }

    public function dataType(): ColumnDataType|null
    {
        return $this->dataType;
    }

    public function valueObject(bool $valueObject = null): bool
    {
        if (!is_null($valueObject)) {
            $this->valueObject = $valueObject;
        }

        return $this->valueObject;
    }

    public function nullable(bool $nullable = null): bool
    {
        if (!is_null($nullable)) {
            $this->nullable = $nullable;
        }

        return $this->nullable;
    }

    public function scheme(array $scheme = null): array|null
    {
        if (!is_null($scheme)) {
            $this->scheme = $scheme;
        }

        return $this->scheme;
    }

    public function unsigned(bool $unsigned = null): bool|null
    {
        if (!is_null($unsigned)) {
            $this->unsigned = $unsigned;
        }

        return $this->unsigned;
    }

    public function autoIncrement(bool $autoIncrement = null): bool|null
    {
        if (!is_null($autoIncrement)) {
            $this->autoIncrement = $autoIncrement;
        }

        return $this->autoIncrement;
    }

    public function primary(bool $primary = null): bool|null
    {
        if (!is_null($primary)) {
            $this->primary = $primary;
        }

        return $this->primary;
    }

    public function length(int $length = null): int|null
    {
        if (!is_null($length)) {
            $this->length = $length;
        }

        return $this->length;
    }

    public function decimals(int $decimals = null): int|null
    {
        if (!is_null($decimals)) {
            $this->decimals = $decimals;
        }

        return $this->decimals;
    }

    public function precision(int $precision = null): int|null
    {
        if (!is_null($precision)) {
            $this->precision = $precision;
        }

        return $this->precision;
    }

    public function foreignKey(bool $foreignKey = null): bool
    {
        if (!is_null($foreignKey)) {
            $this->foreignKey = $foreignKey;
        }

        return $this->foreignKey;
    }

    public function default(mixed $default = null): string|float|int|bool|null
    {
        if (!is_null($default)) {
            if ($this->dataType->hasDecimals()) {
                $default = (float) $default;
            } elseif ($this->dataType->isNumber()) {
                $default = (int) $default;
            } elseif ($this->dataType->isBool()) {
                $default = (bool) $default;
            }

            $this->default = $default;
        }

        return $this->default;
    }

    public function values(array|string $values = null): array|null
    {
        if (is_string($values)) {
            $values = explode(',', $values);
        }

        if (is_array($values)) {
            $this->values = $values;
        }

        return $this->values;
    }

    public function version(int $version = null): int
    {
        if (!is_null($version)) {
            $this->version = $version;
        }

        return $this->version;
    }

    public function position(int $position = null): ?int
    {
        if (!is_null($position)) {
            $this->position = $position;
        }

        return $this->position;
    }

    /**
     * @throws UnexpectedMatchValueException
     */
    public function cast(): string
    {
        if (!is_null($this->values)) {
            return sprintf('%s%sEnum', $this->model, Str::ucfirst(Str::camel($this->name)));
        }

        return $this->dataType->getCastType();
    }

    public static function fromArray(string $model, array $definition): ColumnDefinition
    {
        $columnDefinition = new ColumnDefinition($model);
        $columnDefinition->version($definition['version'] ?? 1);
        #$columnDefinition->position($definition['position'] ?? 1);
        $columnDefinition->name($definition['name']);
        $columnDefinition->type($definition['type']);
        $columnDefinition->valueObject($definition['valueObject'] ?? false);
        $columnDefinition->nullable($definition['nullable'] ?? false);
        $columnDefinition->unsigned($definition['unsigned'] ?? null);
        $columnDefinition->autoIncrement($definition['autoIncrement'] ?? null);
        $columnDefinition->primary($definition['primary'] ?? null);
        $columnDefinition->length($definition['length'] ?? null);
        $columnDefinition->decimals($definition['decimals'] ?? null);
        $columnDefinition->precision($definition['precision'] ?? null);
        $columnDefinition->foreignKey($definition['foreignKey'] ?? false);
        $columnDefinition->default($definition['default'] ?? null);
        $columnDefinition->values($definition['values'] ?? null);
        $columnDefinition->scheme($definition['scheme'] ?? null);

        return $columnDefinition;
    }

    public function toArray(): array
    {
        return $this->removeNullAndFalseValues([
            'version'       => $this->version,
            #'position'      => $this->position,
            'name'          => $this->name,
            'type'          => $this->type,
            'valueObject'   => $this->valueObject,
            'nullable'      => $this->nullable,
            'unsigned'      => $this->unsigned,
            'autoIncrement' => $this->autoIncrement,
            'primary'       => $this->primary,
            'length'        => $this->length,
            'decimals'      => $this->decimals,
            'precision'     => $this->precision,
            'foreignKey'    => $this->foreignKey,
            'default'       => $this->default,
        ]);
    }

    public function validate(): MessageCollection|bool
    {
        $messages = new MessageCollection();

        if (is_null($this->name())) {
            $messages->add(new Message('The column has no name!', Message::TYPE_ERROR));
        }

        if (is_null($this->type())) {
            $messages->add(new Message('No data type defined!', Message::TYPE_ERROR));
        }

        if (!is_null($this->precision()) && is_null($this->decimals())) {
            $messages->add(new Message('DECIMALS must be set when PRECISION is used!', Message::TYPE_ERROR));
        }

        if (!is_null($this->decimals()) && is_null($this->precision())) {
            $messages->add(new Message('PRECISION must be set when DECIMALS is used!', Message::TYPE_ERROR));
        }

        if ($this->autoIncrement() && !$this->primary()) {
            $messages->add(new Message('Usually PRIMARY is true when AUTO_INCREMENT is set!', Message::TYPE_WARNING));
        }

        if ($this->unsigned() && !$this->dataType->isNumber()) {
            $messages->add(new Message('UNSIGNED is only used for numeric values!', Message::TYPE_ERROR));
        }

        if ($this->length() && ($this->precision() || $this->decimals())) {
            $messages->add(new Message('LENGTH needs no PRECISION or DECIMALS!', Message::TYPE_ERROR));
        }

        if ($this->nullable() && ($this->autoIncrement() || $this->primary())) {
            $messages->add(
                new Message('NULLABLE is not possible if AUTO_INCREMENT or PRIMARY is set!', Message::TYPE_ERROR)
            );
        }

        if (in_array($this->dataType, [ColumnDataType::CHAR, ColumnDataType::STRING]) && is_null($this->length())) {
            $messages->add(new Message('LENGTH must be set for data types CHAR and STRING!', Message::TYPE_ERROR));
        }

        if (Str::endsWith($this->name(), '_id') && !$this->foreignKey()) {
            $messages->add(
                new Message(sprintf('Column %s is potentially a FOREIGN_KEY', $this->name()), Message::TYPE_NOTICE)
            );
        }

        if ($messages->count() > 0) {
            return $messages;
        }

        return true;
    }

    public function formatter(): ColumnFormatter
    {
        return new ColumnFormatter($this);
    }
}
