<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Definitions\Formatter;

use Illuminate\Support\Str;
use SimKlee\LaravelPrototype\Definitions\RelationDefinition;

/**
 * @property RelationDefinition $definition
 */
class RelationFormatter extends AbstractFormatter
{
    public function varName(bool $plural = true, bool $snake = false): string
    {
        $name = $snake
            ? Str::camel($this->definition->relatedModel())
            : Str::snake($this->definition->relatedModel());

        if ($plural) {
            $name = Str::plural($name);
        }

        return Str::lcfirst($name);
    }
}