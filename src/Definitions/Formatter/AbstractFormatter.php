<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Definitions\Formatter;

use SimKlee\LaravelPrototype\Definitions\AbstractDefinition;

abstract class AbstractFormatter
{
    public function __construct(protected AbstractDefinition $definition)
    {
    }
}