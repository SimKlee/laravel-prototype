<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Definitions\Formatter;

use Illuminate\Support\Str;
use SimKlee\LaravelPrototype\Definitions\ForeignKeyDefinition;

/**
 * @property ForeignKeyDefinition $definition
 */
class ForeignKeyFormatter extends AbstractFormatter
{
    public function varName(): string
    {
        return Str::lcfirst(Str::camel($this->definition->foreignModel()));
    }

    public function methodReturnType(bool $reverse = false): string
    {
        if ($reverse) {
            return match ($this->definition->type()) {
                ForeignKeyDefinition::TYPE_ONT_TO_MANY => 'HasMany',
                ForeignKeyDefinition::TYPE_ONT_TO_ONE  => 'HasOne',
            };
        }

        return match ($this->definition->type()) {
            ForeignKeyDefinition::TYPE_ONT_TO_MANY,
            ForeignKeyDefinition::TYPE_ONT_TO_ONE => 'BelongsTo',
        };
    }
}