<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Definitions\Formatter;

use Illuminate\Support\Str;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;

/**
 * @property ModelDefinition $definition
 */
class ModelFormatter extends AbstractFormatter
{
    public function modelAsSlug(): string
    {
        return Str::slug($this->definition->model());
    }

    public function modelAsSnake(): string
    {
        return Str::lower(Str::snake($this->definition->model()));
    }
}