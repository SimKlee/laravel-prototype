<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Definitions\Formatter;

use Illuminate\Support\Str;
use SimKlee\LaravelPrototype\Definitions\IndexDefinition;

/**
 * @property IndexDefinition $definition
 */
class IndexFormatter extends AbstractFormatter
{
    public function asPropertyString(string $column, bool $withClass = false): string
    {
        if ($withClass) {
            return sprintf('%s::PROPERTY_%s', $this->definition->model(), Str::upper($column));
        }

        return sprintf('PROPERTY_%s', Str::upper($this->definition->name()));
    }
}