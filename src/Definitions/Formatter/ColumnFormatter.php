<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Definitions\Formatter;

use Illuminate\Support\Str;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\StringBuffer\StringBuffer;

/**
 * @property ColumnDefinition $definition
 */
class ColumnFormatter extends AbstractFormatter
{
    public function asPropertyString(bool $withClass = false): string
    {
        if ($withClass) {
            return sprintf('%s::PROPERTY_%s', $this->definition->model(), Str::upper($this->definition->name()));
        }

        return sprintf('PROPERTY_%s', Str::upper($this->definition->name()));
    }

    public function asVarName(): string
    {
        return Str::ucfirst(Str::camel($this->definition->name()));
    }

    public function columnAsSnake(): string
    {
        return Str::lower(Str::snake($this->definition->name()));
    }

    public function maskedDefault(bool $enumWhenPossible = true): mixed
    {
        $default = $this->definition->default();
        if (is_null($default)) {
            return null;
        }

        $enumClass = sprintf(
            'App\Models\Enumerations\%s%sEnum',
            $this->definition->model(),
            $this->asVarName()
        );
        if (class_exists($enumClass)) {
            return sprintf(
                '%s::default()->value',
                class_basename($enumClass)
            );
        }

        if (is_string($default)) {
            return sprintf("'%s'", $default);
        }

        return $default;
    }

    public function blueprint()
    {
        $default = false;
        if (!is_null($this->definition->default())) {
            $default = sprintf('->default(%s)', $this->castMigrationDefaultValue());
        }

        return StringBuffer::create('$table->')
                           ->append($this->definition->dataType()->value)
                           ->append('(')
                           ->append($this->definition->formatter()->asPropertyString(true))
                           ->appendIf(is_int($this->definition->length()), ', ' . $this->definition->length())
                           ->appendIf(is_int($this->definition->precision()), ', ' . $this->definition->precision())
                           ->appendIf(is_int($this->definition->decimals()), ', ' . $this->definition->decimals())
                           ->append(')')
                           ->appendIf($this->definition->unsigned() === true, $this->migrationNewLine() . '->unsigned()')
                           ->appendIf($this->definition->nullable() === true, $this->migrationNewLine() . '->nullable()')
                           ->appendIf($this->definition->autoIncrement() === true, $this->migrationNewLine() . '->autoIncrement()')
                           ->appendIf($default !== false, $this->migrationNewLine() . $default)
                           ->append(';')
                           ->toString();
    }

    private function castMigrationDefaultValue(): string|null
    {
        return match (gettype($this->definition->default())) {
            'boolean' => $this->definition->default() === true ? 'true' : 'false',
            'string'  => sprintf("'%s'", $this->definition->default()),
            default   => (string) $this->definition->default(),
        };
    }

    private function migrationNewLine(): string
    {
        return '';
        return PHP_EOL . "\t\t\t\t";
    }
}