<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Definitions;

use Illuminate\Support\Collection;

class ColumnDefinitionSuggest
{
    public static function typeOptions(): Collection
    {
        return collect([
            'String'         => 'string',
            'Char'           => 'char',
            'Integer'        => 'integer',
            'Big Integer'    => 'bigInteger',
            'Small Integer'  => 'smallInteger',
            'Medium Integer' => 'mediumInteger',
            'Tiny Integer'   => 'tinyInteger',
            'DateTime'       => 'dateTime',
            'Date'           => 'date',
            'Time'           => 'time',
            'Timestamp'      => 'timestamp',
            'Boolean'        => 'boolean',
            'JSON'           => 'json',
        ]);
    }

    public static function hasLength(string $type): bool
    {
        return in_array($type, [
            'string',
            'char',
        ]);
    }

    public static function isNumber(string $type): bool
    {
        return in_array($type, [
            'integer',
            'bigInteger',
            'smallInteger',
            'mediumInteger',
        ]);
    }

    public static function hasDecimals(string $type): bool
    {
        return in_array($type, [

        ]);
    }
}
