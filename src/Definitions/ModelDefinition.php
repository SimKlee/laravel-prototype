<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Definitions;

use Illuminate\Database\Eloquent\Model;
use SimKlee\LaravelPrototype\Definitions\Collections\ColumnDefinitionCollection;
use SimKlee\LaravelPrototype\Definitions\Collections\ForeignKeyDefinitionCollection;
use SimKlee\LaravelPrototype\Definitions\Collections\IndexDefinitionCollection;
use SimKlee\LaravelPrototype\Definitions\Collections\RelationDefinitionCollection;
use SimKlee\LaravelPrototype\Definitions\Formatter\ModelFormatter;
use SimKlee\LaravelPrototype\Messages\MessageCollection;

class ModelDefinition extends AbstractDefinition
{
    protected int     $version    = 1;
    protected ?int    $position   = 1;
    protected string  $extends    = Model::class;
    protected ?string $model      = null;
    protected ?string $table      = null;
    protected ?string $package    = null;
    protected bool    $timestamps = false;
    protected bool    $softDelete = false;
    protected bool    $uuid       = false;
    public array      $interfaces = [];
    public array      $traits     = [];

    public ColumnDefinitionCollection     $columns;
    public ForeignKeyDefinitionCollection $foreignKeys;
    public IndexDefinitionCollection      $indexes;
    public RelationDefinitionCollection   $relations;

    public function __construct(string $model = null, string $table = null)
    {
        $this->model($model);
        $this->table($table);
        $this->columns     = new ColumnDefinitionCollection();
        $this->foreignKeys = new ForeignKeyDefinitionCollection();
        $this->indexes     = new IndexDefinitionCollection();
        $this->relations   = new RelationDefinitionCollection();
    }

    public function version(int $version = null): int|null
    {
        if (!is_null($version)) {
            $this->version = $version;
        }

        return $this->version;
    }

    public function position(int $postion = null): int|null
    {
        if (!is_null($postion)) {
            $this->position = $postion;
        }

        return $this->position;
    }

    public function extends(string $extends = null): string|null
    {
        if (!is_null($extends)) {
            $this->extends = $extends;
        }

        return $this->extends;
    }

    public function interfaces(array $interfaces = null): array
    {
        if (!is_null($interfaces)) {
            $this->interfaces = $interfaces;
        }

        return $this->interfaces;
    }

    public function traits(array $traits = null): array
    {
        if (!is_null($traits)) {
            $this->traits = $traits;
        }

        return $this->traits;
    }

    public function model(string $model = null): string|null
    {
        if (!is_null($model)) {
            $this->model = $model;
        }

        return $this->model;
    }

    public function table(string $table = null): string|null
    {
        if (!is_null($table)) {
            $this->table = $table;
        }

        return $this->table;
    }

    public function package(string $package = null): string|null
    {
        if (!is_null($package)) {
            $this->package = $package;
        }

        return $this->package;
    }

    public function timestamps(bool $timestamps = null): bool|null
    {
        if (!is_null($timestamps)) {
            $this->timestamps = $timestamps;
        }

        return $this->timestamps;
    }

    public function softDelete(bool $softDelete = null): bool|null
    {
        if (!is_null($softDelete)) {
            $this->softDelete = $softDelete;
        }

        return $this->softDelete;
    }

    public static function fromArray(array $definition): ModelDefinition
    {
        $modelDefinition = new ModelDefinition();
        $modelDefinition->version($definition['version'] ?? 1);
        $modelDefinition->position($definition['position'] ?? 1);
        $modelDefinition->model($definition['model']);
        $modelDefinition->table($definition['table']);
        $modelDefinition->extends($definition['extends'] ?? Model::class);
        $modelDefinition->package($definition['package']);
        $modelDefinition->timestamps($definition['timestamps']);
        $modelDefinition->softDelete($definition['softDelete']);
        $modelDefinition->interfaces($definition['interfaces'] ?? []);
        $modelDefinition->traits($definition['traits'] ?? []);

        collect($definition['columns'])->each(function (array $columnDefinition) use ($modelDefinition) {
            $modelDefinition->columns->add(
                ColumnDefinition::fromArray($modelDefinition->model(), $columnDefinition)
            );
        });

        collect($definition['foreignKeys'])->each(function (array $foreignKeyDefinition) use ($modelDefinition) {
            $modelDefinition->foreignKeys->add(
                ForeignKeyDefinition::fromArray($modelDefinition->model(), $modelDefinition->table(), $foreignKeyDefinition)
            );
        });

        collect($definition['indexes'])->each(function (array $indexDefinition) use ($modelDefinition) {
            $modelDefinition->indexes->add(
                IndexDefinition::fromArray($modelDefinition->model(), $indexDefinition)
            );
        });

        collect($definition['relations'])->each(function (array $relationDefinition) use ($modelDefinition) {
            $modelDefinition->relations->add(
                RelationDefinition::fromArray($modelDefinition->model(), $relationDefinition)
            );
        });

        self::handleTimestamps($modelDefinition);
        self::handleSoftDelete($modelDefinition);

        return $modelDefinition;
    }

    /**
     * @return int[]
     */
    public function toArray(): array
    {
        $this->columns->sortByPosition();

        return [
            'model'       => $this->model,
            'version'     => $this->version,
            'position'    => $this->position,
            'table'       => $this->table,
            'extends'     => $this->extends,
            'package'     => $this->package,
            'timestamps'  => $this->timestamps,
            'softDelete'  => $this->softDelete,
            'interfaces'  => $this->interfaces,
            'traits'      => $this->traits,
            'columns'     => $this->columns->all()
                ->map(fn(ColumnDefinition $columnDefinition) => $columnDefinition->toArray())
                ->toArray(),
            'indexes'     => $this->indexes->all()
                ->map(fn(IndexDefinition $indexDefinition) => $indexDefinition->toArray())
                ->toArray(),
            'foreignKeys' => $this->foreignKeys->all()
                ->map(fn(ForeignKeyDefinition $foreignKeyDefinition) => $foreignKeyDefinition->toArray())
                ->toArray(),
            'relations'   => $this->relations->all()
                ->map(fn(RelationDefinition $relationDefinition) => $relationDefinition->toArray())
                ->toArray(),
        ];
    }

    private static function handleTimestamps(ModelDefinition $modelDefinition): void
    {
        if ($modelDefinition->timestamps()) {
            if ($modelDefinition->columns->hasNot('created_at')) {
                $modelDefinition->columns->add(ColumnDefinition::fromArray($modelDefinition->model(), [
                    'version'  => $modelDefinition->version,
                    'name'     => 'created_at',
                    'type'     => 'timestamp',
                    'nullable' => true,
                ]));
            }
            if ($modelDefinition->columns->hasNot('updated_at')) {
                $modelDefinition->columns->add(ColumnDefinition::fromArray($modelDefinition->model(), [
                    'version'  => $modelDefinition->version,
                    'name'     => 'updated_at',
                    'type'     => 'timestamp',
                    'nullable' => true,
                ]));
            }
        }
    }

    private static function handleSoftDelete(ModelDefinition $modelDefinition): void
    {
        if ($modelDefinition->softDelete()) {
            if ($modelDefinition->columns->hasNot('deleted_at')) {
                $modelDefinition->columns->add(ColumnDefinition::fromArray($modelDefinition->model(), [
                    'version'  => $modelDefinition->version,
                    'name'     => 'deleted_at',
                    'type'     => 'dateTime',
                    'nullable' => true,
                ]));
            }
        }
    }

    public function validate(): MessageCollection|bool
    {
        $messages = new MessageCollection();

        if ($messages->count() > 0) {
            return $messages;
        }

        return true;
    }

    public function formatter(): ModelFormatter
    {
        return new ModelFormatter($this);
    }

    public function getLatestVersion(): int
    {
        return max(
            $this->version,
            $this->columns->all()->max(fn(ColumnDefinition $columnDefinition) => $columnDefinition->version()),
            $this->indexes->all()->max(fn(IndexDefinition $indexDefinition) => $indexDefinition->version()),
            $this->foreignKeys->all()->max(fn(ForeignKeyDefinition $foreignKeyDefinition) => $foreignKeyDefinition->version())
        );
    }
}
