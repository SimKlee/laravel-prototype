<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Definitions\Collections;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\LaravelPrototype\Exceptions\UnexpectedMatchValueException;
use SimKlee\LaravelPrototype\Messages\Message;
use SimKlee\LaravelPrototype\Messages\MessageCollection;

class ColumnDefinitionCollection
{
    private Collection $collection;

    public function __construct()
    {
        $this->collection = new Collection();
    }

    public function add(ColumnDefinition $columnDefinition): void
    {
        $this->collection->add($columnDefinition);
    }

    public function get(string $name): ColumnDefinition|null
    {
        return $this->collection->filter(function (ColumnDefinition $columnDefinition) use ($name) {
            return $columnDefinition->name() === $name;
        })->first();
    }

    public function has(string $name): bool
    {
        return $this->collection->filter(function (ColumnDefinition $columnDefinition) use ($name) {
                return $columnDefinition->name() === $name;
            })->count() === 1;
    }

    public function hasNot(string $name): bool
    {
        return $this->has($name) === false;
    }

    public function all(): Collection
    {
        return $this->collection;
    }

    /**
     * @return Collection|ColumnDefinition[]
     */
    public function guarded(): Collection
    {
        return $this->collection->filter(function (ColumnDefinition $columnDefinition) {
            return $columnDefinition->primary() ||
                $columnDefinition->autoIncrement() ||
                $columnDefinition->name() === 'uuid';
        });
    }

    public function sortByPosition(): void
    {
        $this->collection = $this->collection
            ->sortBy(fn(ColumnDefinition $columnDefinition) => $columnDefinition->position());
    }

    /**
     * @return Collection|ColumnDefinition[]
     */
    public function dates(): Collection
    {
        return $this->collection->filter(function (ColumnDefinition $columnDefinition) {
            return !is_null($columnDefinition->dataType()) && $columnDefinition->dataType()->isDate();
        });
    }

    /**
     * @return Collection|ColumnDefinition[]
     */
    public function required(): Collection
    {
        return $this->collection->filter(function (ColumnDefinition $columnDefinition) {
            return $columnDefinition->nullable() === false;
        });
    }

    /**
     * @return Collection|ColumnDefinition[]
     */
    public function defaults(): Collection
    {
        return $this->collection->filter(function (ColumnDefinition $columnDefinition) {
            return !is_null($columnDefinition->default());
        });
    }

    /**
     * @return Collection|ColumnDefinition[]
     */
    public function nullables(bool $withoutTimestampsAndSoftDelete = true): Collection
    {
        $ignore = false;
        if ($withoutTimestampsAndSoftDelete) {
            $ignore = [
                'created_at',
                'updated_at',
                'deleted_at',
            ];
        }

        return $this->collection->filter(function (ColumnDefinition $columnDefinition) use ($ignore) {
            return $columnDefinition->nullable() &&
                ($ignore === false || !in_array($columnDefinition->name(), $ignore));
        });
    }

    /**
     * @return Collection|ColumnDefinition[]
     */
    public function unsigned(): Collection
    {
        return $this->collection->filter(function (ColumnDefinition $columnDefinition) {
            return $columnDefinition->unsigned();
        });
    }

    /**
     * @return ?ColumnDefinition
     */
    public function primary(): ?ColumnDefinition
    {
        return $this->collection->filter(function (ColumnDefinition $columnDefinition) {
            return $columnDefinition->primary();
        })->first();
    }

    /**
     * @return Collection|ColumnDefinition[]
     */
    public function hidden(): Collection
    {
        return $this->collection->filter(function (ColumnDefinition $columnDefinition) {
            return $columnDefinition->primary();
        });
    }

    /**
     * @return Collection|ColumnDefinition[]
     */
    public function withValues(): Collection
    {
        return $this->collection->filter(function (ColumnDefinition $columnDefinition) {
            return !is_null($columnDefinition->values());
        });
    }

    /**
     * @return Collection|ColumnDefinition[]
     */
    public function withDecimals(): Collection
    {
        return $this->collection->filter(function (ColumnDefinition $columnDefinition) {
            return !is_null($columnDefinition->decimals());
        });
    }

    /**
     * @return Collection|ColumnDefinition[]
     */
    public function withLength(): Collection
    {
        return $this->collection->filter(function (ColumnDefinition $columnDefinition) {
            return !is_null($columnDefinition->length());
        });
    }

    public function enumerations(): Collection
    {
        return $this->collection
            ->filter(function (ColumnDefinition $columnDefinition) {
                try {
                    return Str::endsWith($columnDefinition->cast(), 'Enum');
                } catch (UnexpectedMatchValueException $e) {
                    Log::error($e->getMessage());
                    return false;
                }
            })->map(function (ColumnDefinition $columnDefinition) {
                return sprintf('App\Models\Enumerations\%s', $columnDefinition->cast());
            });
    }

    public function validate(): MessageCollection|bool
    {
        $messages = new MessageCollection();

        $autoIncrementCount = $this->collection->filter(function (ColumnDefinition $columnDefinition) {
            return $columnDefinition->autoIncrement();
        })->count();
        if ($autoIncrementCount > 1) {
            $messages->add(new Message('A table can have only one AUTO_INCREMENT!', Message::TYPE_ERROR));
        }

        $primaryCount = $this->collection->filter(function (ColumnDefinition $columnDefinition) {
            return $columnDefinition->primary();
        })->count();
        if ($primaryCount > 1) {
            $messages->add(new Message('A table can have only one PRIMARY!', Message::TYPE_ERROR));
        } elseif ($primaryCount === 0) {
            $messages->add(new Message('A table should have a PRIMARY!', Message::TYPE_WARNING));
        }

        $names = $this->collection->map(function (ColumnDefinition $columnDefinition) {
            return $columnDefinition->name();
        });
        if ($names->count() !== $names->unique()->count()) {
            $messages->add(new Message('Each column must have a unique name!', Message::TYPE_ERROR));
        }

        if ($messages->count() > 0) {
            return $messages;
        }

        return true;
    }
}
