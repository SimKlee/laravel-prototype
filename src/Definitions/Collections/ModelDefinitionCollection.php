<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Definitions\Collections;

use Exception;
use Illuminate\Support\Collection;
use SimKlee\LaravelPrototype\Definitions\ForeignKeyDefinition;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelPrototype\Definitions\RelationDefinition;
use SimKlee\LaravelPrototype\Messages\MessageCollection;

class ModelDefinitionCollection
{
    private Collection $collection;

    public function __construct()
    {
        $this->collection = new Collection();
    }

    public function add(ModelDefinition $modelDefinition): void
    {
        if ($this->hasNot($modelDefinition->model())) {
            $this->collection->add($modelDefinition);
        }
    }

    public function has(string $model): bool
    {
        return $this->collection
            ->filter(fn(ModelDefinition $modelDefinition) => $modelDefinition->model() === $model)
            ->isNotEmpty();
    }

    public function hasNot(string $model): bool
    {
        return $this->has($model) === false;
    }

    public function get(string $model): ModelDefinition|null
    {
        return $this->collection->filter(function (ModelDefinition $modelDefinition) use ($model) {
            return $modelDefinition->model() === $model;
        })->first();
    }

    public function all(): Collection
    {
        return $this->collection;
    }

    /**
     * @throws Exception
     */
    public static function fromArray(array $data): ModelDefinitionCollection
    {
        $collection = new ModelDefinitionCollection();

        if (!isset($data['ModelConfiguration']['models'])) {
            throw new Exception('Missing models in data');
        }

        collect($data['ModelConfiguration']['models'])->each(function (array $model) use ($collection) {
            $collection->add(ModelDefinition::fromArray($model));
        });

        return $collection;
    }

    public function getRelationsForModel(string $model): void
    {
        $modelDefinition = $this->get($model);
        $this->collection
            ->filter(fn(ModelDefinition $definition) => $definition->model() !== $modelDefinition->model())
            ->each(function (ModelDefinition $definition) use ($modelDefinition) {
                $definition->foreignKeys->all()
                    ->filter(fn(ForeignKeyDefinition $foreignKeyDefinition) => $modelDefinition->model() === $foreignKeyDefinition->foreignModel())
                    ->each(fn(ForeignKeyDefinition $foreignKeyDefinition) => $modelDefinition->relations->add(
                        RelationDefinition::fromForeignKey($modelDefinition->model(), $foreignKeyDefinition)
                    ));
            });
    }

    public function resetRelations(): void
    {
        $this->collection
            ->each(fn(ModelDefinition $modelDefinition) => $modelDefinition->relations = new RelationDefinitionCollection());
    }

    public function fillRelations(bool $reset = false): void
    {
        if ($reset) {
            $this->resetRelations();
        }

        $this->collection
            ->each(fn(ModelDefinition $modelDefinition) => $this->getRelationsForModel($modelDefinition->model()));
    }

    public function validate(): MessageCollection|bool
    {
        $messages = new MessageCollection();

        if ($messages->count() > 0) {
            return $messages;
        }

        return true;
    }

    private function sortByPosition(): void
    {
        $this->collection = $this->collection
            ->sortBy(fn(ModelDefinition $modelDefinition) => $modelDefinition->position())
            ->values();
    }

    public function toArray(): array
    {
        $this->sortByPosition();

        return [
            'ModelConfiguration' => [
                'version' => 1,
                'models'  => $this->collection
                    ->map(fn(ModelDefinition $modelDefinition) => $modelDefinition->toArray())
                    ->toArray(),
            ],
        ];
    }
}
