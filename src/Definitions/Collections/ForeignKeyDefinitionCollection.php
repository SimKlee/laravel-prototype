<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Definitions\Collections;

use Illuminate\Support\Collection;
use SimKlee\LaravelPrototype\Definitions\ForeignKeyDefinition;
use SimKlee\LaravelPrototype\Messages\MessageCollection;

class ForeignKeyDefinitionCollection
{
    private Collection $collection;

    public function __construct()
    {
        $this->collection = new Collection();
    }

    public function add(ForeignKeyDefinition $foreignKeyDefinition): void
    {
        $this->collection->add($foreignKeyDefinition);
    }

    public function get(string $name): ForeignKeyDefinition|null
    {
        return $this->collection->filter(function (ForeignKeyDefinition $foreignKeyDefinition) use ($name) {
            return $foreignKeyDefinition->name() === $name;
        })->first();
    }

    public function has(string $name): bool
    {
        return $this->collection->filter(function (ForeignKeyDefinition $foreignKeyDefinition) use ($name) {
                return $foreignKeyDefinition->name() === $name;
            })->count() === 1;
    }

    public function all(): Collection
    {
        return $this->collection;
    }

    public function count(): int
    {
        return $this->collection->count();
    }

    public function getUsedRelations(): Collection
    {
        $classes = [];
        $this->collection->each(function (ForeignKeyDefinition $foreignKeyDefinition) use (&$classes) {
            $relationClass = sprintf(
                'Illuminate\Database\Eloquent\Relations\%s',
                $foreignKeyDefinition->formatter()->methodReturnType()
            );
            if (!in_array($relationClass, $classes)) {
                $classes[] = $relationClass;
            }
        });

        return collect($classes);
    }

    public function getUsedModels(): Collection
    {
        $classes = [];
        $this->collection->each(function (ForeignKeyDefinition $foreignKeyDefinition) use (&$classes) {
            $modelClass = sprintf(
                'App\Models\%s',
                $foreignKeyDefinition->foreignModel()
            );
            if (!in_array($modelClass, $classes)) {
                $classes[] = $modelClass;
            }
        });

        return collect($classes)->unique();
    }

    public function validate(): MessageCollection|bool
    {
        $messages = new MessageCollection();

        if ($messages->count() > 0) {
            return $messages;
        }

        return true;
    }
}