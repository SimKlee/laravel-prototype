<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Definitions\Collections;

use Illuminate\Support\Collection;
use SimKlee\LaravelPrototype\Definitions\IndexDefinition;
use SimKlee\LaravelPrototype\Messages\MessageCollection;

class IndexDefinitionCollection
{
    private Collection $collection;

    public function __construct()
    {
        $this->collection = new Collection();
    }

    public function add(IndexDefinition $indexDefinition): void
    {
        $this->collection->add($indexDefinition);
    }

    public function get(string $name): IndexDefinition|null
    {
        return $this->collection->filter(function (IndexDefinition $indexDefinition) use ($name) {
            return $indexDefinition->name() === $name;
        })->first();
    }

    public function count(): int
    {
        return $this->collection->count();
    }

    public function has(string $name): bool
    {
        return $this->collection->filter(function (IndexDefinition $indexDefinition) use ($name) {
                return $indexDefinition->name() === $name;
            })->count() === 1;
    }

    public function all(): Collection
    {
        return $this->collection;
    }

    public function validate(): MessageCollection|bool
    {
        $messages = new MessageCollection();

        if ($messages->count() > 0) {
            return $messages;
        }

        return true;
    }
}