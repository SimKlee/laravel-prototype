<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Definitions\Collections;

use Illuminate\Support\Collection;
use SimKlee\LaravelPrototype\Definitions\RelationDefinition;
use SimKlee\LaravelPrototype\Messages\MessageCollection;

class RelationDefinitionCollection
{
    private Collection $collection;

    public function __construct()
    {
        $this->collection = new Collection();
    }

    public function add(RelationDefinition $relationDefinition): void
    {
        if ($this->has($relationDefinition->relatedTable(), $relationDefinition->relatedColumn())) {
            return;
        }

        $this->collection->add($relationDefinition);
    }

    public function get(string $relatedTable, string $relatedColumn): RelationDefinition|null
    {
        return $this->collection
            ->filter(fn(RelationDefinition $relationDefinition) => $relationDefinition->relatedTable() === $relatedTable
                && $relationDefinition->relatedColumn() === $relatedColumn
            )->first();
    }

    public function has(string $relatedTable, string $relatedColumn): bool
    {
        return $this->collection
            ->filter(fn(RelationDefinition $relationDefinition) => $relationDefinition->relatedTable() === $relatedTable
                && $relationDefinition->relatedColumn() === $relatedColumn
            )->isNotEmpty();
    }

    public function all(): Collection
    {
        return $this->collection;
    }

    public function count(): int
    {
        return $this->collection->count();
    }

    public function getUsedClasses(): Collection
    {
        $classes = [];
        $this->collection->each(function (RelationDefinition $relationDefinition) use (&$classes) {
            $relationClass = sprintf(
                'Illuminate\Database\Eloquent\Relations\%s',
                $relationDefinition->type()
            );
            if (!in_array($relationClass, $classes)) {
                $classes[] = $relationClass;
            }
        });

        return collect($classes);
    }

    public function validate(): MessageCollection|bool
    {
        $messages = new MessageCollection();

        if ($messages->count() > 0) {
            return $messages;
        }

        return true;
    }
}
