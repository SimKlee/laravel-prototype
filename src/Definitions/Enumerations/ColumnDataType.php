<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Definitions\Enumerations;

use SimKlee\LaravelPrototype\Exceptions\UnexpectedMatchValueException;

enum ColumnDataType: string
{
    case BigInteger    = 'bigInteger';
    case Binary        = 'binary';
    case Boolean       = 'boolean';
    case Char          = 'char';
    case Date          = 'date';
    case DateTime      = 'dateTime';
    case Decimal       = 'decimal';
    case Double        = 'double';
    case Enum          = 'enum';
    case Float         = 'float';
    case Integer       = 'integer';
    case Json          = 'json';
    case LongText      = 'longText';
    case MediumInteger = 'mediumInteger';
    case MediumText    = 'mediumText';
    case SmallInteger  = 'smallInteger';
    case TinyInteger   = 'tinyInteger';
    case String        = 'string';
    case Text          = 'text';
    case Time          = 'time';
    case Timestamp     = 'timestamp';

    public function getMethod(): string
    {
        return $this->value;
    }

    /**
     * @throws UnexpectedMatchValueException
     */
    public function getCastType(): string
    {
        return match ($this) {
            ColumnDataType::BigInteger,
            ColumnDataType::Integer,
            ColumnDataType::MediumInteger,
            ColumnDataType::SmallInteger,
            ColumnDataType::TinyInteger => 'int',
            ColumnDataType::Binary,
            ColumnDataType::Char,
            ColumnDataType::LongText,
            ColumnDataType::MediumText,
            ColumnDataType::String,
            ColumnDataType::Text        => 'string',
            ColumnDataType::Boolean     => 'boolean',
            ColumnDataType::Date        => 'date',
            ColumnDataType::Time        => 'time',
            ColumnDataType::DateTime,
            ColumnDataType::Timestamp   => 'datetime',
            ColumnDataType::Decimal,
            ColumnDataType::Double,
            ColumnDataType::Float       => 'float',
            ColumnDataType::Json        => 'array',
            default                     => throw new UnexpectedMatchValueException(
                sprintf('%s::getCastType(): %s', self::class, $this->value)
            ),
        };
    }

    /**
     * @throws UnexpectedMatchValueException
     */
    public function getSqlType(): string
    {
        return match ($this) {
            ColumnDataType::BigInteger    => 'bigint',
            ColumnDataType::Integer       => 'int',
            ColumnDataType::MediumInteger => 'mediumint',
            ColumnDataType::SmallInteger  => 'smallint',
            ColumnDataType::TinyInteger   => 'tinyint',
            ColumnDataType::Binary        => 'binary',
            ColumnDataType::Char          => 'char',
            ColumnDataType::LongText      => 'longtext',
            ColumnDataType::MediumText    => 'mediumtext',
            ColumnDataType::String        => 'varchar',
            ColumnDataType::Text          => 'text',
            ColumnDataType::Boolean       => 'tinyint',
            ColumnDataType::Date          => 'date',
            ColumnDataType::DateTime      => 'datetime',
            ColumnDataType::Time          => 'time',
            ColumnDataType::Timestamp     => 'timestamp',
            ColumnDataType::Decimal       => 'decimal',
            ColumnDataType::Double        => 'double',
            ColumnDataType::Float         => 'float',
            ColumnDataType::Json          => 'json',
            default                       => throw new UnexpectedMatchValueException(
                sprintf('%s::getSqlType(): %s', self::class, $this->value)
            ),
        };
    }

    public function isNumber(): bool
    {
        return in_array($this, [
            ColumnDataType::BigInteger,
            ColumnDataType::Integer,
            ColumnDataType::MediumInteger,
            ColumnDataType::SmallInteger,
            ColumnDataType::TinyInteger,
            ColumnDataType::Decimal,
            ColumnDataType::Double,
            ColumnDataType::Float,
        ]);
    }

    public function isString(): bool
    {
        return in_array($this, [
            ColumnDataType::Char,
            ColumnDataType::String,
            ColumnDataType::Text,
            ColumnDataType::MediumText,
            ColumnDataType::LongText,
            ColumnDataType::Binary,
        ]);
    }


    public function isText(): bool
    {
        return in_array($this, [
            ColumnDataType::Text,
            ColumnDataType::MediumText,
            ColumnDataType::LongText,
            ColumnDataType::Binary,
        ]);
    }

    public function isJson(): bool
    {
        return $this === ColumnDataType::Json;
    }

    public function isBool(): bool
    {
        return $this === ColumnDataType::Boolean;
    }

    public function isDate(bool $strict = false): bool
    {
        if ($strict) {
            return $this === ColumnDataType::Date;
        }

        return in_array($this, [
            ColumnDataType::Date,
            ColumnDataType::DateTime,
            ColumnDataType::Time,
            ColumnDataType::Timestamp,
        ]);
    }

    public function isTime(): bool
    {
        return $this === ColumnDataType::Time;
    }

    public function hasLength(): bool
    {
        return in_array($this, [
            ColumnDataType::Char,
            ColumnDataType::String,
        ]);
    }

    public function hasDecimals(): bool
    {
        return in_array($this, [
            ColumnDataType::Decimal,
            ColumnDataType::Double,
        ]);
    }

    public function hasPrecision(): bool
    {
        return in_array($this, [
            ColumnDataType::Decimal,
            ColumnDataType::Double,
        ]);
    }

}
