<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Definitions\Enumerations;

use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;

enum InputType: string
{
    case INPUT_CHECKBOX = 'input::checkbox';
    case INPUT_COLOR = 'input::color';
    case INPUT_DATE = 'input::date';
    case INPUT_DATETIME = 'input::datetime';
    case INPUT_EMAIL = 'input::email';
    case INPUT_FILE = 'input::file';
    case INPUT_HIDDEN = 'input::hidden';
    case INPUT_MONTH = 'input::month';
    case INPUT_NUMBER = 'input::number';
    case INPUT_PASSWORD = 'input::password';
    case INPUT_RADIO = 'input::radio';
    case INPUT_RANGE = 'input::range';
    case INPUT_TEL = 'input::tel';
    case INPUT_STRING = 'input::text';
    case INPUT_TEXT = 'input::textarea';
    case INPUT_TIME = 'input::time';
    case INPUT_URL = 'input::url';
    case INPUT_WEEK = 'input::week';
    case SELECT_SINGLE = 'select::single';
    case SELECT_MULTIPLE = 'select::multiple';
    case JSON = 'json';
    case DATALIST = 'datalist';

    public static function fromColumnDefinition(ColumnDefinition $definition): InputType
    {
        if ($definition->primary()) {
            return self::INPUT_NUMBER;
        }

        if ($definition->foreignKey()) {
            return self::SELECT_SINGLE;
        }

        if ($definition->dataType()->isBool()) {
            return self::INPUT_CHECKBOX;
        }

        if ($definition->dataType()->isDate(strict: true)) {
            return self::INPUT_DATE;
        }

        if ($definition->dataType()->isTime()) {
            return self::INPUT_TIME;
        }

        if ($definition->dataType()->isDate(strict: false)) {
            return self::INPUT_DATETIME;
        }

        if ($definition->dataType()->isText()) {
            return self::INPUT_TEXT;
        }

        if ($definition->dataType()->isJson()) {
            return self::JSON;
        }

        if ($definition->dataType()->isNumber()) {
            return self::INPUT_NUMBER;
        }

        return self::INPUT_STRING;
    }

}