<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Definitions\Class;

use Illuminate\Support\Collection;
use SimKlee\LaravelPrototype\Exceptions\NotSupportedException;

class BackedEnumDefinition extends ClassDefinition
{
    public const TYPE_STRING = 'string';
    public const TYPE_INT = 'int';

    protected string $type = 'string';
    protected Collection $cases;

    public function __construct(protected string $class)
    {
        parent::__construct($this->class);

        $this->cases = new Collection();
    }

    /**
     * @throws NotSupportedException
     */
    public function extends(string $class = null): string|null
    {
        throw new NotSupportedException('Extending is not allowed in enums!');
    }

    public function type(string $type = null): string
    {
        if (!is_null($type)) {
            $this->type = $type;
        }

        return $this->type;
    }

    public function addCase(string $case, $value): void
    {
        if ($this->cases->has($case) === false) {
            $this->cases->put($case, $value);
        }
    }

    public function cases(): Collection
    {
        return $this->cases;
    }
}