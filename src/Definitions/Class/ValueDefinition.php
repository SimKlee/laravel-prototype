<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Definitions\Class;

class ValueDefinition
{
    public const TYPE_AS_CONST = 'const';

    public function __construct(public mixed $value, public mixed $type = null)
    {

    }

    public function value(bool $masked = true): mixed
    {
        if ($this->type === self::TYPE_AS_CONST) {
            return $this->value;
        }

        if (is_string($this->value) && $masked) {
            return sprintf("'%s'", $this->value);
        }

        return $this->value;
    }
}