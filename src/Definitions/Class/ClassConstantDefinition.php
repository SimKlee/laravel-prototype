<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Definitions\Class;

class ClassConstantDefinition
{
    public const VISIBILITY_PUBLIC    = 'public';
    public const VISIBILITY_PROTECTED = 'protected';
    public const VISIBILITY_PRIVATE   = 'private';

    public function __construct(public string $name,
                                public mixed  $value,
                                public string $visibility = self::VISIBILITY_PUBLIC)
    {

    }

    public function value(): mixed
    {
        if (is_string($this->value)) {
            return sprintf("'%s'", $this->value);
        }

        return $this->value;
    }
}