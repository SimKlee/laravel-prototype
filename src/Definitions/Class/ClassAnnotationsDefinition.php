<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Definitions\Class;

use Illuminate\Support\Collection;

class ClassAnnotationsDefinition
{
    protected Collection $properties;
    protected Collection $methods;

    public function __construct()
    {
        $this->properties = new Collection();
        $this->methods    = new Collection();
    }

    public function addProperty(string $name, string|array $type): void
    {
        $this->properties->add((object) [
            'name' => $name,
            'type' => $type,
        ]);
    }

    public function addMethod(string       $name,
                              string|array $returnType,
                              string       $signature = null,
                              bool         $isStatic = false): void
    {
        $this->methods->add((object) [
            'name'       => $name,
            'returnType' => $returnType,
            'signature'  => $signature,
            'static'     => $isStatic,
        ]);
    }

    public function properties(): Collection
    {
        return $this->properties;
    }

    public function methods(): Collection
    {
        return $this->methods;
    }
}