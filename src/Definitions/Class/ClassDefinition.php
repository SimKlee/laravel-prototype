<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Definitions\Class;

use Illuminate\Support\Collection;

class ClassDefinition
{
    protected ?string                 $namespace = null;
    protected ?string                 $extends   = null;
    protected Collection              $implements;
    protected Collection              $uses;
    protected Collection              $traits;
    protected Collection              $properties;
    protected Collection              $constants;
    protected Collection              $annotations;
    public ClassAnnotationsDefinition $classAnnotationsDefinition;

    public function __construct(protected string $class)
    {
        $this->implements                 = new Collection();
        $this->uses                       = new Collection();
        $this->traits                     = new Collection();
        $this->properties                 = new Collection();
        $this->constants                  = new Collection();
        $this->annotations                = new Collection();
        $this->classAnnotationsDefinition = new ClassAnnotationsDefinition();
    }

    public function class(): string
    {
        return $this->class;
    }

    public function namespace(string $namespace = null): string|null
    {
        if (!is_null($namespace)) {
            $this->namespace = $namespace;
        }

        return $this->namespace;
    }

    public function extends(string $class = null): string|null
    {
        if (!is_null($class)) {
            $this->addUse($class);
            $this->extends = class_basename($class);
        }

        return $this->extends;
    }

    public function implements(string $class = null): Collection
    {
        if (!is_null($class)) {
            $this->addUse($class);
            $this->implements->add(class_basename($class));
        }

        return $this->implements;
    }

    public function hasInterface(string $interface): bool
    {
        return $this->implements->filter(function (string $usedInterface) use ($interface) {
                return $usedInterface === class_basename($interface);
            })->count() > 0;
    }

    public function addUse(string $class): void
    {
        if ($this->uses->contains($class) === false) {
            $this->uses->add($class);
        }
    }

    public function uses(): Collection
    {
        return $this->uses->sort();
    }

    public function addTrait(string $class = null): void
    {
        if ($this->traits->contains($class) === false) {
            $this->addUse($class);
            $this->traits->add(class_basename($class));
        }
    }

    public function traits(): Collection
    {
        return $this->traits;
    }

    public function hasTrait(string $trait): bool
    {
        return $this->traits->filter(function (string $usedTrait) use ($trait) {
                return $usedTrait === class_basename($trait);
            })->count() > 0;
    }

    public function addProperty(ClassPropertyDefinition $property): void
    {
        $this->properties->add($property);
    }

    public function properties(): Collection
    {
        return $this->properties;
    }

    public function addConstant(ClassConstantDefinition $constant): void
    {
        $this->constants->add($constant);
    }

    public function constants(): Collection
    {
        return $this->constants;
    }

}