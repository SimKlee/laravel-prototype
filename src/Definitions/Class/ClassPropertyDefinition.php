<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Definitions\Class;

use SimKlee\StringBuffer\StringBuffer;

class ClassPropertyDefinition
{
    public const VISIBILITY_PUBLIC    = 'public';
    public const VISIBILITY_PROTECTED = 'protected';
    public const VISIBILITY_PRIVATE   = 'private';

    public const TYPE_MIXED   = 'mixed';
    public const TYPE_STRING  = 'string';
    public const TYPE_BOOLEAN = 'bool';
    public const TYPE_INTEGER = 'int';
    public const TYPE_FLOAT   = 'float';

    public function __construct(public string            $name,
                                public array|string|null $type = null,
                                public mixed             $value = null,
                                public bool              $optional = false,
                                public string            $visibility = self::VISIBILITY_PRIVATE,
                                public array             $annotations = [])
    {

    }

    public function value(): mixed
    {
        if ($this->value instanceof ValueDefinition) {
            return $this->value->value();
        }

        if (is_bool($this->value)) {
            return $this->value ? 'true' : 'false';
        }

        if (is_array($this->value) && count($this->value) === 0) {
            return '[]';
        } elseif (is_array($this->value)) {

            return StringBuffer::create('[')
                               ->append("\n")
                               ->append(
                                   collect($this->value)->map(function (string $line) {
                                       return sprintf("\t\t%s,", $line);
                                   })->implode(PHP_EOL)
                               )
                               ->append("\n\t]")
                               ->toString();
        }

        return $this->value;
    }
}