<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Definitions;

use SimKlee\LaravelPrototype\Definitions\Formatter\AbstractFormatter;
use SimKlee\LaravelPrototype\Messages\MessageCollection;

abstract class AbstractDefinition
{
    public function setProperty(string $property, $value = null): mixed
    {
        if ($this->{$property} !== $value) {
            $this->{$property} = $value;
        }

        return $this->{$property};
    }

    abstract public function validate(): MessageCollection|bool;

    abstract public function formatter(): AbstractFormatter;

    abstract public function toArray(): array;

    protected function removeNullAndFalseValues(array $data): array
    {
        return collect($data)
            ->reject(fn($value, string $key) => is_null($value))
            ->reject(fn($value, string $key) => $value === false)
            ->mapWithKeys(fn($value, string $key) => [$key => $value])
            ->toArray();
    }
}
