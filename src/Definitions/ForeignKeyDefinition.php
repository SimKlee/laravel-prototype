<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Definitions;

use SimKlee\LaravelPrototype\Definitions\Formatter\ForeignKeyFormatter;
use SimKlee\LaravelPrototype\Messages\MessageCollection;

class ForeignKeyDefinition extends AbstractDefinition
{
    public const TYPE_HAS_ONE    = 'HasOne';
    public const TYPE_HAS_MANY   = 'HasMany';
    public const TYPE_BELONGS_TO = 'BelongsTo';

    public const ACTION_CASCADE   = 'CASCADE';
    public const ACTION_NO_ACTION = 'NO ACTION';
    public const ACTION_SET_NULL  = 'SET NULL';
    public const ACTION_RESTRICT  = 'RESTRICT';

    protected int     $version       = 1;
    protected ?string $type          = null;
    protected ?string $foreignModel  = null;
    protected ?string $foreignTable  = null;
    protected ?string $foreignColumn = null;
    protected ?string $name          = null;
    protected ?string $onUpdate      = null;
    protected ?string $onDelete      = null;

    public function __construct(protected string $model, protected string $table, protected string $column)
    {
    }

    public function version(int $version = null): int
    {
        if (!is_null($version)) {
            $this->version = $version;
        }

        return $this->version;
    }

    public function foreignModel(string $model = null): string|null
    {
        if (!is_null($model)) {
            $this->foreignModel = $model;
        }

        return $this->foreignModel;
    }

    public function foreignTable(string $table = null): string|null
    {
        if (!is_null($table)) {
            $this->foreignTable = $table;
        }

        return $this->foreignTable;
    }

    public function foreignColumn(string $column = null): string|null
    {
        if (!is_null($column)) {
            $this->foreignColumn = $column;
        }

        return $this->foreignColumn;
    }

    public function type(string $type = null): string|null
    {
        if (!is_null($type)) {
            $this->type = $type;
        }

        return $this->type;
    }

    public function model(): string
    {
        return $this->model;
    }

    public function table(): string
    {
        return $this->table;
    }

    public function column(): string
    {
        return $this->column;
    }

    public function name(string $name = null): string|null
    {
        if (!is_null($name)) {
            $this->name = $name;
        }

        return $this->name;
    }

    public function onUpdate(string $action = null): string|null
    {
        if (!is_null($action)) {
            $this->onUpdate = $action;
        }

        return $this->onUpdate;
    }

    public function onDelete(string $action = null): string|null
    {
        if (!is_null($action)) {
            $this->onDelete = $action;
        }

        return $this->onDelete;
    }

    public static function fromArray(string $model, string $table, array $definition): ForeignKeyDefinition
    {
        $foreignKeyDefinition                = new ForeignKeyDefinition($model, $table, $definition['column']);
        $foreignKeyDefinition->version       = $definition['version'] ?? 1;
        $foreignKeyDefinition->type          = $definition['type'] ?? null; // @TODO: remove
        $foreignKeyDefinition->column        = $definition['column'];
        $foreignKeyDefinition->foreignModel  = $definition['foreignModel'];
        $foreignKeyDefinition->foreignTable  = $definition['foreignTable'] ?? null;
        $foreignKeyDefinition->foreignColumn = $definition['foreignColumn'];
        $foreignKeyDefinition->name          = $definition['name'];
        $foreignKeyDefinition->onUpdate      = $definition['onUpdate'];
        $foreignKeyDefinition->onDelete      = $definition['onDelete'];

        return $foreignKeyDefinition;
    }

    public function toArray(): array
    {
        return [
            'version'       => $this->version,
            'type'          => $this->type,
            'model'         => $this->model,
            'table'         => $this->table,
            'column'        => $this->column,
            'foreignModel'  => $this->foreignModel,
            'foreignTable'  => $this->foreignTable,
            'foreignColumn' => $this->foreignColumn,
            'name'          => $this->name,
            'onUpdate'      => $this->onUpdate,
            'onDelete'      => $this->onDelete,
        ];
    }

    public function formatter(): ForeignKeyFormatter
    {
        return new ForeignKeyFormatter($this);
    }

    public function validate(): MessageCollection|bool
    {
        // TODO: Implement validate() method.
    }
}
