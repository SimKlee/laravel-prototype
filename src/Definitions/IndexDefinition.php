<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Definitions;

use Illuminate\Support\Collection;
use SimKlee\LaravelPrototype\Definitions\Formatter\AbstractFormatter;
use SimKlee\LaravelPrototype\Definitions\Formatter\IndexFormatter;
use SimKlee\LaravelPrototype\Messages\MessageCollection;

class IndexDefinition extends AbstractDefinition
{
    public const TYPE_INDEX   = 'index';
    public const TYPE_UNIQUE  = 'unique';
    public const TYPE_PRIMARY = 'primary';

    protected int         $version = 1;
    protected ?string     $type    = null;
    protected ?string     $name    = null;
    protected ?Collection $columns = null;

    public function __construct(protected string $model)
    {

    }

    public function model(): string
    {
        return $this->model;
    }

    public function type(string $type = null): string|null
    {
        if (!is_null($type) && in_array($type, [self::TYPE_INDEX, self::TYPE_UNIQUE, self::TYPE_PRIMARY])) {
            $this->type = $type;
        }

        return $this->type;
    }

    public function name(string $name = null): string|null
    {
        if (!is_null($name)) {
            $this->name = $name;
        }

        return $this->name;
    }

    public function version(int $version = null): int
    {
        if (!is_null($version)) {
            $this->version = $version;
        }

        return $this->version;
    }

    public function columns(array $columns = null): Collection|null
    {
        if (is_array($columns)) {
            $this->columns = collect($columns);
        }

        return $this->columns;
    }

    public function addColumns(string $column): void
    {
        $this->columns->add($column);
    }

    public static function fromArray(string $model, array $definition): IndexDefinition
    {
        $indexDefinition = new IndexDefinition($model);
        $indexDefinition->version($definition['version'] ?? 1);
        $indexDefinition->type($definition['type']);
        $indexDefinition->name($definition['name']);
        $indexDefinition->columns($definition['columns']);

        return $indexDefinition;
    }

    public function toArray(): array
    {
        return [
            'version' => $this->version,
            'type'    => $this->type,
            'name'    => $this->name,
            'columns' => $this->columns->toArray(),
        ];
    }

    public function validate(): MessageCollection|bool
    {
        return true;
    }

    public function formatter(): AbstractFormatter
    {
        return new IndexFormatter($this);
    }
}
