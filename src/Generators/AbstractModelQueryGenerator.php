<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Generators;

use SimKlee\LaravelWorkbench\Generators\ClassGenerator;

class AbstractModelQueryGenerator extends ClassGenerator
{
    public function __construct(string $class)
    {
        parent::__construct(class: $class, templateFile: __DIR__ . '/../Templates/AbstractModelQuery.php');
    }
}
