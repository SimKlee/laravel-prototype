<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Generators;

use Illuminate\Support\Str;
use PhpParser\Node\Arg;
use PhpParser\Node\ArrayItem;
use PhpParser\Node\Expr\Array_;
use PhpParser\Node\Expr\Cast\Bool_;
use PhpParser\Node\Expr\ClassConstFetch;
use PhpParser\Node\Expr\ConstFetch;
use PhpParser\Node\Expr\FuncCall;
use PhpParser\Node\Expr\Match_;
use PhpParser\Node\Identifier;
use PhpParser\Node\MatchArm;
use PhpParser\Node\Name;
use PhpParser\Node\Scalar\Int_;
use PhpParser\Node\Scalar\String_;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelWorkbench\Generators\ClassGenerator;
use SimKlee\LaravelWorkbench\String\StringBuffer;

class ModelValidatorGenerator extends ClassGenerator
{
    public function __construct(private readonly ModelDefinition $modelDefinition)
    {
        $modelClass = sprintf('App\Models\Validators\%sValidator', $modelDefinition->model());
        parent::__construct(class: $modelClass, templateFile: __DIR__ . '/../Templates/ModelValidatorTemplate.php');
    }

    protected function init(): void
    {
        parent::init();
        $this->useComponent->add(sprintf('App\Models\%s', $this->modelDefinition->model()));
        $this->fillRules();
    }

    private function fillRules(): void
    {
        /** @var Array_ $array */
        $array = $this->parser->findFirstInstanceOf(
            class: Array_::class,
            ast  : $this->parser->findClassMethod('rules')
        );

        $array->items = collect($this->modelDefinition->columns->all())
            ->reject(fn(ColumnDefinition $columnDefinition) => in_array($columnDefinition->name(), [
                'id',
                'created_at',
                'updated_at',
                'deleted_at',
            ]))
            ->map(fn(ColumnDefinition $columnDefinition) => new ArrayItem(
                value: new String_(value: $this->getValidationString($columnDefinition)),
                key  : new ClassConstFetch(
                    class: new Name(name: $this->modelDefinition->model()),
                    name : new Identifier(name: sprintf(
                        'PROPERTY_%s',
                        Str::upper(Str::snake($columnDefinition->name()))
                    ))
                )
            ))->toArray();
    }

    private function getValidationString(ColumnDefinition $columnDefinition): string
    {
        return StringBuffer::create($columnDefinition->nullable() ? 'nullable' : 'required')
            ->appendIf($columnDefinition->dataType()->isString(), '|string')
            ->appendIf($this->hasMin($columnDefinition), '|min:1')
            ->appendIf($this->hasMax($columnDefinition), '|max:' . $columnDefinition->length())
            ->appendIf($this->hasValues($columnDefinition), '|in:' . implode(',', $columnDefinition->values() ?? []))
            ->toString();
    }

    private function hasMin(ColumnDefinition $columnDefinition): bool
    {
        return $columnDefinition->nullable() === false
            && $columnDefinition->dataType()->hasLength()
            && is_null($columnDefinition->values());
    }

    private function hasMax(ColumnDefinition $columnDefinition): bool
    {
        return $columnDefinition->dataType()->hasLength()
            && is_null($columnDefinition->values());
    }

    private function hasValues(ColumnDefinition $columnDefinition): bool
    {
        return is_array($columnDefinition->values()) && count($columnDefinition->values()) > 0;
    }
}
