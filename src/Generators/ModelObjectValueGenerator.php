<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Generators;

use Illuminate\Support\Str;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelWorkbench\Generators\ClassGenerator;

class ModelObjectValueGenerator extends ClassGenerator
{
    public function __construct(private readonly ModelDefinition $modelDefinition, private ColumnDefinition|string $columnDefinition)
    {
        if (is_string($this->columnDefinition)) {
            $this->columnDefinition = $this->modelDefinition->columns->get($this->columnDefinition);
        }

        $class = sprintf(
            'App\Models\ValueObjects\%s%s',
            Str::ucfirst(Str::camel($this->modelDefinition->model())),
            Str::ucfirst(Str::camel($this->columnDefinition->name()))
        );

        parent::__construct(
            class       : $class,
            templateFile: __DIR__ . '/../Templates/ModelValueObjectTemplate.php'
        );
    }

    protected function init(): void
    {
        parent::init();
        $this->setNamespace('App\Models\ValueObjects');
    }
}
