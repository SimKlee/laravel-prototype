<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Generators;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use PhpParser\Modifiers;
use PhpParser\Node\Expr\Assign;
use PhpParser\Node\Expr\PropertyFetch;
use PhpParser\Node\Expr\Variable;
use PhpParser\Node\Scalar\Int_;
use PhpParser\Node\Stmt;
use PhpParser\Node\Stmt\ClassMethod;
use SimKlee\LaravelPrototype\Ast\ModelTestBuilder;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelPrototype\Definitions\RelationDefinition;
use SimKlee\LaravelWorkbench\Ast\AssignBuilder;
use SimKlee\LaravelWorkbench\Ast\ClassConstFetchBuilder;
use SimKlee\LaravelWorkbench\Ast\ClassMethodBuilder;
use SimKlee\LaravelWorkbench\Ast\MethodCallBuilder;
use SimKlee\LaravelWorkbench\Ast\PropertyFetchBuilder;
use SimKlee\LaravelWorkbench\Ast\StaticCallBuilder;
use SimKlee\LaravelWorkbench\Generators\ClassGenerator;

class ModelTest2Generator extends ClassGenerator
{
    public function __construct(private readonly ModelDefinition $modelDefinition)
    {
        parent::__construct(
            class       : sprintf('Tests\Unit\Models\%s2Test', $modelDefinition->model()),
            templateFile: __DIR__ . '/../Templates/ModelTest.php'
        );
    }

    protected function init(): void
    {
        parent::init();

        collect([
            $this->createTestCreateModel(),
            $this->createTestModelFactory(),
            $this->createTestModelQuery(),
            $this->createTestModelMeta(),
            $this->createTestModelRepository(),
            $this->createTestTimestamps(),
            $this->createTestSoftDelete(),
            $this->createTestForceDelete(),
            $this->createTestRestore(),
        ])->each(fn(ClassMethod $classMethod) => $this->methodComponent->addClassMethod($classMethod));
    }

    private function createTestCreateModel(): ClassMethod
    {
        return $this->classMethod('testCreateModel', [
            AssignBuilder::create(
                var : Str::lcfirst($this->modelDefinition->model()),
                expr: StaticCallBuilder::create(class: $this->modelDefinition->model(), name: 'create')
            )->asExpression(),
            MethodCallBuilder::create(
                var : 'this',
                name: 'assertInstanceOf',
                args: [
                    ClassConstFetchBuilder::create(class: $this->modelDefinition->model(), name: 'class'),
                    new Variable(name: Str::lcfirst($this->modelDefinition->model())),
                ]
            )->asExpression(),
        ]);
    }

    private function createTestModelFactory(): ClassMethod
    {
        return $this->classMethod('testModelFactory', [
            MethodCallBuilder::create(
                var : 'this',
                name: 'assertInstanceOf',
                args: [
                    ClassConstFetchBuilder::create(class: sprintf('%sFactory', $this->modelDefinition->model()), name: 'class'),
                    StaticCallBuilder::create(class: $this->modelDefinition->model(), name: 'factory'),
                ]
            )->asExpression(),
            MethodCallBuilder::create(
                var : 'this',
                name: 'assertInstanceOf',
                args: [
                    ClassConstFetchBuilder::create(class: $this->modelDefinition->model(), name: 'class'),
                    MethodCallBuilder::create(
                        var : StaticCallBuilder::create(class: $this->modelDefinition->model(), name: 'factory'),
                        name: 'create'
                    ),
                ]
            )->asExpression(),
        ]);
    }

    private function createTestModelQuery(): ClassMethod
    {
        return $this->classMethod('testModelQuery', [
            MethodCallBuilder::create(
                var : 'this',
                name: 'assertInstanceOf',
                args: [
                    ClassConstFetchBuilder::create(class: sprintf('%sQuery', $this->modelDefinition->model()), name: 'class'),
                    StaticCallBuilder::create(class: $this->modelDefinition->model(), name: 'query'),
                ]
            )->asExpression(),
        ]);
    }

    private function createTestModelMeta(): ClassMethod
    {
        return $this->classMethod('testModelMeta', [
            MethodCallBuilder::create(
                var : 'this',
                name: 'assertInstanceOf',
                args: [
                    ClassConstFetchBuilder::create(class: sprintf('%sMeta', $this->modelDefinition->model()), name: 'class'),
                    StaticCallBuilder::create(class: $this->modelDefinition->model(), name: 'meta'),
                ]
            )->asExpression(),
        ]);
    }

    private function createTestModelRepository(): ClassMethod
    {
        return $this->classMethod('testModelRepository', [
            MethodCallBuilder::create(
                var : 'this',
                name: 'assertInstanceOf',
                args: [
                    ClassConstFetchBuilder::create(class: sprintf('%sRepository', $this->modelDefinition->model()), name: 'class'),
                    StaticCallBuilder::create(class: $this->modelDefinition->model(), name: 'repository'),
                ]
            )->asExpression(),
        ]);
    }

    private function createTestTimestamps(): ClassMethod
    {
        return $this->classMethod('testTimestamps', [
            AssignBuilder::create(
                var : Str::lcfirst($this->modelDefinition->model()),
                expr: StaticCallBuilder::create(class: $this->modelDefinition->model(), name: 'create')
            )->asExpression(),
            MethodCallBuilder::create(
                var : 'this',
                name: 'assertIsCarbon',
                args: [
                    PropertyFetchBuilder::create(var: Str::lcfirst($this->modelDefinition->model()), name: 'created_at'),
                    PropertyFetchBuilder::create(var: Str::lcfirst($this->modelDefinition->model()), name: 'updated_at'),
                ]
            )->asExpression(),
        ]);
    }

    private function createTestSoftDelete(): ClassMethod
    {
        return $this->classMethod('testSoftDelete', [
            $this->assignModelStmt(),
            MethodCallBuilder::create(
                var : 'this',
                name: 'assertIsCarbon',
                args: [
                    PropertyFetchBuilder::create(var: Str::lcfirst($this->modelDefinition->model()), name: 'created_at'),
                    PropertyFetchBuilder::create(var: Str::lcfirst($this->modelDefinition->model()), name: 'updated_at'),
                ]
            )->asExpression(),

        ]);
    }

    private function createTestForceDelete(): ClassMethod
    {
        return $this->classMethod('testForceDelete', [
            $this->assignModelStmt(),
            MethodCallBuilder::create(
                var : 'this',
                name: 'assertCount',
                args: [
                    new Int_(value: 0),
                    MethodCallBuilder::create(
                        var : StaticCallBuilder::create(class: $this->modelDefinition->model(), name: 'withTrashed'),
                        name: 'get'
                    )->object(),
                ]
            )->asExpression(),
            MethodCallBuilder::create(
                var : Str::lcfirst($this->modelDefinition->model()),
                name: 'forceDelete'
            )->asExpression(),
        ]);
    }

    private function createTestRestore(): ClassMethod
    {
        return $this->classMethod('testRestore', [
            $this->assignModelStmt(),
            MethodCallBuilder::create(
                var : Str::lcfirst($this->modelDefinition->model()),
                name: 'delete'
            )->asExpression(),
            MethodCallBuilder::create(
                var : 'this',
                name: 'assertCount',
                args: [
                    new Int_(value: 0),
                    StaticCallBuilder::create(class: $this->modelDefinition->model(), name: 'all')->object()
                ]
            )->asExpression(),
            MethodCallBuilder::create(
                var : Str::lcfirst($this->modelDefinition->model()),
                name: 'restore'
            )->asExpression(),
            MethodCallBuilder::create(
                var : 'this',
                name: 'assertCount',
                args: [
                    new Int_(value: 1),
                    StaticCallBuilder::create(class: $this->modelDefinition->model(), name: 'all')->object()
                ]
            )->asExpression(),
        ]);
    }

    private function classMethod(string $name, array $stmts = []): ClassMethod
    {
        $classMethod = ClassMethodBuilder::create()
            ->name($name)
            ->flags(Modifiers::PUBLIC)
            ->returnType('void');

        collect($stmts)->each(fn(Stmt $stmt) => $classMethod->stmt($stmt));

        return $classMethod->object();
    }

    private function assignModelStmt(): Stmt
    {
        return AssignBuilder::create(
            var : Str::lcfirst($this->modelDefinition->model()),
            expr: StaticCallBuilder::create(class: $this->modelDefinition->model(), name: 'create')
        )->object();
    }
}
