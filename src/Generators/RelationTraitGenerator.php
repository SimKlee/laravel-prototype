<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Generators;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use PhpParser\Modifiers;
use PhpParser\Node\Arg;
use PhpParser\Node\Expr\ArrowFunction;
use PhpParser\Node\Expr\Variable;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use PhpParser\Node\Param;
use PhpParser\Node\Stmt\ClassMethod;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelWorkbench\Ast\ArrayBuilder;
use SimKlee\LaravelWorkbench\Ast\ArrayItemBuilder;
use SimKlee\LaravelWorkbench\Ast\ClassConstFetchBuilder;
use SimKlee\LaravelWorkbench\Ast\ConstFetchBuilder;
use SimKlee\LaravelWorkbench\Ast\FuncCallBuilder;
use SimKlee\LaravelWorkbench\Ast\MethodCallBuilder;
use SimKlee\LaravelWorkbench\Ast\ReturnBuilder;
use SimKlee\LaravelWorkbench\Ast\VariableBuilder;
use SimKlee\LaravelWorkbench\Generators\TraitGenerator;

class RelationTraitGenerator extends TraitGenerator
{
    public function __construct(private readonly ModelDefinition $modelDefinition)
    {
        $modelClass = sprintf('App\Models\Traits\Relations\%sRelationTrait', $modelDefinition->model());
        parent::__construct(trait: $modelClass, templateFile: __DIR__ . '/../Templates/RelationTraitTemplate.php');
    }

    protected function init(): void
    {
        parent::init();
        $this->useComponent->add(Collection::class);
        $this->useComponent->add(sprintf('App\Models\%s', $this->modelDefinition->model()));
        $this->useComponent->add('App\Models\AbstractRelationModel');
        $this->traitComponent->add('App\Models\Traits\HasRelationsTrait');
        $this->methodComponent->addClassMethod($this->createRelationMethod());
    }

    private function createRelationMethod(): ClassMethod
    {
        return new ClassMethod(
            name    : Str::camel($this->modelDefinition->table()),
            subNodes: [
                'flags'      => Modifiers::PUBLIC,
                'returnType' => new Name('Collection'),
                'params'     => [
                    new Param(
                        var    : new Variable('relationType'),
                        default: ConstFetchBuilder::create('null')->object(),
                        type   : new Identifier('string')
                    ),
                ],
                'stmts'      => [
                    ReturnBuilder::create(
                        expr: MethodCallBuilder::create(
                            var: MethodCallBuilder::create(var: 'this', name: 'relations', args: [
                                VariableBuilder::create('relationType')->asArg(),
                                ArrayBuilder::create(items: [
                                    ArrayItemBuilder::create(
                                        value: FuncCallBuilder::create(
                                            name: 'class_basename',
                                            args: [
                                                ClassConstFetchBuilder::create(class: $this->modelDefinition->model(), name: 'class')->asArg()
                                            ]
                                        )),
                                ])->asArg(),
                            ]),
                            name: 'map',
                            args: [
                                new Arg(new ArrowFunction(subNodes: [
                                    'params' => [
                                        new Param(var: new Variable('relationModel'), type: new Name('AbstractRelationModel'))
                                    ],
                                    'expr' => MethodCallBuilder::create(var: 'relationModel', name: 'relatedModel')->object()
                                ]))
                            ]
                        )
                    )->object(),
                ],
            ]
        );
    }
}
