<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Generators;

use Carbon\Carbon;
use Closure;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use PhpParser\Node\ArrayItem;
use PhpParser\Node\Expr\Array_;
use PhpParser\Node\Expr\ClassConstFetch;
use PhpParser\Node\Expr\ConstFetch;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use PhpParser\Node\PropertyItem;
use PhpParser\Node\Scalar\String_;
use PhpParser\Node\Stmt\Property;
use PhpParser\Node\Stmt\Return_;
use PhpParser\Node\VarLikeIdentifier;
use SimKlee\LaravelPrototype\Ast\ModelBuilder;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\LaravelPrototype\Definitions\ForeignKeyDefinition;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelPrototype\Definitions\RelationDefinition;
use SimKlee\LaravelWorkbench\Generators\ClassGenerator;
use SimKlee\LaravelWorkbench\Generators\Components\Annotations\MethodAnnotation;
use SimKlee\LaravelWorkbench\Generators\Components\Annotations\MethodAnnotationParam;
use SimKlee\LaravelWorkbench\Generators\Components\Annotations\PropertyAnnotation;
use SimKlee\LaravelWorkbench\Tools\FQN;

class ModelGenerator extends ClassGenerator
{
    private ModelBuilder $builder;

    public function __construct(private readonly ModelDefinition $modelDefinition)
    {
        $this->builder = new ModelBuilder($this->modelDefinition);
        parent::__construct(
            class       : sprintf('App\Models\%s', $modelDefinition->model()),
            templateFile: __DIR__ . '/../Templates/ModelTemplate.php'
        );
    }

    protected function init(): void
    {
        parent::init();

        $this->addUses([
            Arrayable::class,
            Closure::class,
            Collection::class,
        ]);
        if ($this->modelDefinition->columns->dates()->isNotEmpty()) {
            $this->useComponent->add(Carbon::class);
        }

        $this->modelDefinition->relations->all()
            ->each(fn(RelationDefinition $relationDefinition) => $this->useComponent->add(
                sprintf('App\Models\%s', $relationDefinition->relatedModel())
            ));

        $this->setExtends($this->modelDefinition->extends());

        collect($this->modelDefinition->interfaces())
            ->each(fn(string $interface) => $this->addInterface($interface));

        collect($this->modelDefinition->traits())
            ->each(fn(string $trait) => $this->traitComponent->add($trait));

        $this->propertyComponent->get('timestamps')->props = [
            new PropertyItem(
                name   : new VarLikeIdentifier(name: 'timestamps'),
                default: new ConstFetch(name: new Name(name: $this->modelDefinition->timestamps() ? 'true' : 'false'))
            ),
        ];

        if ($this->modelDefinition->softDelete()) {
            $this->traitComponent->add(SoftDeletes::class);
        }

        $this->setClassConstants();
        $this->setAnnotations();
        $this->fillGuardedProperty();
        $this->fillCastsMethod();

        $this->modelDefinition->foreignKeys->all()
            ->each(function (ForeignKeyDefinition $foreignKeyDefinition) {
                $this->methodComponent->addClassMethod(
                    call_user_func(
                        [$this->builder, Str::camel($foreignKeyDefinition->type())],
                        $foreignKeyDefinition
                    )
                );
                $this->useComponent->add($this->builder->getUsage($foreignKeyDefinition));
            });

        $this->modelDefinition->relations->all()
            ->each(function (RelationDefinition $relationDefinition) {
                $this->methodComponent->addClassMethod(
                    call_user_func(
                        [$this->builder, Str::camel($relationDefinition->type())],
                        $relationDefinition
                    )
                );
                $this->useComponent->add($this->builder->getUsage($relationDefinition));
            });
    }

    private function setClassConstants(): void
    {
        $this->constantComponent->changeOrCreate('TABLE', $this->modelDefinition->table());
        $this->modelDefinition->columns->all()
            ->each(fn(ColumnDefinition $columnDefinition) => $this->constantComponent->add(
                name : sprintf('PROPERTY_%s', Str::upper($columnDefinition->name())),
                value: $columnDefinition->name()
            ));

        $this->modelDefinition->foreignKeys->all()
            ->each(function (ForeignKeyDefinition $foreignKeyDefinition) {
                $this->useComponent->add(sprintf('App\Models\%s', $foreignKeyDefinition->foreignModel()));
                $this->constantComponent->add(
                    name : sprintf('WITH_%s', Str::upper(Str::snake($foreignKeyDefinition->foreignModel()))),
                    value: Str::lower($foreignKeyDefinition->foreignModel())
                );
            });

        $this->modelDefinition->relations->all()
            ->each(function (RelationDefinition $relationDefinition) {
                $this->constantComponent->add(
                    name : sprintf('WITH_%s', Str::upper(Str::snake($relationDefinition->relatedTable()))),
                    value: Str::lcfirst(Str::camel($relationDefinition->relatedTable()))
                );
            });
    }

    private function setAnnotations(): void
    {
        $this->createPropertyAnnotations();
        $this->modelDefinition->foreignKeys->all()
            ->each(fn(ForeignKeyDefinition $foreignKeyDefinition) => $this->annotationComponent->add(
                call_user_func(
                    [$this->builder, sprintf('%sAnnotation', Str::lcfirst($foreignKeyDefinition->type()))],
                    $foreignKeyDefinition
                )
            ));
        $this->modelDefinition->relations->all()
            ->each(fn(RelationDefinition $relationDefinition) => $this->annotationComponent->add(
                call_user_func(
                    [$this->builder, sprintf('%sAnnotation', Str::lcfirst($relationDefinition->type()))],
                    $relationDefinition
                )
            ));
        $this->createExtensionAnnotations();
        $this->createMethodAnnotations();
    }

    private function createPropertyAnnotations(): void
    {
        $this->modelDefinition->columns
            ->all()
            ->each(fn(ColumnDefinition $columnDefinition) => $this->annotationComponent->add(
                PropertyAnnotation::create($columnDefinition->name())
                    ->type($this->getColumnType($columnDefinition))
            ));
    }

    private function getColumnType(ColumnDefinition $columnDefinition): string
    {
        if ($columnDefinition->dataType()->getCastType() === 'array') {
            $valueObjectClass = sprintf('App\Models\ValueObjects\%s%s',
                $columnDefinition->model(),
                Str::ucfirst(Str::camel($columnDefinition->name()))
            );

            if (class_exists($valueObjectClass)) {
                $this->useComponent->add($valueObjectClass);
                return class_basename($valueObjectClass);
            }
        }

        if (is_array($columnDefinition->values())) {
            $enumClass = sprintf('App\Models\Enumerations\%s%sEnum',
                $columnDefinition->model(),
                Str::ucfirst(Str::camel($columnDefinition->name()))
            );
            if (class_exists($enumClass)) {
                $this->useComponent->add($enumClass);
                return class_basename($enumClass);
            }
        }

        return match ($columnDefinition->dataType()->getCastType()) {
            'date', 'datetime', 'time', 'timestamps' => 'Carbon',
            default                                  => $columnDefinition->dataType()->getCastType(),
        };
    }

    private function createMethodAnnotations(): void
    {
        $this->annotationComponent->add(
            MethodAnnotation::create('create')
                ->static()
                ->type($this->modelDefinition->model())
                ->param(MethodAnnotationParam::create('attributes')->type('array')->default([]))
        );

        if ($this->traitComponent->has('App\Models\Traits\AutoUuidSecondaryKey') || $this->traitComponent->has('App\Models\Traits\UuidSecondaryKey')) {
            $this->annotationComponent->add(
                MethodAnnotation::create('findByUuid')
                    ->static()
                    ->type($this->modelDefinition->model() . '|null')
                    ->param(MethodAnnotationParam::create('uuid')->type('string'))
            );
        }

        $this->annotationComponent->add(
            MethodAnnotation::create('find')
                ->static()
                ->type($this->modelDefinition->model())
                ->param(MethodAnnotationParam::create('id')->type('mixed'))
                ->param(MethodAnnotationParam::create('columns')->type('array')->default([]))
        );
        $this->annotationComponent->add(
            MethodAnnotation::create('findMany')
                ->static()
                ->type('Collection')
                ->type($this->modelDefinition->model() . '[]')
                ->param(MethodAnnotationParam::create('ids')->type('Arrayable'))
                ->param(MethodAnnotationParam::create('columns')->type('array')->default([]))
        );
        $this->annotationComponent->add(
            MethodAnnotation::create('findOrFail')
                ->static()
                ->type('Collection')
                ->type($this->modelDefinition->model() . '[]')
                ->param(MethodAnnotationParam::create('ids')->type('Arrayable'))
                ->param(MethodAnnotationParam::create('columns')->type('array')->default([]))
        );
        $this->annotationComponent->add(
            MethodAnnotation::create('findOrNew')
                ->static()
                ->type('Collection')
                ->type($this->modelDefinition->model() . '[]')
                ->param(MethodAnnotationParam::create('id')->type('mixed'))
                ->param(MethodAnnotationParam::create('columns')->type('array')->default([]))
        );
        $this->annotationComponent->add(
            MethodAnnotation::create('firstOr')
                ->static()
                ->type($this->modelDefinition->model())
                ->type('mixed')
                ->param(MethodAnnotationParam::create('columns')->type('Closure')->type('array')->default([]))
                ->param(MethodAnnotationParam::create('callback')->type('Closure')->type('array')->default(null))
        );
        $this->annotationComponent->add(
            MethodAnnotation::create('firstOrCreate')
                ->static()
                ->type($this->modelDefinition->model())
                ->param(MethodAnnotationParam::create('attributes')->type('array'))
                ->param(MethodAnnotationParam::create('values')->type('array')->default([]))
        );
        $this->annotationComponent->add(
            MethodAnnotation::create('firstOrFail')
                ->static()
                ->type($this->modelDefinition->model())
                ->param(MethodAnnotationParam::create('columns')->type('array')->default([]))
        );
        $this->annotationComponent->add(
            MethodAnnotation::create('firstOrNew')
                ->static()
                ->type($this->modelDefinition->model())
                ->param(MethodAnnotationParam::create('attributes')->type('array')->default([]))
                ->param(MethodAnnotationParam::create('values')->type('array')->default([]))
        );
        $this->annotationComponent->add(
            MethodAnnotation::create('updateOrCreate')
                ->static()
                ->type($this->modelDefinition->model())
                ->param(MethodAnnotationParam::create('attributes')->type('array'))
                ->param(MethodAnnotationParam::create('values')->type('array')->default([]))
        );
        $this->annotationComponent->add(
            MethodAnnotation::create('sole')
                ->static()
                ->type($this->modelDefinition->model())
                ->param(MethodAnnotationParam::create('columns')->type('array')->default([]))
        );
    }

    private function createExtensionAnnotations(): void
    {
        $this->addExtensionAnnotation(
            extensionClass: sprintf('App\Models\Meta\%sMeta', $this->modelDefinition->model()),
            methodName    : 'meta'
        );
        $this->addExtensionAnnotation(
            extensionClass: sprintf('App\Models\Queries\%sQuery', $this->modelDefinition->model()),
            methodName    : 'query'
        );
        $this->addExtensionAnnotation(
            extensionClass: sprintf('App\Models\Repositories\%sRepository', $this->modelDefinition->model()),
            methodName    : 'repository'
        );
        $this->addExtensionAnnotation(
            extensionClass: sprintf('App\Models\Validators\%sValidator', $this->modelDefinition->model()),
            methodName    : 'validator'
        );
    }

    private function addExtensionAnnotation(string $extensionClass, string $methodName): void
    {
        if (File::exists(FQN::file($extensionClass))) {
            $this->useComponent->add($extensionClass);
            $this->annotationComponent->add(
                MethodAnnotation::create($methodName)
                    ->static()
                    ->type(class_basename($extensionClass))
                    ->type('null')
            );
        }
    }

    private function fillCastsMethod(): void
    {
        $castsMethod = $this->parser->findClassMethod('casts');
        if ($castsMethod) {
            $castsMethod->stmts = [
                new Return_(
                    expr: new Array_(
                        items: $this->modelDefinition->columns->all()
                            ->filter(fn(ColumnDefinition $columnDefinition) => $this->isCastable($columnDefinition))
                            ->map(fn(ColumnDefinition $columnDefinition) => $this->getCastValue($columnDefinition))
                            ->toArray()
                    )
                ),
            ];
        }
    }

    private function isCastable(ColumnDefinition $columnDefinition): bool
    {
        if ($this->modelDefinition->timestamps() && in_array($columnDefinition->name(), [Model::CREATED_AT, Model::UPDATED_AT])) {
            return false;
        }

        if ($this->modelDefinition->softDelete() && $columnDefinition->name() === 'deleted_at') {
            return false;
        }

        if ($columnDefinition->dataType()->getCastType() === 'string' && !is_null($columnDefinition->values())) {
            return true;
        }

        if ($columnDefinition->dataType()->getCastType() === 'string') {
            return false;
        }

        return true;
    }

    private function getCastValue(ColumnDefinition $columnDefinition): ArrayItem
    {
        $castType = match ($columnDefinition->dataType()->getCastType()) {
            'Carbon' => new ClassConstFetch(class: new Name(name: 'Carbon'), name: new Identifier(name: 'class')),
            'array'  => $this->getValueObjectClassIfExists($columnDefinition),
            'string' => $this->getEnumerationClassIfExists($columnDefinition),
            'date'   => new String_('date:Y-m-d'),
            'time'   => new String_('date:H:i:s'),
            default  => new String_(value: $columnDefinition->dataType()->getCastType()),
        };

        return new ArrayItem($castType, $this->builder->modelConstFetch($columnDefinition->name()));
    }

    private function getValueObjectClassIfExists(ColumnDefinition $columnDefinition): ClassConstFetch|String_
    {
        if ($columnDefinition->valueObject() === false) {
            return new String_('array');
        }

        $class = sprintf('App\Models\ValueObjects\%s%s',
            $columnDefinition->model(),
            Str::ucfirst(Str::camel($columnDefinition->name()))
        );

        if (class_exists($class)) {
            return new ClassConstFetch(class: new Name(name: class_basename($class)), name: new Identifier(name: 'class'));
        }

        return new String_(value: $columnDefinition->dataType()->getCastType());
    }

    private function getEnumerationClassIfExists(ColumnDefinition $columnDefinition): ClassConstFetch|String_
    {
        $class = sprintf('App\Models\Enumerations\%s%sEnum',
            $columnDefinition->model(),
            Str::ucfirst(Str::camel($columnDefinition->name()))
        );

        if (class_exists($class)) {
            return new ClassConstFetch(class: new Name(name: class_basename($class)), name: new Identifier(name: 'class'));
        }

        return new String_(value: $columnDefinition->dataType()->getCastType());
    }

    private function fillGuardedProperty(): void
    {
        $guarded = [$this->modelDefinition->columns->primary()->name()];

        if ($this->traitComponent->has('App\Models\Traits\AutoUuidSecondaryKey') || $this->traitComponent->has('App\Models\Traits\AutoUuid')) {
            $guarded[] = 'uuid';
        }

        /** @var Property $property */
        $property        = $this->parser->findProperty('guarded');
        $property->props = [
            new PropertyItem(
                name   : new VarLikeIdentifier('guarded'),
                default: $this->builder->propertiesArray($guarded)
            ),
        ];
    }
}
