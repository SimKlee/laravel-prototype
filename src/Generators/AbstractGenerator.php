<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Generators;

use Illuminate\Support\Facades\File;
use SimKlee\StringBuffer\StringBuffer;

abstract class AbstractGenerator
{
    protected array $data = [];

    public function __construct(protected string $stub)
    {
    }

    public function render(): StringBuffer
    {
        $view = view($this->stub);

        foreach ($this->data as $key => $value) {
            $view->with($key, $value);
        }

        return StringBuffer::create($view->render());
    }

    public function save(string $file, bool $overwrite = false): bool
    {
        if ($overwrite === false && File::exists($file)) {
            return false;
        }

        $this->createDirectoryIfNotExists($file);

        return File::put($file, $this->beforeSave($this->render())->toString()) !== false;
    }

    private function createDirectoryIfNotExists(string $file): void
    {
        $parts = explode('/', $file);
        array_pop($parts);
        $path = implode('/', $parts);
        if (File::isDirectory($path) === false) {
            File::makeDirectory(path: $path, mode: 0755, recursive: true);
        }
    }

    protected function beforeSave(StringBuffer $stringBuffer): StringBuffer
    {
        return $stringBuffer;
    }

    public function setData(string $key, mixed $value): void
    {
        $this->data[$key] = $value;
    }
}