<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Generators;

use Illuminate\Support\Str;
use PhpParser\Node\Arg;
use PhpParser\Node\ArrayItem;
use PhpParser\Node\Expr\Array_;
use PhpParser\Node\Expr\Cast\Bool_;
use PhpParser\Node\Expr\ClassConstFetch;
use PhpParser\Node\Expr\ConstFetch;
use PhpParser\Node\Expr\FuncCall;
use PhpParser\Node\Expr\Match_;
use PhpParser\Node\Identifier;
use PhpParser\Node\MatchArm;
use PhpParser\Node\Name;
use PhpParser\Node\Scalar\Int_;
use PhpParser\Node\Scalar\String_;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelWorkbench\Generators\ClassGenerator;

class ModelMetaGenerator extends ClassGenerator
{
    public function __construct(private readonly ModelDefinition $modelDefinition)
    {
        $modelClass = sprintf('App\Models\Meta\%sMeta', $modelDefinition->model());
        parent::__construct(class: $modelClass, templateFile: __DIR__ . '/../Templates/ModelMetaTemplate.php');
    }

    protected function init(): void
    {
        parent::init();
        $this->useComponent->add(sprintf('App\Models\%s', $this->modelDefinition->model()));
        $this->setExtends('App\Models\Meta\AbstractModelMeta');
        $this->fillRequiredPropertiesMethod();
        $this->fillNullableProperties();
        $this->fillJsonProperties();
        $this->fillUnsignedProperties();
        $this->fillValues();
        $this->fillMin();
        $this->fillMax();
        $this->fillDecimals();
        $this->fillDefault();
    }

    private function fillArrayInMethod(string $methodName, array $items): void
    {
        /** @var Array_ $array */
        $array        = $this->parser->findFirstInstanceOf(
            class: Array_::class,
            ast  : $this->parser->findClassMethod($methodName)
        );
        $array->items = $items;
    }

    private function fillRequiredPropertiesMethod(): void
    {
        $this->fillArrayInMethod(
            'requiredProperties',
            collect($this->modelDefinition->columns->all()
                ->reject(fn(ColumnDefinition $columnDefinition) => $columnDefinition->nullable()))
                ->map(fn(ColumnDefinition $columnDefinition) => new ArrayItem(
                    value: new ClassConstFetch(
                        class: new Name(name: $this->modelDefinition->model()),
                        name : new Identifier(name: sprintf(
                            'PROPERTY_%s',
                            Str::upper(Str::snake($columnDefinition->name()))
                        ))
                    )
                ))->toArray()
        );
    }

    private function fillNullableProperties(): void
    {
        $this->fillArrayInMethod(
            'nullableProperties',
            collect($this->modelDefinition->columns->all()
                ->filter(fn(ColumnDefinition $columnDefinition) => $columnDefinition->nullable()))
                ->map(fn(ColumnDefinition $columnDefinition) => new ArrayItem(
                    value: new ClassConstFetch(
                        class: new Name(name: $this->modelDefinition->model()),
                        name : new Identifier(name: sprintf(
                            'PROPERTY_%s',
                            Str::upper(Str::snake($columnDefinition->name()))
                        ))
                    )
                ))->toArray()
        );
    }

    private function fillJsonProperties(): void
    {
        $this->fillArrayInMethod(
            'jsonProperties',
            collect($this->modelDefinition->columns->all()
                ->filter(fn(ColumnDefinition $columnDefinition) => $columnDefinition->dataType()->getCastType() === 'array'))
                ->map(fn(ColumnDefinition $columnDefinition) => new ArrayItem(
                    value: new ClassConstFetch(
                        class: new Name(name: $this->modelDefinition->model()),
                        name : new Identifier(name: sprintf(
                            'PROPERTY_%s',
                            Str::upper(Str::snake($columnDefinition->name()))
                        ))
                    )
                ))->toArray()
        );
    }

    private function fillUnsignedProperties(): void
    {
        $this->fillArrayInMethod(
            'unsignedProperties',
            collect($this->modelDefinition->columns->all()
                ->filter(fn(ColumnDefinition $columnDefinition) => $columnDefinition->unsigned()))
                ->map(fn(ColumnDefinition $columnDefinition) => new ArrayItem(
                    value: new ClassConstFetch(
                        class: new Name(name: $this->modelDefinition->model()),
                        name : new Identifier(name: sprintf(
                            'PROPERTY_%s',
                            Str::upper(Str::snake($columnDefinition->name()))
                        ))
                    )
                ))->toArray()
        );
    }

    private function fillMatchInMethod(string $methodName, array $matchArms): void
    {
        /** @var Match_ $match */
        $match       = $this->parser->findFirstInstanceOf(
            class: Match_::class,
            ast  : $this->parser->findClassMethod($methodName)
        );
        $match->arms = $matchArms;
    }

    private function fillValues(): void
    {
        $columns = $this->modelDefinition->columns->all()
            ->filter(fn(ColumnDefinition $columnDefinition) => is_array($columnDefinition->values()));

        $this->fillMatchInMethod(
            'values',
            $columns->map(fn(ColumnDefinition $columnDefinition) => new MatchArm(
                conds: [new ClassConstFetch(
                            class: new Name(name: Str::ucfirst(Str::camel($columnDefinition->model()))),
                            name : new Identifier(name: sprintf(
                                'PROPERTY_%s',
                                Str::upper(Str::snake($columnDefinition->name()))
                            ))
                        )],
                body : new FuncCall(
                    name: new Name('collect'),
                    args: [new Arg(value: new Array_(
                        items: collect($columnDefinition->values())
                            ->map(fn($value) => new ArrayItem(value: new String_(value: $value)))
                            ->toArray()
                    ))]
                )
            ))->add(new MatchArm(conds: null, body: new ConstFetch(name: new Name(name: 'null'))))
                ->toArray()
        );
    }

    private function fillMin(): void
    {
        $this->fillMatchInMethod(
            'min',
            collect($this->modelDefinition->columns->all()
                ->reject(fn(ColumnDefinition $columnDefinition) => $columnDefinition->nullable())
                ->reject(fn(ColumnDefinition $columnDefinition) => is_null($columnDefinition->length())))
                ->map(fn(ColumnDefinition $columnDefinition) => new MatchArm(
                    conds: [new ClassConstFetch(
                                class: new Name(name: Str::ucfirst(Str::camel($columnDefinition->model()))),
                                name : new Identifier(name: sprintf(
                                    'PROPERTY_%s',
                                    Str::upper(Str::snake($columnDefinition->name()))
                                ))
                            )],
                    body : new Int_(value: 1)
                ))->add(new MatchArm(conds: null, body: new ConstFetch(name: new Name(name: 'null'))))
                ->toArray()
        );
    }

    private function fillMax(): void
    {
        $this->fillMatchInMethod(
            'max',
            collect($this->modelDefinition->columns->all()
                ->reject(fn(ColumnDefinition $columnDefinition) => is_null($columnDefinition->length())))
                ->map(fn(ColumnDefinition $columnDefinition) => new MatchArm(
                    conds: [new ClassConstFetch(
                                class: new Name(name: Str::ucfirst(Str::camel($columnDefinition->model()))),
                                name : new Identifier(name: sprintf(
                                    'PROPERTY_%s',
                                    Str::upper(Str::snake($columnDefinition->name()))
                                ))
                            )],
                    body : new Int_(value: $columnDefinition->length())
                ))->add(new MatchArm(conds: null, body: new ConstFetch(name: new Name(name: 'null'))))
                ->toArray()
        );
    }

    private function fillDecimals(): void
    {
        $this->fillMatchInMethod(
            'decimals',
            collect($this->modelDefinition->columns->all()
                ->reject(fn(ColumnDefinition $columnDefinition) => $columnDefinition->nullable())
                ->reject(fn(ColumnDefinition $columnDefinition) => is_null($columnDefinition->decimals())))
                ->map(fn(ColumnDefinition $columnDefinition) => new MatchArm(
                    conds: [new ClassConstFetch(
                                class: new Name(name: Str::ucfirst(Str::camel($columnDefinition->model()))),
                                name : new Identifier(name: sprintf(
                                    'PROPERTY_%s',
                                    Str::upper(Str::snake($columnDefinition->name()))
                                ))
                            )],
                    body : new Int_(value: $columnDefinition->decimals())
                ))->add(new MatchArm(conds: null, body: new ConstFetch(name: new Name(name: 'null'))))
                ->toArray()
        );
    }

    private function fillDefault(): void
    {
        $columns = $this->modelDefinition->columns->all()
            ->reject(fn(ColumnDefinition $columnDefinition) => $columnDefinition->nullable())
            ->reject(fn(ColumnDefinition $columnDefinition) => is_null($columnDefinition->default()));

        $this->fillMatchInMethod(
            'default',
            $columns->map(fn(ColumnDefinition $columnDefinition) => new MatchArm(
                conds: [new ClassConstFetch(
                            class: new Name(name: Str::ucfirst(Str::camel($columnDefinition->model()))),
                            name : new Identifier(name: sprintf(
                                'PROPERTY_%s',
                                Str::upper(Str::snake($columnDefinition->name()))
                            ))
                        )],
                body : match ($columnDefinition->dataType()->getCastType()) {
                    'int'     => new Int_(value: $columnDefinition->default()),
                    'boolean' => new ConstFetch(name: new Name(name: $columnDefinition->default() ? 'true' : 'false')),
                    default   => new String_(value: $columnDefinition->default()),
                }
            ))->add(new MatchArm(conds: null, body: new ConstFetch(name: new Name(name: 'null'))))
                ->toArray()
        );
    }
}
