<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Generators;

use SimKlee\LaravelWorkbench\Generators\ClassGenerator;
use SimKlee\LaravelWorkbench\Tools\FQN;

class ModelQueryTraitGenerator extends ClassGenerator
{
    public function __construct(string $class)
    {
        parent::__construct(class: $class, templateFile: __DIR__ . '/../Templates/ModelQueryTrait.php');
    }

    protected function init(): void
    {
        $this->setNamespace(FQN::namespace($this->class));
    }
}
