<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Generators;

use Illuminate\Support\Str;
use PhpParser\Modifiers;
use PhpParser\Node\Expr\ClassConstFetch;
use PhpParser\Node\Expr\ConstFetch;
use PhpParser\Node\Name;
use PhpParser\Node\Identifier;
use PhpParser\Node\Scalar\String_;
use PhpParser\Node\Stmt\ClassMethod;
use PhpParser\Node\Stmt\Enum_;
use PhpParser\Node\Stmt\EnumCase;
use PhpParser\Node\Stmt\Return_;
use PhpParser\Node\UnionType;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelWorkbench\Generators\ClassGenerator;
use SimKlee\LaravelWorkbench\Tools\FQN;

class ModelEnumerationGenerator extends ClassGenerator
{
    public function __construct(private readonly ModelDefinition $modelDefinition, private ColumnDefinition|string $columnDefinition)
    {
        if (is_string($this->columnDefinition)) {
            $this->columnDefinition = $this->modelDefinition->columns->get($this->columnDefinition);
        }

        $class = sprintf(
            'App\Models\Enumerations\%s%sEnum',
            $this->modelDefinition->model(),
            Str::ucfirst(Str::camel($this->columnDefinition->name()))
        );

        parent::__construct(
            class       : $class,
            templateFile: __DIR__ . '/../Templates/ModelEnumerationTemplate.php'
        );
    }

    protected function init(): void
    {
        /** @var Enum_ $enum */
        $enum = $this->parser->findFirstInstanceOf(Enum_::class);

        if (is_null($enum)) {
            $this->parser->namespace->stmts[] = new Enum_(
                name    : new Identifier(name: FQN::className($this->class)),
                subNodes: ['scalarType' => new Identifier(name: 'string')]
            );
        } else {
            $enum->name       = new Identifier(name: FQN::className($this->class));
            $enum->scalarType = new Identifier(name: 'string');
        }

        $this->setNamespace(FQN::namespace($this->class));

        $this->addCases();
        $this->addDefaultMethod();
    }

    private function addCases(): void
    {
        collect($this->columnDefinition->values())
            ->reject(fn($value) => $this->caseExists(Str::upper($value)))
            ->each(fn($value) => $this->parser->addEnumStmt(
                new EnumCase(
                    name: new Identifier(name: Str::upper($value)),
                    expr: new String_(value: $value)
                )
            ));
    }

    private function caseExists(string $case): bool
    {
        return $this->parser->findInstanceOf(EnumCase::class)
            ->filter(fn(EnumCase $enumCase) => $enumCase->name->name === $case)
            ->isNotEmpty();
    }

    private function addDefaultMethod(): void
    {
        if ($this->parser->findClassMethod('default', $this->parser->enum)) {
            return;
        }

        $returnExpr = new ConstFetch(name: new Name(name: 'null'));
        if (!is_null($this->columnDefinition->default())) {
            $returnExpr = new ClassConstFetch(
                class: new Name(name: 'self'),
                name : new Identifier(name: Str::upper($this->columnDefinition->default()))
            );
        }

        $this->parser->enum->stmts[] = new ClassMethod(
            name    : new Identifier(name: 'default'),
            subNodes: [
                'flags'      => Modifiers::PUBLIC | Modifiers::STATIC,
                'returnType' => new UnionType(
                    types: [
                        new Name(name: 'self'),
                        new Identifier(name: 'null'),
                    ],
                ),
                'stmts'      => [new Return_(expr: $returnExpr)],
            ]
        );
    }
}
