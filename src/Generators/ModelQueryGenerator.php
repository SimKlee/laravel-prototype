<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Generators;

use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelWorkbench\Generators\ClassGenerator;

class ModelQueryGenerator extends ClassGenerator
{
    public function __construct(private readonly ModelDefinition $modelDefinition)
    {
        $modelClass = sprintf('App\Models\Queries\%sQuery', $modelDefinition->model());
        parent::__construct(class: $modelClass, templateFile: __DIR__ . '/../Templates/ModelQueryTemplate.php');
    }

    protected function init(): void
    {
        parent::init();
        $this->generateAbstractModelQueryClass();
        $this->setExtends('App\Models\Queries\AbstractModelQuery');
    }

    private function generateAbstractModelQueryClass(): void
    {
        (new AbstractModelQueryGenerator('App\Models\Queries\AbstractModelQuery'))->write();
    }

}
