<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Generators;

use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelWorkbench\Generators\ClassGenerator;

class ModelRepositoryGenerator extends ClassGenerator
{
    public function __construct(private readonly ModelDefinition $modelDefinition)
    {
        $modelClass = sprintf('App\Models\Repositories\%sRepository', $modelDefinition->model());
        parent::__construct(class: $modelClass, templateFile: __DIR__ . '/../Templates/ModelRepositoryTemplate.php');
    }

    protected function init(): void
    {
        parent::init();
        $this->extends('App\Models\Repositories\AbstractModelRepository');
    }
}
