<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Generators;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;
use PhpParser\Node\Arg;
use PhpParser\Node\Expr\ClassConstFetch;
use PhpParser\Node\Expr\MethodCall;
use PhpParser\Node\Expr\New_;
use PhpParser\Node\Expr\StaticCall;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use PhpParser\Node\Stmt\ClassMethod;
use PhpParser\Node\Stmt\Expression;
use PhpParser\Node\Stmt\Return_;
use PhpParser\Node\Stmt\Use_;
use PhpParser\Node\UseItem;
use PhpParser\PrettyPrinter\Standard;
use SimKlee\LaravelPrototype\Ast\MigrationBuilder;
use SimKlee\LaravelPrototype\Definitions\ForeignKeyDefinition;
use SimKlee\LaravelPrototype\Definitions\IndexDefinition;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;

class ModelMigrationGenerator
{
    private array            $ast = [];
    private MigrationBuilder $builder;

    public function __construct(private readonly ModelDefinition $modelDefinition, private readonly int $version = 1)
    {
        $this->builder = new MigrationBuilder($this->modelDefinition);
        $this->init();
    }

    public static function exists(ModelDefinition $modelDefinition, int $version = 1): bool
    {
        return File::exists(self::file($modelDefinition, $version));
    }

    public static function file(ModelDefinition $modelDefinition, int $version = 1): string
    {
        return database_path('migrations/' . sprintf(
                '%s_%s_%s_table.php',
                Str::padLeft((string)$modelDefinition->position(), 4, '0'),
                Str::padLeft((string)$version, 3, '0'),
                $modelDefinition->table()
            ));
    }

    public function getFile(): string
    {
        return self::file($this->modelDefinition, $this->version);
    }

    public function write(string $file = null, bool $overwrite = true): bool
    {
        if (is_null($file)) {
            $file = self::file($this->modelDefinition, $this->version);
        }

        if ($overwrite === false && File::exists($file)) {
            return false;
        }

        $parts = explode('/', $file);
        array_pop($parts);
        $path = implode('/', $parts);
        if (File::isDirectory($path) === false) {
            File::makeDirectory(path: $path, recursive: true);
        }

        return File::put($file, (new Standard())->prettyPrintFile($this->ast)) !== false;
    }

    protected function init(): void
    {
        $this->addUses();
        $class          = $this->builder->migrationClass();
        $this->ast[]    = new Return_(expr: new New_(class: $class));
        $class->stmts[] = $this->upMethod();
        $class->stmts[] = $this->downMethod();
    }

    private function addUses(): void
    {
        $this->ast[] = new Use_(uses: [
            new UseItem(name: new Name(name: sprintf('App\Models\%s', $this->modelDefinition->model()))),
        ]);
        $this->ast[] = new Use_(uses: [new UseItem(name: new Name(name: Migration::class))]);
        $this->ast[] = new Use_(uses: [new UseItem(name: new Name(name: Blueprint::class))]);
        $this->ast[] = new Use_(uses: [new UseItem(name: new Name(name: Schema::class))]);
        $this->getForeignKeys()
            ->each(fn(ForeignKeyDefinition $foreignKeyDefinition) => $this->ast[] = new Use_(uses: [
                new UseItem(name: new Name(name: sprintf('App\Models\%s', $foreignKeyDefinition->foreignModel()))),
            ]));
    }

    private function getColumns(): Collection
    {
        return $this->modelDefinition->columns->all()
            ->filter(fn(ColumnDefinition $columnDefinition) => $columnDefinition->version() === $this->version);
    }

    private function getIndexes(): Collection
    {
        return $this->modelDefinition->indexes->all()
            ->filter(fn(IndexDefinition $indexDefinition) => $indexDefinition->version() === $this->version);
    }

    private function getForeignKeys(): Collection
    {
        return $this->modelDefinition->foreignKeys->all()
            ->filter(fn(ForeignKeyDefinition $foreignKeyDefinition) => $foreignKeyDefinition->version() === $this->version);
    }

    private function upMethod(): ClassMethod
    {
        return $this->builder->upMethod(
            stmts: [
                $this->builder->schemaExpression(
                    method: ($this->version === 1) ? 'create' : 'table',
                    stmts : array_merge(
                        $this->getColumns()
                            ->map(fn(ColumnDefinition $columnDefinition) => $this->builder->createOrUpdateColumn($columnDefinition))
                            ->toArray(),
                        $this->getIndexes()
                            ->map(fn(IndexDefinition $indexDefinition) => $this->builder->indexCreate($indexDefinition))
                            ->toArray(),
                        $this->getForeignKeys()
                            ->map(fn(ForeignKeyDefinition $foreignKeyDefinition) => $this->builder->foreignKeyCreate($foreignKeyDefinition))
                            ->toArray()
                    )
                ),
            ]
        );
    }

    private function downMethod(): ClassMethod
    {
        if ($this->version === 1) {
            $stmts = [
                new Expression(expr: new StaticCall(
                    class: new Name(name: 'Schema'),
                    name : new Identifier(name: 'dropIfExists'),
                    args : [
                        new Arg(value: new ClassConstFetch(
                            class: new Name(name: $this->modelDefinition->model()),
                            name : new Identifier(name: 'TABLE')
                        )),
                    ]
                )),
            ];
        } else {
            $stmts = [
                $this->builder->schemaExpression(
                    method: 'table',
                    stmts : array_merge(
                        $this->getForeignKeys()
                            ->filter(fn(ForeignKeyDefinition $foreignKeyDefinition) => $foreignKeyDefinition->version() === $this->version)
                            ->map(fn(ForeignKeyDefinition $foreignKeyDefinition) => $this->builder->dropForeignKey($foreignKeyDefinition))
                            ->toArray(),
                        $this->getIndexes()
                            ->filter(fn(IndexDefinition $indexDefinition) => $indexDefinition->version() === $this->version)
                            ->map(fn(IndexDefinition $indexDefinition) => $this->builder->dropIndex($indexDefinition))
                            ->toArray(),
                        $this->getColumns()
                            ->filter(fn(ColumnDefinition $columnDefinition) => $columnDefinition->version() === $this->version)
                            ->map(fn(ColumnDefinition $columnDefinition) => $this->builder->dropColumn($columnDefinition))
                            ->toArray(),
                    )
                ),
            ];
        }

        return $this->builder->downMethod(stmts: $stmts);
    }
}
