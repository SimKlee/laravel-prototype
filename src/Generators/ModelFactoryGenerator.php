<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Generators;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use PhpParser\Node\ArrayItem;
use PhpParser\Node\Expr\Array_;
use PhpParser\Node\Expr\ClassConstFetch;
use PhpParser\Node\Expr\ConstFetch;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelWorkbench\Generators\ClassGenerator;

class ModelFactoryGenerator extends ClassGenerator
{
    public function __construct(private readonly ModelDefinition $modelDefinition)
    {
        $class = sprintf('Database\Factories\%sFactory', $modelDefinition->model());
        parent::__construct(class: $class, templateFile: __DIR__ . '/../Templates/ModelFactoryTemplate.php');
    }

    protected function init(): void
    {
        parent::init();
        $this->useComponent->add(Model::class);
        $this->useComponent->add(sprintf('App\Models\%s', $this->modelDefinition->model()));
        $this->fillDefinitions();
        $this->setClassComment(implode(PHP_EOL, [
            '/**',
            sprintf(' * @extends Factory<%s>', $this->modelDefinition->model()),
            sprintf(' * @method %s create(array $attributes = [], ?Model $parent = null)', $this->modelDefinition->model()),
            ' */',
        ]));
    }

    private function fillDefinitions(): void
    {
        $definitionsMethod = $this->parser->findClassMethod('definitions');
        /** @var Array_ $array */
        $array = $this->parser->findFirstInstanceOf(Array_::class, $definitionsMethod);
        $array->items = $this->modelDefinition->columns->all()
            ->reject(fn(ColumnDefinition $columnDefinition) => in_array($columnDefinition->name(), [
                'id',
                'created_at',
                'updated_at',
                'deleted_at',
            ]))->map(fn(ColumnDefinition $columnDefinition) => new ArrayItem(
                value: new ConstFetch(new Name('null')),
                key: new ClassConstFetch(
                    class: new Name($this->modelDefinition->model()),
                    name: new Identifier(sprintf('PROPERTY_%s', Str::upper($columnDefinition->name())))
                )
            ))->toArray();
    }
}
