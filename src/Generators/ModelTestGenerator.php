<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Generators;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use PhpParser\Node\Stmt\ClassMethod;
use SimKlee\LaravelPrototype\Ast\ModelTestBuilder;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelPrototype\Definitions\RelationDefinition;
use SimKlee\LaravelWorkbench\Generators\ClassGenerator;

class ModelTestGenerator extends ClassGenerator
{
    private ModelTestBuilder $builder;

    public function __construct(private readonly ModelDefinition $modelDefinition)
    {
        $this->builder = new ModelTestBuilder($this->modelDefinition);
        parent::__construct(
            class       : sprintf('Tests\Unit\Models\%sTest', $modelDefinition->model()),
            templateFile: __DIR__ . '/../Templates/ModelTestTemplate.php'
        );
    }

    protected function init(): void
    {
        parent::init();

        $this->useComponent->add(sprintf('App\Models\%s', $this->modelDefinition->model()));

        if ($this->modelDefinition->columns->dates()->isNotEmpty()) {
            $this->useComponent->add(Carbon::class);
        }

        $this->modelDefinition->relations->all()
            ->each(fn(RelationDefinition $relationDefinition) => $this->useComponent->add(
                sprintf('App\Models\%s', $relationDefinition->relatedModel())
            ));

        $this->getClassMethods()
            ->each(fn(ClassMethod $classMethod) => $this->methodComponent->addClassMethod($classMethod));
    }

    private function getClassMethods(): Collection
    {
        $methods = collect([
            $this->builder->getTestData(),
            $this->builder->getTestData(forUpdate: true),
            $this->builder->testCreate(),
            $this->builder->testUpdate(),
        ]);

        if ($this->modelDefinition->timestamps()) {
            $methods->add($this->builder->testTimestamps());
        }

        if ($this->modelDefinition->softDelete()) {
            $methods->add($this->builder->testSoftDelete());
            $methods->add($this->builder->testForceDelete());
        } else {
            $methods->add($this->builder->testDelete());
        }

        if ($this->modelDefinition->foreignKeys->count() > 0) {
            $methods->add($this->builder->testForeignKeys());
        }

        if ($this->modelDefinition->relations->count() > 0) {
            $methods->add($this->builder->testRelations());
        }

        return $methods;
    }
}
