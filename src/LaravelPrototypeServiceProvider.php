<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype;

use Illuminate\Support\ServiceProvider;
use SimKlee\LaravelPrototype\Console\Commands\AddModelDefinitionCommand;
use SimKlee\LaravelPrototype\Console\Commands\ChangeModelDefinitionCommand;
use SimKlee\LaravelPrototype\Console\Commands\CreateModelCommand;
use SimKlee\LaravelPrototype\Console\Commands\CreateModelMetaCommand;
use SimKlee\LaravelPrototype\Console\Commands\CreateModelMigrationCommand;
use SimKlee\LaravelPrototype\Console\Commands\CreateModelPropertyEnumCommand;
use SimKlee\LaravelPrototype\Console\Commands\CreateModelQueryCommand;
use SimKlee\LaravelPrototype\Console\Commands\CreateModelRepositoryCommand;
use SimKlee\LaravelPrototype\Console\Commands\CreateRelationTraitCommand;
use SimKlee\LaravelPrototype\Console\Commands\GenerateAbstractClassesCommand;
use SimKlee\LaravelPrototype\Console\Commands\GenerateEnumerationCommand;
use SimKlee\LaravelPrototype\Console\Commands\GenerateModelFactoryCommand;
use SimKlee\LaravelPrototype\Console\Commands\GenerateModelMetaCommand;
use SimKlee\LaravelPrototype\Console\Commands\GenerateModelMigrationCommand;
use SimKlee\LaravelPrototype\Console\Commands\GenerateModelQueryCommand;
use SimKlee\LaravelPrototype\Console\Commands\GenerateModelRepositoryCommand;
use SimKlee\LaravelPrototype\Console\Commands\GenerateModelTestCommand;
use SimKlee\LaravelPrototype\Console\Commands\GenerateModelValidatorCommand;
use SimKlee\LaravelPrototype\Console\Commands\GenerateTraitsCommand;
use SimKlee\LaravelPrototype\Console\Commands\GenerateValueObjectCommand;
use SimKlee\LaravelPrototype\Console\Commands\GenerateModelCommand;
use SimKlee\LaravelPrototype\Console\Commands\ImportFromDatabaseCommand;
use SimKlee\LaravelPrototype\Console\Commands\ImportModelDefinitionsCommand;
use SimKlee\LaravelPrototype\Console\Commands\RemoveModelCommand;

class LaravelPrototypeServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/laravel-prototype.php', 'laravel-prototype');
    }

    public function boot(): void
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'laravel-prototype');

        $this->commands([
            CreateModelCommand::class,
            CreateModelPropertyEnumCommand::class,
            CreateModelRepositoryCommand::class,
            CreateModelQueryCommand::class,
            CreateModelMetaCommand::class,
            CreateModelMigrationCommand::class,
            ImportFromDatabaseCommand::class,

            ImportModelDefinitionsCommand::class,
            GenerateAbstractClassesCommand::class,
            GenerateTraitsCommand::class,
            GenerateModelCommand::class,
            GenerateValueObjectCommand::class,
            GenerateModelMetaCommand::class,
            GenerateModelRepositoryCommand::class,
            GenerateModelQueryCommand::class,
            GenerateModelValidatorCommand::class,
            GenerateEnumerationCommand::class,
            GenerateModelMigrationCommand::class,
            GenerateModelTestCommand::class,
            GenerateModelFactoryCommand::class,

            AddModelDefinitionCommand::class,
            ChangeModelDefinitionCommand::class,

            RemoveModelCommand::class,
            CreateRelationTraitCommand::class,
        ]);

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/laravel-prototype.php' => config_path('laravel-prototype.php'),
            ], 'config');
        }
    }

    public static function packagePath(string $file = null): string
    {
        $path = __DIR__ . '/..';

        if (!is_null($file)) {
            $path .= '.' . $file;
        }

        return $path;
    }

    public static function packageResourcePath(string $file = null): string
    {
        $path = __DIR__ . '/../resources';
        if (!is_null($file)) {
            $path .= '/' . $file;
        }
        return $path;
    }
}
