<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Templates;

use Illuminate\Support\Collection;

final class ModelMetaTemplate
{
    public function requiredProperties(): Collection
    {
        return collect([]);
    }

    public function nullableProperties(): Collection
    {
        return collect([]);
    }

    public function unsignedProperties(): Collection
    {
        return collect([]);
    }

    public function jsonProperties(): Collection
    {
        return collect([]);
    }

    public function values(string $property): Collection|null
    {
        return match ($property) {
            default => null,
        };
    }

    public function min(string $property): int|null
    {
        return match ($property) {
            default => null,
        };
    }

    public function max(string $property): int|null
    {
        return match ($property) {
            default => null,
        };
    }

    public function decimals(string $property): int|null
    {
        return match ($property) {
            default => null,
        };
    }

    public function default(string $property): mixed
    {
        return match ($property) {
            default => null,
        };
    }
}
