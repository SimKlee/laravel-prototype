<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Templates;

use SimKlee\LaravelPrototype\Exceptions\ClassNotFoundException;
use SimKlee\LaravelPrototype\Model\AbstractModelQuery;

trait ModelQueryTrait
{
    /**
     * @throws ClassNotFoundException
     */
    public function newEloquentBuilder($query): AbstractModelQuery
    {
        $class = sprintf('\App\Models\Queries\%sQuery', class_basename(static::class));

        if (class_exists($class) === false) {
            throw new ClassNotFoundException($class);
        }

        return new $class($query);
    }
}
