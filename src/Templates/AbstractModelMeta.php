<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Templates;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

abstract class AbstractModelMeta
{
    protected Model $model;

    public function __construct(Model|string $model)
    {
        $this->model = is_string($model) ? new $model() : $model;
    }

    public function guarded(): Collection
    {
        return collect($this->model->getGuarded());
    }

    public function isGuarded(string $property): bool
    {
        return $this->model->isGuarded($property);
    }

    public function fillable(): Collection
    {
        return collect($this->model->getFillable());
    }

    public function isFillable(string $property): bool
    {
        return $this->model->isFillable($property);
    }

    public function dates(): Collection
    {
        return collect($this->model->getDates());
    }

    public function isDate(string $property): bool
    {
        return $this->dates()->contains($property);
    }

    public function isRequired(string $property): bool
    {
        return $this->requiredProperties()->contains($property);
    }

    public function isNullable(string $property): bool
    {
        return $this->nullableProperties()->contains($property);
    }

    public function isJson(string $property): bool
    {
        return $this->jsonProperties()->contains($property);
    }

    public function isUnsigned(string $property): bool|null
    {
        return $this->unsignedProperties()->contains($property);
    }

    abstract public function requiredProperties(): Collection;

    abstract public function nullableProperties(): Collection;

    abstract public function unsignedProperties(): Collection;

    abstract public function jsonProperties(): Collection;

    abstract public function values(string $property): Collection|null;

    abstract public function min(string $property): int|null;

    abstract public function max(string $property): int|null;

    abstract public function decimals(string $property): int|null;

    abstract public function default(string $property): mixed;
}
