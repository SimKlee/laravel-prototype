<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Templates;

use Illuminate\Database\Eloquent\Builder;

abstract class AbstractModelQuery extends Builder
{
}
