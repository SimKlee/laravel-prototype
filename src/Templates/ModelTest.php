<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Templates;

use App\Models\Meta\ProcessMeta;
use App\Models\Process;
use App\Models\ProcessRelation;
use App\Models\Queries\ProcessQuery;
use App\Models\Repositories\ProcessRepository;
use Database\Factories\ProcessFactory;
use Tests\Unit\Models\ModelTestCase;

class ModelTest extends ModelTestCase
{
    public function testCreateModel(): void
    {
        $process = Process::create();

        $this->assertInstanceOf(Process::class, $process);
    }

    public function testModelFactory(): void
    {
        $this->assertInstanceOf(ProcessFactory::class, Process::factory());
        $this->assertInstanceOf(Process::class, Process::factory()->create());
    }

    public function testModelQuery(): void
    {
        $this->assertInstanceOf(ProcessQuery::class, Process::query());
    }

    public function testModelMeta(): void
    {
        $this->assertInstanceOf(ProcessMeta::class, Process::meta());
    }

    public function testModelRepository(): void
    {
        $this->assertInstanceOf(ProcessRepository::class, Process::repository());
    }

    public function testTimestamps(): void
    {
        $process = Process::create();

        $this->assertIsCarbon($process->created_at);
        $this->assertIsCarbon($process->updated_at);
    }

    public function testSoftDelete(): void
    {
        $process = Process::create();
        $this->assertNull($process->deleted_at);

        $process->delete();
        $this->assertIsCarbon($process->deleted_at);
    }

    public function testForceDelete(): void
    {
        $process = Process::create();

        $process->forceDelete();
        $this->assertCount(0, Process::withTrashed()->get());
    }

    public function testRestore(): void
    {
        $process = Process::create();

        $process->delete();
        $this->assertCount(0, Process::all());

        $process->restore();
        $this->assertCount(1, Process::all());
    }

    public function testProcessRelations(): void
    {
        $process         = Process::create();
        $processRelation = $process->relate($process);

        $this->assertInstanceOf(ProcessRelation::class, $processRelation);
        $this->assertInstanceOf(ProcessRelation::class, $process->processRelations->first());
        $this->assertCount(1, $process->processRelations);
        $this->assertCount(1, $process->relations());
    }

    public function testColumn(): void
    {
        $this->assertSame('processes.id', Process::column(Process::PROPERTY_ID));
        $this->assertSame('processes.id AS process_id', Process::column(Process::PROPERTY_ID, 'process_id'));
    }
}
