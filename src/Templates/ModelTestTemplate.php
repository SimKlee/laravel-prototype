<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Templates;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

final class ModelTestTemplate extends TestCase
{
    use DatabaseTransactions;
}
