<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Templates;

use Illuminate\Database\Eloquent\Factories\Factory;

final class ModelFactoryTemplate extends Factory
{
    public function definition(): array
    {
        return [];
    }
}
