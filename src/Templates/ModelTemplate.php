<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Templates;

use Illuminate\Database\Eloquent\Factories\HasFactory;

final class ModelTemplate
{
    use HasFactory;

    public const TABLE = '';

    /** @var string */
    public $table = self::TABLE;

    /** @var bool */
    public $timestamps = false;

    /** @var array */
    protected $fillable = [];

    /** @var array */
    protected $guarded = [];

    protected function casts(): array
    {
        return [];
    }
}
