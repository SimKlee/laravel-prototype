<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Templates;

use App\Models\Meta\ProcessMeta;
use App\Models\Process;
use Tests\TestCase;

class ModelMetaTest extends TestCase
{
    private ProcessMeta $meta;

    public function setUp(): void
    {
        $this->meta = Process::meta();
    }

    public function testRequiredProperties(): void
    {
        $this->assertSame([
            Process::PROPERTY_ID,
            Process::PROPERTY_UUID,
        ], $this->meta->requiredProperties()->toArray());
    }

    public function testNullableProperties(): void
    {
        $this->assertSame([
            Process::PROPERTY_CREATED_AT,
            Process::PROPERTY_UPDATED_AT,
            Process::PROPERTY_DELETED_AT,
        ], $this->meta->nullableProperties()->toArray());
    }

    public function testUnsignedProperties(): void
    {
        $this->assertSame([
            Process::PROPERTY_ID,
        ], $this->meta->unsignedProperties()->toArray());
    }

    public function testJsonProperties(): void
    {
        $this->assertSame([
        ], $this->meta->jsonProperties()->toArray());
    }

    public function testValues(): void
    {
        collect([
            Process::PROPERTY_ID   => null,
            Process::PROPERTY_UUID => null,
        ])->each(function ($values, $property) {
            $this->assertSame($values, $this->meta->values($property));
        });
    }

    public function testMin(): void
    {
        collect([
            Process::PROPERTY_UUID => 1,
        ])->each(function ($min, string $property) {
            $this->assertSame($min, $this->meta->min($property));
        });
    }

    public function testMax(): void
    {
        collect([
            Process::PROPERTY_UUID => 36,
        ])->each(function ($max, string $property) {
            $this->assertSame($max, $this->meta->max($property));
        });
    }

    public function testDecimals(): void
    {
        collect([
            Process::PROPERTY_UUID => null,
        ])->each(function ($decimals, string $property) {
            $this->assertSame($decimals, $this->meta->decimals($property));
        });
    }

    public function testDefault(): void
    {
        collect([
            Process::PROPERTY_UUID => null,
        ])->each(function ($default, string $property) {
            $this->assertSame($default, $this->meta->default($property));
        });
    }
}
