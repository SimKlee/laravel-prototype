<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Templates;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Illuminate\Database\Eloquent\Model;
#use Illuminate\Support\Facades\Validator as ValidatorBuilder;
#use Illuminate\Validation\Validator;
use ReflectionClass;
use ReflectionProperty;

abstract class AbstractJsonValueObject implements CastsAttributes
{
    public function get(Model $model, string $key, mixed $value, array $attributes): static
    {
        return $this->fromArray(json_decode(json: $value, associative: true));
    }

    public function set(Model $model, string $key, mixed $value, array $attributes): false|string
    {
        return json_encode($value);
    }

    public function fromArray(array $value = null): static
    {
        if (is_array($value)) {
            collect((new ReflectionClass($this))->getProperties(ReflectionProperty::IS_PUBLIC))
                ->each(fn(ReflectionProperty $reflectionProperty) => $this->{$reflectionProperty->name} = $value[$reflectionProperty->name]);
        }

        return $this;
    }

    public function toArray(): array
    {
        return collect((new ReflectionClass($this))->getProperties(ReflectionProperty::IS_PUBLIC))
            ->mapWithKeys(fn(ReflectionProperty $reflectionProperty) => [
                $reflectionProperty->name => $this->{$reflectionProperty->name},
            ])->toArray();
    }

/*
    public function validator(array $data): Validator
    {
        return ValidatorBuilder::make($data, $this->rules());
    }

    abstract public function rules(): array;
*/
}
