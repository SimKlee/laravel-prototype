<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Modifiers;

use PhpParser\Comment\Doc;

class AddClassMethodAnnotation extends AbstractClassModifier
{
    public function modify(): void
    {
        $this->parser->class->getAttribute('comments');

        $this->parser->class->setAttribute('comments', [new Doc($comment)]);
    }
}
