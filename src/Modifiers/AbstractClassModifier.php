<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Modifiers;

use PhpParser\Comment\Doc;
use SimKlee\LaravelWorkbench\Parser\PhpParser;
use SimKlee\LaravelWorkbench\Tools\FQN;

abstract class AbstractClassModifier
{
    protected PhpParser $parser;
    protected array     $annotations         = [];
    protected array     $propertyAnnotations = [];
    protected array     $methodAnnotations   = [];

    public function __construct(string $class)
    {
        $this->parser = new PhpParser(FQN::file($class));
    }

    protected function getClassComment(): array
    {
        $comment = $this->parser->class->getAttribute('comments');
        if ($comment && $comment[0] instanceof Doc) {
            return explode(PHP_EOL, $comment[0]->getText());
        }

        return [];
    }

    protected function setClassComment(): void
    {
        $lines = array_merge(
            ['/**'],
            $this->annotations,
            $this->propertyAnnotations,
            $this->methodAnnotations,
            [' */'],
        );

        $this->parser->class->setAttribute(
            key  : 'comments',
            value: [new Doc(implode(PHP_EOL, $lines))]
        );
    }

    abstract public function modify(): void;
}
