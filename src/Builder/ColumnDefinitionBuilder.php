<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Builder;

use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;

class ColumnDefinitionBuilder
{
    protected ColumnDefinition $columnDefinition;

    public function __construct(protected readonly ModelDefinition $modelDefinition, string $name = null)
    {
        $this->columnDefinition = is_string($name) && $this->modelDefinition->columns->has($name)
            ? $this->modelDefinition->columns->get($name)
            : new ColumnDefinition($this->modelDefinition->model());
    }

    public function name(string $name): self
    {
        $this->columnDefinition->name($name);

        return $this;
    }

    public function type(string $type): self
    {
        $this->columnDefinition->type($type);

        return $this;
    }
}
