<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Builder;

use Illuminate\Support\Str;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\LaravelPrototype\Definitions\IndexDefinition;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;

class RelationModelDefinitionBuilder extends AbstractModelDefinitionBuilder
{
    protected ?ModelDefinition $baseModelDefinition = null;

    protected function init(): void
    {
        $this->baseModelDefinition = $this->models->get(
            Str::replaceEnd('Relation', '', $this->modelDefinition->model())
        );

        $this->table(sprintf('%s_relations', Str::lower(Str::snake($this->baseModelDefinition->model()))));
        $this->extends('App\Models\AbstractRelationModel');
        $this->position($this->baseModelDefinition->position() + 1);

        $this->addPrimary();
        $this->addForeignKey($this->baseModelDefinition);
        $this->addForeignModel();
        $this->addRelationType();
        $this->addTimestamps();
        $this->addSoftDelete();
    }

    protected function addRelationType(): void
    {
        $this->modelDefinition->columns->add(
            ColumnDefinition::fromArray($this->modelDefinition->model(), [
                'name'   => 'relation_type',
                'type'   => 'string',
                'length' => 50,
            ])
        );
    }

    protected function addForeignModel(): void
    {
        $this->modelDefinition->columns->add(
            ColumnDefinition::fromArray($this->modelDefinition->model(), [
                'name'   => 'model',
                'type'   => 'string',
                'length' => 50,
            ])
        );
        $this->modelDefinition->columns->add(
            ColumnDefinition::fromArray($this->modelDefinition->model(), [
                'name'     => 'model_id',
                'type'     => 'integer',
                'unsigned' => true,
            ])
        );
        $this->modelDefinition->indexes->add(
            IndexDefinition::fromArray($this->modelDefinition->model(), [
                'name'    => 'foreign_model',
                'type'    => IndexDefinition::TYPE_INDEX,
                'columns' => ['model_id', 'model'],
            ])
        );
    }
}
