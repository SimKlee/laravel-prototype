<?php

declare(strict_types=1);

namespace SimKlee\LaravelPrototype\Builder;

use Exception;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use SimKlee\LaravelPrototype\Definitions\Collections\ModelDefinitionCollection;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\LaravelPrototype\Definitions\ForeignKeyDefinition;
use SimKlee\LaravelPrototype\Definitions\IndexDefinition;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;

abstract class AbstractModelDefinitionBuilder
{
    protected ?ModelDefinitionCollection $models = null;
    protected ModelDefinition            $modelDefinition;

    /**
     * @throws Exception
     */
    public function __construct(string $model, int $position = 1000)
    {
        $this->readModelDefinitions();

        if ($this->models->has($model)) {
            throw new Exception(sprintf('Model %s already exists', $model));
        }

        $this->modelDefinition = new ModelDefinition($model);
        $this->modelDefinition->position($position);

        $this->init();
    }

    protected function init(): void
    {
    }

    private function readModelDefinitions(): void
    {
        $json = json_decode(
            json       : File::get(resource_path('json/models.json')),
            associative: true
        );

        $this->models = ModelDefinitionCollection::fromArray($json);
        $this->models->fillRelations();
    }

    public function table(string $table): self
    {
        $this->modelDefinition->table($table);

        return $this;
    }

    public function extends(string $extends): self
    {
        $this->modelDefinition->extends($extends);

        return $this;
    }

    public function position(int $position): self
    {
        $this->modelDefinition->position($position);

        return $this;
    }

    public function save(): void
    {
        $this->models->add($this->modelDefinition);
        $this->models->fillRelations();
        File::put(resource_path('json/models.json'), json_encode(
            $this->models->toArray(),
            JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
        ));
    }

    public function addPrimary(): void
    {
        $this->modelDefinition->columns->add(
            ColumnDefinition::fromArray($this->modelDefinition->model(), [
                'name'          => 'id',
                'type'          => 'integer',
                'unsigned'      => true,
                'autoIncrement' => true,
                'primary'       => true,
            ])
        );
    }

    public function addSoftDelete(): void
    {
        $this->modelDefinition->columns->add(
            ColumnDefinition::fromArray($this->modelDefinition->model(), [
                'name'     => 'deleted_at',
                'type'     => 'timestamp',
                'nullable' => true,
            ])
        );
        $this->modelDefinition->softDelete(true);
    }

    public function addTimestamps(): void
    {
        $this->modelDefinition->columns->add(
            ColumnDefinition::fromArray($this->modelDefinition->model(), [
                'name'     => 'created_at',
                'type'     => 'timestamp',
                'nullable' => true,
            ])
        );
        $this->modelDefinition->columns->add(
            ColumnDefinition::fromArray($this->modelDefinition->model(), [
                'name'     => 'updated_at',
                'type'     => 'timestamp',
                'nullable' => true,
            ])
        );
        $this->modelDefinition->timestamps(true);
    }

    public function addForeignKey(ModelDefinition $foreignModel): void
    {
        $foreignModelPrimary = $foreignModel->columns->get('id');

        $foreignColumn = ColumnDefinition::fromArray($this->modelDefinition->model(), [
            'name'     => sprintf('%s_id', Str::lower(Str::snake($foreignModel->model()))),
            'type'     => $foreignModelPrimary->type(),
            'unsigned' => $foreignModelPrimary->unsigned(),
        ]);

        $foreignKey = new ForeignKeyDefinition(
            model : $this->modelDefinition->model(),
            table : $this->modelDefinition->table(),
            column: $foreignColumn->name()
        );
        $foreignKey->foreignModel($foreignModel->model());
        $foreignKey->foreignTable($foreignModel->table());
        $foreignKey->foreignColumn('id');
        $foreignKey->type(ForeignKeyDefinition::TYPE_BELONGS_TO);
        $foreignKey->name(sprintf('fk__%s__%s', $this->modelDefinition->table(), $foreignColumn->name()));

        $foreignIndex = new IndexDefinition($this->modelDefinition->model());
        $foreignIndex->type(IndexDefinition::TYPE_INDEX);
        $foreignIndex->name($foreignColumn->name());
        $foreignIndex->columns([$foreignColumn->name()]);

        $this->modelDefinition->columns->add($foreignColumn);
        $this->modelDefinition->indexes->add($foreignIndex);
        $this->modelDefinition->foreignKeys->add($foreignKey);
    }
}
