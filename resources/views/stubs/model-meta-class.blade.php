<?php /** @var \SimKlee\LaravelPrototype\Definitions\ModelDefinition $modelDefinition */ ?>
<?php /** @var \SimKlee\LaravelPrototype\Definitions\ColumnDefinition $columnDefinition */ ?>
@extends('prototype::class-layout')
@section('body')
    public function requirements(): Collection
    {
        return collect([
@foreach($modelDefinition->columns->required() as $columnDefinition)
            {{ $columnDefinition->formatter()->asPropertyString(true) }},
@endforeach
        ]);
    }

    public function nullables(): Collection
    {
        return collect([
@foreach($modelDefinition->columns->nullables() as $columnDefinition)
            {{ $columnDefinition->formatter()->asPropertyString(true) }},
@endforeach
        ]);
    }

    public function unsigned(string $property): Collection
    {
        return collect([
@foreach($modelDefinition->columns->unsigned() as $columnDefinition)
            {{ $columnDefinition->formatter()->asPropertyString(true) }},
@endforeach
        ]);
    }

    public function inputType(string $property): InputType|null
    {
    // TODO: Implement inputType() method.
    }

    public function values(string $property): Collection|null
    {
    // TODO: Implement values() method.
    }

    public function min(string $property): int|null
    {
        return match($property) {
@foreach($modelDefinition->columns->withLength() as $columnDefinition)
            {{ $columnDefinition->formatter()->asPropertyString(true) }} => 1,
@endforeach
            default => null,
        };
    }

    public function max(string $property): int|null
    {
        return match($property) {
@foreach($modelDefinition->columns->withLength() as $columnDefinition)
            {{ $columnDefinition->formatter()->asPropertyString(true) }} => {{ $columnDefinition->length() }},
@endforeach
            default => null,
        };
    }

    public function decimals(string $property): int|null
    {
        return match($property) {
@foreach($modelDefinition->columns->withDecimals() as $columnDefinition)
            {{ $columnDefinition->formatter()->asPropertyString(true) }} => {{ $columnDefinition->decimals() }},
@endforeach
            default => null,
        };
    }

    public function default(string $property): string|int|float|bool|null
    {
        return match($property) {
@foreach($modelDefinition->columns->defaults() as $columnDefinition)
            {{ $columnDefinition->formatter()->asPropertyString(true) }} => {!! $columnDefinition->formatter()->maskedDefault() !!},
@endforeach
            default => null,
        };
    }
@endsection