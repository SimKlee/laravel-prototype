<?php /** @var \SimKlee\LaravelPrototype\Definitions\Class\ClassDefinition $classDefinition */ ?>
<?php /** @var \SimKlee\LaravelPrototype\Definitions\ModelDefinition $modelDefinition */ ?>
<?php /** @var \SimKlee\LaravelPrototype\Definitions\ColumnDefinition $columnDefinition */ ?>
<?php /** @var \SimKlee\LaravelPrototype\Definitions\IndexDefinition $indexDefinition */ ?>

@if($classDefinition->uses()->count() > 0)
@foreach($classDefinition->uses() as $use)
use {{ $use }};
@endforeach
@endif

return new class @if($classDefinition->extends()) extends {{ $classDefinition->extends() }} @endif

{
    public function up(): void
    {
        Schema::create({{ $modelDefinition->model() }}::TABLE, function (Blueprint $table) {
@foreach($modelDefinition->columns->all() as $columnDefinition)
            {!! $columnDefinition->formatter()->blueprint() !!}
@endforeach

@if($modelDefinition->foreignKeys->count() > 0 || $modelDefinition->indexes->count() > 0)
            // indexes
@foreach($modelDefinition->foreignKeys->all() as $foreignKey)
            $table->index([{{ $modelDefinition->model() }}::PROPERTY_{{ Str::upper($foreignKey->column()) }}], {{ $modelDefinition->model() }}::PROPERTY_{{ Str::upper($foreignKey->column()) }});
@endforeach
@foreach($modelDefinition->indexes->all() as $indexDefinition)
@if($indexDefinition->columns()->count() === 1)
            $table->{{ $indexDefinition->type() }}([{{ $modelDefinition->model() }}::PROPERTY_{{ Str::upper($indexDefinition->columns()->first()) }}], '{{ $indexDefinition->name() }}');
@else
            $table->{{ $indexDefinition->type() }}([
@foreach($indexDefinition->columns() as $column)
                {{ $modelDefinition->model() }}::PROPERTY_{{ Str::upper($column) }},
@endforeach
            ], '{{ $indexDefinition->name() }}');
@endif
@endforeach
@endif

@if($modelDefinition->foreignKeys->count() > 0)
            // foreign keys
@foreach($modelDefinition->foreignKeys->all() as $foreignKey)
            $table->foreign({{ $modelDefinition->model() }}::PROPERTY_{{ Str::upper($foreignKey->column()) }}, '{{ $foreignKey->name() }}')
                ->references({{ $foreignKey->foreignModel() }}::PROPERTY_{{ Str::upper($foreignKey->foreignColumn()) }})
                ->on({{ $foreignKey->foreignModel() }}::TABLE)
                ->onDelete('{{ $foreignKey->onDelete() }}')
                ->onUpdate('{{ $foreignKey->onUpdate() }}');
@endforeach
@endif

        });
    }

    public function down(): void
    {
        Schema::dropIfExists({{ $modelDefinition->model() }}::TABLE);
    }
};
