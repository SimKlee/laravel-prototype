<?php /** @var \SimKlee\LaravelPrototype\Definitions\Class\ClassDefinition $classDefinition */ ?>
<?php /** @var \SimKlee\LaravelPrototype\Definitions\ModelDefinition $modelDefinition */ ?>
<?php /** @var \SimKlee\LaravelPrototype\Definitions\ForeignKeyDefinition $foreignKey */ ?>
@if($modelDefinition->foreignKeys->count() > 0)
@foreach($modelDefinition->foreignKeys->all() as $foreignKey)
    public function {{ $foreignKey->formatter()->varName() }}(): {{ $foreignKey->formatter()->methodReturnType() }}
    {
        return $this->{{ Str::lcfirst($foreignKey->formatter()->methodReturnType()) }}({{ $foreignKey->foreignModel() }}::class);
    }

@endforeach
@endif
