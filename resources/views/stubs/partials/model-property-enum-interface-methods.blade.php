<?php /** @var \SimKlee\LaravelPrototype\Definitions\Class\BackedEnumDefinition $classDefinition */ ?>
<?php /** @var \SimKlee\LaravelPrototype\Definitions\ModelDefinition $modelDefinition */ ?>
<?php /** @var \SimKlee\LaravelPrototype\Definitions\ColumnDefinition $columnDefinition */ ?>
@if($classDefinition->hasInterface(\SimKlee\LaravelPrototype\Model\Enumerations\Contracts\TranslatableInterface::class))
    public function translationKey(): string
    {
        return '{{ $modelDefinition->model() }}.property.{{ $columnDefinition->name() }}.value.';
    }

@endif
@if($classDefinition->hasInterface(\SimKlee\LaravelPrototype\Model\Enumerations\Contracts\DefaultValueInterface::class))
    public static function default(): BackedEnum|null
    {
@if($columnDefinition->default())
        return {{ $classDefinition->class() }}::{{ Str::upper($columnDefinition->default()) }};
@else
        return null;
@endif
    }

@endif