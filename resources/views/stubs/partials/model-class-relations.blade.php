<?php /** @var \SimKlee\LaravelPrototype\Definitions\Class\ClassDefinition $classDefinition */ ?>
<?php /** @var \SimKlee\LaravelPrototype\Definitions\ModelDefinition $modelDefinition */ ?>
<?php /** @var \SimKlee\LaravelPrototype\Definitions\RelationDefinition $relation */ ?>
@if($modelDefinition->relations->count() > 0)
@foreach($modelDefinition->relations->all() as $relation)
    public function {{ $relation->formatter()->varName() }}(): {{ $relation->type() }}
    {
        return $this->{{ Str::lcfirst($relation->type()) }}({{ $relation->relatedModel() }}::class);
    }

@endforeach
@endif
