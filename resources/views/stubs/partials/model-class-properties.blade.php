<?php /** @var \SimKlee\LaravelPrototype\Definitions\Class\ClassDefinition $classDefinition */ ?>
<?php /** @var \SimKlee\LaravelPrototype\Definitions\Class\ClassPropertyDefinition $property */ ?>
@if($classDefinition->properties()->count() > 0)
@foreach($classDefinition->properties() as $property)

@if(count($property->annotations) > 0)
    /**
@foreach($property->annotations as $annotation => $value)
     * {{ '@' }}{{ $annotation }} {{ $value }}
@endforeach
     */
@endif
    {{ $property->visibility }} {{ $property->type }} ${{ $property->name }} = {!! $property->value() !!};
@endforeach
@endif
