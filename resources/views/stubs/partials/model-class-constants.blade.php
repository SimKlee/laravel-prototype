<?php /** @var \SimKlee\LaravelPrototype\Definitions\Class\ClassDefinition $classDefinition */ ?>
<?php /** @var \SimKlee\LaravelPrototype\Definitions\Class\ClassConstantDefinition $constant */ ?>
@if($classDefinition->constants()->count() > 0)
@foreach($classDefinition->constants() as $constant)
    {{ $constant->visibility }} const {{ $constant->name }} = {!! $constant->value() !!};
@endforeach
@endif
