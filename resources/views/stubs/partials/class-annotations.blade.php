<?php /** @var \SimKlee\LaravelPrototype\Definitions\Class\ClassDefinition $classDefinition */ ?>
@if($classDefinition->classAnnotationsDefinition->properties()->count() > 0 || $classDefinition->classAnnotationsDefinition->methods()->count() > 0)
/**
@foreach($classDefinition->classAnnotationsDefinition->properties() as $property)
 * @@property {{ $property->type }} ${{ $property->name }}
@endforeach
 *
@foreach($classDefinition->classAnnotationsDefinition->methods() as $method)
 * @@method @if($method->static)static @endif{{ $method->returnType }} {{ $method->name }}({{ $method->signature }})
@endforeach
 */
@endif
