<?php /** @var \SimKlee\LaravelPrototype\Definitions\Class\BackedEnumDefinition $classDefinition */ ?>
<?php /** @var \SimKlee\LaravelPrototype\Definitions\ModelDefinition $modelDefinition */ ?>
<?php /** @var \SimKlee\LaravelPrototype\Definitions\ColumnDefinition $columnDefinition */ ?>
declare(strict_types=1);

namespace {{ $classDefinition->namespace() }};
@if($classDefinition->uses()->count() > 0)

@foreach($classDefinition->uses() as $use)
use {{ $use }};
@endforeach
@endif

@include('prototype::stubs.partials.class-annotations')
enum {{ $classDefinition->class() }}: {{ $classDefinition->type() }} @if($classDefinition->implements()->count() > 0) implements {{ $classDefinition->implements()->implode(', ') }} @endif

{
@if($classDefinition->traits()->count() > 0)
@foreach($classDefinition->traits() as $trait)
    use {{ $trait }};
@endforeach

@endif
@foreach($classDefinition->cases() as $case => $value)
    case {{ $case }} = '{{ $value }}';
@endforeach

    @include('prototype::stubs.partials.model-property-enum-interface-methods')
}