@extends('prototype::class-layout')
@section('body')
    @include('prototype::stubs.partials.model-class-constants')

    @include('prototype::stubs.partials.model-class-properties')

    @include('prototype::stubs.partials.model-class-foreign-keys')

    @include('prototype::stubs.partials.model-class-relations')
@endsection