<?php /** @var \SimKlee\LaravelPrototype\Definitions\Class\ClassDefinition $classDefinition */ ?>
<?php /** @var \SimKlee\LaravelPrototype\Definitions\ModelDefinition $modelDefinition */ ?>
declare(strict_types=1);

namespace {{ $classDefinition->namespace() }};
@if($classDefinition->uses()->count() > 0)

@foreach($classDefinition->uses() as $use)
use {{ $use }};
@endforeach
@endif

@include('prototype::stubs.partials.class-annotations')
class {{ $classDefinition->class() }}@if($classDefinition->extends()) extends {{ $classDefinition->extends() }} @endif

{
@if($classDefinition->traits()->count() > 0)
@foreach($classDefinition->traits() as $trait)
    use {{ $trait }};
@endforeach

@endif
@yield('body')
}
